##
#   Test runner for NeoPS
#
##
import subprocess
from glob import glob


def run_tests():
    # Run all CPU tests
    cpu_tests =  glob('./build/bin/tests/hw/r3000/*')
    for test in cpu_tests:
        print(test)
        if test.find("test_"):
            subprocess.call([test, "--colour-mode ansi"])

    # Run all System tests
    cpu_tests =  glob('./build/bin/tests/hw/system/*')
    for test in cpu_tests:
        print(test)
        if test.find("test_"):
            subprocess.call([test, "--colour-mode ansi"])

    # Run all util tests
    cpu_tests =  glob('./build/bin/tests/util/*')
    for test in cpu_tests:
        print(test)
        if test.find("test_"):
            subprocess.call([test, "--colour-mode ansi"])
    
    subprocess.call(['sh', './generate_coverage.sh'])

if __name__ == "__main__":
    run_tests()