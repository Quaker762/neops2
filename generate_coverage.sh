#!/bin/bash

rm -rf ./coverage
mkdir -p ./coverage
py -3 -m gcovr ./build/source --html ./coverage/ --html-details 
