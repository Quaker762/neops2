/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "emu/emulator.h"
#include "util/global_config.h"
#include "util/log.h"

int main(int, char**)
{
    PSX::Emulator psx;

    SDL_Init(SDL_INIT_VIDEO);
    log(LogLevel::INFO, "Welcome to NeoPS!");

    auto launch_with_debugger = g_config.get_config_value<bool>("[debug]", "await_gdb");
    if (launch_with_debugger)
    {
        psx.run_with_debugger();
    }
    else
    {
        psx.run();
    }

    return 0;
}
