/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "emu/emulator.h"
#include "hw/system/bus.h"
#include "debugger/gdb.h"
#include "emu/display.h"

namespace PSX {

void Emulator::run()
{
    Display::the().create("NeoPS");

    SDL_Event event;
    while (true)
    {
        if (m_paused == false)
        {
            for (unsigned i = 0u; i < 250000u; i++)
            {
                m_system.cycle();
            }

            while (SDL_PollEvent(&event) != 0)
            {
                switch (event.type)
                {
                case SDL_QUIT:
                    return;
                }
            }
        }
    }

    // This section of this function can never be reached
    __builtin_unreachable();
}

void Emulator::run_with_debugger()
{
    GDBRemoteServer gdb;
    log(LogLevel::INFO, "Awaiting GDB connection...");
    gdb.await_connection();

    Display::the().create("NeoPS");
    SDL_Event event;
    while (true)
    {
        if (m_paused == false)
        {
            for (unsigned i = 0u; i < 250000u; i++)
            {
                // Only handle a command every 1000 cycles. This frees the emulator
                // up significantly and prevents huge slowdowns
                if ((i % 1000u) == 0u)
                    gdb.handle_command();

                if (gdb.should_run())
                {
                    m_system.cycle();
                }
            }

            while (SDL_PollEvent(&event) != 0)
            {
                switch (event.type)
                {
                case SDL_QUIT:
                    return;
                }
            }
        }
    }

    // This section of this function can never be reached
    __builtin_unreachable();
}

}
