/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "emu/display.h"
#include "hw/gpu/rasterizer/color.h"
#include "util/log.h"

namespace PSX
{

Display* s_display = nullptr;

void Display::create(std::string const& title [[maybe_unused]])
{
#ifndef SYSTEM_UNDER_TEST
    m_window.reset(SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height, 0));
    if (m_window.get() == nullptr)
    {
        // This condition is a terminal error
        Util::die("Failed to create display!");
    }

    // Get the draw surface
    m_draw_surface = SDL_GetWindowSurface(m_window.get());
#endif
}

void Display::resize(int width [[maybe_unused]], int height [[maybe_unused]])
{
#ifndef SYSTEM_UNDER_TEST
    SDL_SetWindowSize(m_window.get(), width, height);
    m_width     = width;
    m_height    = height;
#endif
}

// FIXME: Optimise this!
void Display::redraw(VRAM const& vram [[maybe_unused]])
{
#ifndef SYSTEM_UNDER_TEST
    u32* pixel_data = reinterpret_cast<u32*>(m_draw_surface->pixels);

    // Lock the draw surface
    auto sdl_rc = SDL_LockSurface(m_draw_surface);
    if (sdl_rc != 0)
    {
        Util::die("Failed to lock draw surface!");
    }

    for (auto y = 0u; y < static_cast<unsigned>(m_height); y++)
    {
        for (auto x = 0u; x < static_cast<unsigned>(m_width); x++)
        {
            u16 pixel = vram.read(x, y);

            // Convert pixel to RGBA8888
            u32 converted_pixel = RGBA5551ToRGBA8888(pixel);
            converted_pixel = SDL_MapRGBA(m_draw_surface->format,
                                         (converted_pixel & 0xFFu),
                                         ((converted_pixel >> 8u) & 0xFFu),
                                         ((converted_pixel >> 16u) & 0xFFu),
                                         static_cast<u8>((converted_pixel >> 24u) & 0xFFu));
            pixel_data[x + (y * static_cast<unsigned>(m_width))] = converted_pixel;
        }
    }

    SDL_UnlockSurface(m_draw_surface);
    SDL_UpdateWindowSurface(m_window.get());
#endif
}

Display& Display::the()
{
    if (s_display == nullptr)
        s_display = new Display();

    return *s_display;
}


}
