/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <sdl_header.h>
#include <emu/system.h>

namespace PSX {

class Emulator
{
    static constexpr int WINDOW_WIDTH   = 800;
    static constexpr int WINDOW_HEIGHT  = 600;

public:
    Emulator()  = default;
    ~Emulator() = default;

    /**
     * @brief Start the simulation
     */
    void run();

    /**
     * @brief Start the simulation, with debugger support
     */
    void run_with_debugger();

    /**
     * @brief Pause the currently running simulation
     */
    void pause() { m_paused = true; }

    /**
     * @brief Unpause the currently running simulation
     */
    void unpause() { m_paused = false; }

private:
    System m_system;
    bool m_paused { false };

};

}
