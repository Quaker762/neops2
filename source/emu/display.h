/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <sdl_header.h>
#include <memory>
#include <string>

#include "hw/gpu/rasterizer/vram.h"

namespace PSX
{

class Display
{
    static constexpr int DEFAULT_WIDTH  = 640;
    static constexpr int DEFAULT_HEIGHT = 480;

    struct SDL_Window_Deleter
    {
        void operator()(SDL_Window* window)
        {
            SDL_DestroyWindow(window);
        }
    };

public:
    int width() const { return m_width; }
    int height() const { return m_height; }

    SDL_Window* display() const { return m_window.get(); }

    void create(std::string const& title);
    void resize(int width, int height);
    void redraw(VRAM const& vram);

    static Display& the();

private:
    int m_width { DEFAULT_WIDTH };
    int m_height { DEFAULT_HEIGHT };

    std::unique_ptr<SDL_Window, SDL_Window_Deleter> m_window;
    SDL_Surface* m_draw_surface;
};

}
