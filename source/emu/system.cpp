/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <emu/system.h>

namespace PSX
{

bool System::create()
{
    // FIXME: This is very, very naughty
    m_system_bus = new Bus();
    if (m_system_bus == nullptr)
    {
        log(LogLevel::FATAL, "Failed to create System Bus!");
        return false;
    }

    return true;
}

void System::cycle()
{
    m_system_bus->the()->cpu().cycle();
    m_system_bus->the()->cdrom().cycle();
}

}
