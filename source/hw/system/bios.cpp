/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/bios.h"
#include "util/log.h"

#include <fstream>

namespace PSX
{

bool BIOS::load(std::string const& path)
{
    std::ifstream bios_file;

    bios_file.open(path, std::ios_base::openmode::_S_in | std::ios_base::openmode::_S_bin);
    if (bios_file.is_open() == false)
    {
        log(LogLevel::ERROR, "Failed to open BIOS file %s!", path.c_str());
        return false;
    }

    // Jesus Christ C++....
    (void)bios_file.read(reinterpret_cast<char*>(&m_bios[0]), sizeof(m_bios));

    return true;
}

u8 BIOS::read8(paddr_t address)
{
    auto index = address & ADDRESS_MASK;

    return m_bios[index];
}

u16 BIOS::read16(paddr_t address)
{
    auto index = address & ADDRESS_MASK;
    u16 value;

    (void)memcpy(&value, &m_bios[index], sizeof(value));

    return value;
}

u32 BIOS::read32(paddr_t address)
{
    auto index = address & ADDRESS_MASK;
    u32 value;

    (void)memcpy(&value, &m_bios[index], sizeof(value));

    return value;
}

void BIOS::write8(paddr_t address, u8 data)
{
    log(LogLevel::WARN, "Attempted to write 0x%2x to bios @ 0x%08x", data, address);
}

void BIOS::write16(paddr_t address, u16 data)
{
    log(LogLevel::WARN, "Attempted to write 0x%4x to bios @ 0x%08x", data, address);
}

void BIOS::write32(paddr_t address, u32 data)
{
    log(LogLevel::WARN, "Attempted to write 0x%8x to bios @ 0x%08x", data, address);
}

}
