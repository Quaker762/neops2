/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/ram.h"
#include <cstring>
#include <fstream>

namespace PSX
{

static constexpr const char* RAM_DUMP_FILENAME = "ram.bin";

u8 SystemMemory::read8(paddr_t address)
{
    auto index = address & ADDRESS_MASK;

    return m_ram[index];
}

u16 SystemMemory::read16(paddr_t address)
{
    auto index = address & ADDRESS_MASK;
    u16 value;

    (void)memcpy(&value, &m_ram[index], sizeof(value));

    return value;
}

u32 SystemMemory::read32(paddr_t address)
{
    auto index = address & ADDRESS_MASK;
    u32 value;

    (void)memcpy(&value, &m_ram[index], sizeof(value));

    return value;
}

void SystemMemory::write8(paddr_t address, u8 data)
{
    auto index = address & ADDRESS_MASK;

    m_ram[index] = data;
}

void SystemMemory::write16(paddr_t address, u16 data)
{
    auto index = address & ADDRESS_MASK;

    (void)memcpy(&m_ram[index], &data, sizeof(data));
}

void SystemMemory::write32(paddr_t address, u32 data)
{
    auto index = address & ADDRESS_MASK;

    (void)memcpy(&m_ram[index], &data, sizeof(data));
}

void SystemMemory::dump_to_disk() const
{
    std::ofstream ram_file;

    ram_file.open(RAM_DUMP_FILENAME, std::ios_base::binary);
    ram_file.write(reinterpret_cast<char const*>(&m_ram[0]), sizeof(m_ram));
    ram_file.close();
}

}
