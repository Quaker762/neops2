/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

static constexpr paddr_t IO_BASE_ADDRESS                = 0x1F801000u;
static constexpr paddr_t IO_MEMORY_CONTROL1_BIOS_ROM    = (IO_BASE_ADDRESS + 0x10u);
static constexpr paddr_t IO_MEMORY_CONTROL1_COM_DELAY   = (IO_BASE_ADDRESS + 0x20u);
static constexpr paddr_t IO_MEMORY_CONTROL2_RAM_SIZE    = (IO_BASE_ADDRESS + 0x60u);

static constexpr paddr_t IO_MEMORY_CONTROL3_CACHE_CTL   = 0xFFFE0130u;

// Expansion 2 POST I/O addresses
static constexpr paddr_t EXPANSION2_POST_ATCONS_STAT    = 0x1F802000u;
static constexpr paddr_t EXPANSION2_POST_ATCONS_DATA    = 0x1F802002u;
static constexpr paddr_t EXPANSION2_POST_BOOT_DIP       = 0x1F802040u;
static constexpr paddr_t EXPANSION2_POST_POST1          = 0x1F802041u;
static constexpr paddr_t EXPANSION2_POST_POST2          = 0x1F802042u;

// Interrupt Control
static constexpr paddr_t INTERRUPT_CONTROL_I_STAT       = 0x1F801070u;
static constexpr paddr_t INTERRUPT_CONTROL_I_MASK       = 0x1F801074u;

/**
 * @brief Assorted BIOS/Console POST data
 */
struct POSTInfo
{
    u8 atcons_stat;
    u8 atconst_data;
    u16 unknown;
    u8 irq10_flags;
    u16 boot_mode_switch;
    u8 post1;
    u8 post2;
};

}
