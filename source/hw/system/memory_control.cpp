/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/memory_control.h"
#include "util/log.h"

namespace PSX
{

u8 MemoryControl::read8(paddr_t address)
{
    auto offset = ((address & 0xFFu) >> 2u);

    return (m_registers[offset >> 2u] & 0xFFu);
}

u16 MemoryControl::read16(paddr_t address)
{
    auto offset = ((address & 0xFFu) >> 2u);

    return (m_registers[offset] & 0xFFFFu);
}

u32 MemoryControl::read32(paddr_t address)
{
    auto offset = (address & 0xFFu);

    return m_registers[offset >> 2u];
}

void MemoryControl::write8(paddr_t, u8)
{
}

void MemoryControl::write16(paddr_t, u16)
{

}

void MemoryControl::write32(paddr_t address, u32 data)
{
    auto offset = ((address & 0xFFu) >> 2u);

    log(LogLevel::INFO, "Writing 0x%08x to Memory Control 1 register %s", data, REGISTER_NAMES[offset >> 2u]);
    m_registers[offset >> 2u] = data;
}

}
