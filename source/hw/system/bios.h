/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"
#include "types.h"

#include <string>

namespace PSX
{

class BIOS : public BusDevice
{
    static constexpr size_t     BIOS_SIZE       = (512 * KB);
    static constexpr u32        ADDRESS_MASK    = 0x7FFFFu;

    static constexpr paddr_t    BIOS_ADDRESS_LO = 0x1FC00000u;
    static constexpr paddr_t    BIOS_ADDRESS_HI = (BIOS_ADDRESS_LO + BIOS_SIZE);

public:
    BIOS() : BusDevice(BIOS_ADDRESS_LO, BIOS_ADDRESS_HI) {};
    virtual ~BIOS(){}

    bool load(std::string const& path);

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

private:
    u8 m_bios[BIOS_SIZE];
};

}
