/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"
#include "types.h"

namespace PSX
{

class Timers : public BusDevice
{
    // Raw timer (containing registers)
    struct Timer
    {
        static constexpr size_t NUMBER_OF_REGISTERS = 3u;
        u16 registers[NUMBER_OF_REGISTERS];
    };

    static constexpr paddr_t TIMER_ADDRESS_RANGE_LO     = 0x1F801100u;
    static constexpr paddr_t TIMER_ADDRESS_RANGE_HI     = 0x1F801128u;
    static constexpr size_t NUMBER_OF_TIMERS            = 3u;

    static constexpr size_t CURRENT_COUNTER_INDEX       = 0u;
    static constexpr size_t COUNTER_MODE_INDEX          = 1u;
    static constexpr size_t COUNTER_TARGET_INDEX        = 2u;

public:
    Timers() : BusDevice(TIMER_ADDRESS_RANGE_LO, TIMER_ADDRESS_RANGE_HI) {}

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

private:
    Timer m_timers[NUMBER_OF_TIMERS];

};

}
