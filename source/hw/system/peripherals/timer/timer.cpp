/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/peripherals/timer/timer.h"
#include "util/log.h"

u8 PSX::Timers::read8(paddr_t address)
{
    Util::die("Attempted 8-bit read of timer register @ 0x%08x", address);
}

u16 PSX::Timers::read16(paddr_t address)
{
    auto register_index = ((address & 0xFu) / 4u);
    auto timer_index    = ((address >> 4u) & 0xFu);

    return m_timers[timer_index].registers[register_index];
}

u32 PSX::Timers::read32(paddr_t address)
{
    auto register_index = ((address & 0xFu) / 4u);
    auto timer_index    = ((address >> 4u) & 0xFu);

    return m_timers[timer_index].registers[register_index];
}

void PSX::Timers::write8(paddr_t address, u8 data)
{
    Util::die("Attempted 8-bit write of 0x%02x to timer register @ 0x%08x", data, address);
}

void PSX::Timers::write16(paddr_t address, u16 data)
{
    auto register_index = ((address & 0xFu) / 4u);
    auto timer_index    = ((address >> 4u) & 0xFu);

    m_timers[timer_index].registers[register_index] = data;
}

void PSX::Timers::write32(paddr_t address, u32 data)
{
    Util::die("Attempted 32-bit write of 0x%02x to timer register @ 0x%08x", data, address);
}
