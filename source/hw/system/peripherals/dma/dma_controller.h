/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"
#include "types.h"
#include "dma_channel.h"

#include <vector>

namespace PSX
{

class DMAController : public BusDevice
{
    static constexpr size_t DMA_CONTROLLER_NUMBER_OF_CHANNELS   = 7u;
    static constexpr paddr_t DMA_CONTROLLER_ADDRESS_START       = 0x1F801080u;
    static constexpr paddr_t DMA_CONTROLLER_ADDRESS_END         = 0x1F8010FCu;
    static constexpr paddr_t DMA_DPCR_ADDRESS                   = 0x1F8010F0u;
    static constexpr paddr_t DMA_DICR_ADDRESS                   = 0x1F8010F4u;
    static constexpr paddr_t DMA_CONTROLLER_CHANNELS_START      = 0x1F801080u;
    static constexpr paddr_t DMA_CONTROLLER_CHANNELS_END        = DMA_CONTROLLER_CHANNELS_START + (DMA_CONTROLLER_NUMBER_OF_CHANNELS * (sizeof(DMAChannel) + 4u));\

    static constexpr u32 DMA_CHANNEL_REGISTER_MASK              = 0x0000000Fu;
    static constexpr u32 DMA_CHANNEL_NUMBER_MASK                = 0x000000E0u;
    static constexpr u32 DMA_CHANNEL_INDEX_MADR                 = 0u;
    static constexpr u32 DMA_CHANNEL_INDEX_BCR                  = 1u;
    static constexpr u32 DMA_CHANNEL_INDEX_CHCR                 = 2u;

    static constexpr u32 DMA_DPCR_RESET_VALUE                   = 0x07654321u;
    static constexpr u32 DMA_DICR_WRITE_MASK                    = 0x7FFFFFFFu;
    static constexpr u32 DMA_CHANNEL6_WRITE_MASK                = 0x51000002u;
    static constexpr u32 DMA_LIST_ENTRY_ADDRESS_MASK            = 0x00FFFFFFu;
    static constexpr u32 DMA_LIST_ENTRY_END_MARKER              = 0x00FFFFFFu;

public:
    DMAController() : BusDevice(DMA_CONTROLLER_ADDRESS_START, DMA_CONTROLLER_ADDRESS_END), m_channels(DMA_CONTROLLER_NUMBER_OF_CHANNELS)
    {
        auto channel_id = 0u;
        for (auto& channel : m_channels)
        {
            channel.set_channel_id(channel_id);
            channel_id++;
        }
    }

    ~DMAController(){}

    // TODO: Add `reset()` function to BusDevice??!

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

    bool irq() const;

private:
    // Register handling functions
    void write_dicr(u32 data);

    // DMA Channel handling functions
    u32 constexpr get_channel_register_index(paddr_t address) const { return (address & DMA_CHANNEL_REGISTER_MASK) / 4u; }
    u32 constexpr get_channel_number(paddr_t address) const { return ((address & DMA_CHANNEL_NUMBER_MASK) >> 4u) - 8u; }

    template <typename T>
    void write_channel(DMAChannel& channel, u32 channel_index, u32 register_index, T const data);
    template <typename T>
    T read_channel(DMAChannel& channel, u32 channel_index, u32 register_index);
    void do_dma(DMAChannel& channel);

    bool channel_active(DMAChannel const& channel);

    u32 immediate_transfer(DMAChannel&);
    u32 sync_block_transfer(DMAChannel&);
    u32 linked_list_transfer(DMAChannel&);

    // Specific Channel functions
    u32 get_otc_write_value(u32 words_left, paddr_t current_address);

private:
    DMAControlRegister      m_dpcr { .m_word = DMA_DPCR_RESET_VALUE };    // BUG: This won't be reset if the system warm boots (and probably break something!)
    DMAInterruptRegister    m_dicr { .m_word = 0u };
    std::vector<DMAChannel> m_channels;
};

}
