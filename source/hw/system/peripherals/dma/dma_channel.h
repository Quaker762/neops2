/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "dma_types.h"
#include "types.h"

namespace PSX
{

class DMAChannel
{
public:
    DMAChannel()    = default;
    ~DMAChannel()   = default;

    void write_madr(u32 data) { m_madr.m_word = data; }
    void write_bcr(u32 data) { m_bcr.m_word = data; }
    void write_chcr(u32 data) { m_chcr.m_word = data; }

    u32 read_madr() const { return m_madr.m_word; }
    u32 read_bcr() const { return m_bcr.m_word; }
    u32 read_chcr() const { return m_chcr.m_word; }

    BaseAddressRegister const& madr() const { return m_madr; }
    BlockControlRegister const& bcr() const { return m_bcr; }
    ChannelControlRegister const& chcr() const { return m_chcr; }

    bool start_busy_flag_set() const { return m_chcr.bits.start_busy; }
    bool start_trigger_flag_set() const { return m_chcr.bits.start_trigger; }

    void clear_busy_flag() { m_chcr.bits.start_busy = 0u; }
    void clear_trigger_flag() { m_chcr.bits.start_trigger = 0u; }
    void set_channel_id(u32 channel_id) { m_channel_id = channel_id; }

    u32 number_of_words_to_transfer() const;
    u32 channel_id() const { return m_channel_id; }

    TransferMode transfer_mode() const;
    DMADirection direction() const;
    DMAAddressStep address_step() const;

private:
    BaseAddressRegister     m_madr;
    BlockControlRegister    m_bcr;
    ChannelControlRegister  m_chcr;
    u32                     m_channel_id;
};

}
