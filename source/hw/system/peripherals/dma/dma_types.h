/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

static constexpr char const* const CHANNEL_REGISTER_NAMES[3] =
{
    "MADR",
    "BCR",
    "CHCR",
};

enum TransferMode
{
    BlockTransfer,
    SyncBlockTransfer,
    LinkedListTransfer
};

enum class DMAChannels : u32
{
    MDECIn      = 0u,
    MDECOut     = 1u,
    GPU         = 2u,
    CDDROM      = 3u,
    SPU         = 4u,
    PIO         = 5u,
    OTC         = 6u
};

enum class DMADirection
{
    ToRAM,
    FromRAM,
};

enum class DMAAddressStep
{
    Forward,
    Backward
};

union [[gnu::packed]] BaseAddressRegister
{
    struct
    {
        u32 start_address   : 24;
        u32 always_zero     : 8;
    } bits;

    u32 m_word;
};

union [[gnu::packed]] BlockControlRegister
{
    struct
    {
        u32 block_count : 16;
        u32 unused      : 16;
    } sync0;

    struct
    {
        u32 block_size      : 16;
        u32 block_amount    : 16;
    } sync1;

    struct
    {
        u32 should_be_zero;
    } sync2;

    u32 m_word;
};

union [[gnu::packed]] ChannelControlRegister
{
    struct
    {
        u32 transfer_direction          : 1;
        u32 address_step                : 1;
        u32 unused1                     : 6;
        u32 chopping_enable             : 1;
        u32 sync_mode                   : 2;
        u32 unused2                     : 5;
        u32 chopping_dma_window_size    : 3;
        u32 unused3                     : 1;
        u32 chopping_cpu_window_size    : 3;
        u32 unused4                     : 1;
        u32 start_busy                  : 1;
        u32 unused5                     : 3;
        u32 start_trigger               : 1;
        u32 unknown1                    : 1;
        u32 unknown2                    : 1;
        u32 unused6                     : 1;
    } bits;

    u32 m_word;
};

union [[gnu::packed]] DMAControlRegister
{
    struct
    {
        u32 dma0_priority : 3;
        u32 dma0_enable   : 1;
        u32 dma1_priority : 3;
        u32 dma1_enable   : 1;
        u32 dma2_priority : 3;
        u32 dma2_enable   : 1;
        u32 dma3_priority : 3;
        u32 dma3_enable   : 1;
        u32 dma4_priority : 3;
        u32 dma4_enable   : 1;
        u32 dma5_priority : 3;
        u32 dma5_enable   : 1;
        u32 dma6_priority : 3;
        u32 dma6_enable   : 1;
        u32 unknown1      : 3;
        u32 unknown2      : 1;
    } bits;

    u32 m_word;
};

union DMAInterruptRegister
{
    struct
    {
        u32 unknown             : 6;
        u32 unused              : 9;
        u32 force_irq           : 1;
        u32 dma0_irq_en         : 1;
        u32 dma1_irq_en         : 1;
        u32 dma2_irq_en         : 1;
        u32 dma3_irq_en         : 1;
        u32 dma4_irq_en         : 1;
        u32 dma5_irq_en         : 1;
        u32 dma6_irq_en         : 1;
        u32 irq_master_en       : 1;
        u32 dma0_irq            : 1;
        u32 dma1_irq            : 1;
        u32 dma2_irq            : 1;
        u32 dma3_irq            : 1;
        u32 dma4_irq            : 1;
        u32 dma5_irq            : 1;
        u32 dma6_irq            : 1;
        u32 irq_triggered       : 1;
    } bits;

    u32 m_word;
};

}
