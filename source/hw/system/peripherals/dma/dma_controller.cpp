/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/bus.h"
#include "hw/system/peripherals/dma/dma_controller.h"
#include "util/log.h"

namespace PSX
{

static constexpr bool DEBUG_ENABLE = false;

template <>
void DMAController::write_channel(DMAChannel& channel, u32 channel_index, u32 register_index, u8 const data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%08x to DMA Channel %d %s", data, channel_index, CHANNEL_REGISTER_NAMES[register_index]);
    }

    switch (register_index)
    {
    case DMA_CHANNEL_INDEX_MADR:
        channel.write_madr(data);
        break;
    case DMA_CHANNEL_INDEX_BCR:
        channel.write_bcr(data);
        break;
    case DMA_CHANNEL_INDEX_CHCR:
        channel.write_chcr(data);
        break;
    default:
        log(LogLevel::WARN, "Bad DMA Channel register index %d! Ignoring write!", register_index);
        break;
    }
}

template <>
void DMAController::write_channel(DMAChannel& channel, u32 channel_index, u32 register_index, u16 const data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%08x to DMA Channel %d %s", data, channel_index, CHANNEL_REGISTER_NAMES[register_index]);
    }

    switch (register_index)
    {
    case DMA_CHANNEL_INDEX_MADR:
        channel.write_madr(data);
        break;
    case DMA_CHANNEL_INDEX_BCR:
        channel.write_bcr(data);
        break;
    case DMA_CHANNEL_INDEX_CHCR:
        channel.write_chcr(data);
        break;
    default:
        log(LogLevel::WARN, "Bad DMA Channel register index %d! Ignoring write!", register_index);
        break;
    }
}

template <>
void DMAController::write_channel(DMAChannel& channel, u32 channel_index, u32 register_index, u32 const data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%08x to DMA Channel %d register %s", data, channel_index, CHANNEL_REGISTER_NAMES[register_index]);
    }

    switch (register_index)
    {
    case DMA_CHANNEL_INDEX_MADR:
        channel.write_madr(data);
        break;
    case DMA_CHANNEL_INDEX_BCR:
        channel.write_bcr(data);
        break;
    case DMA_CHANNEL_INDEX_CHCR:
        channel.write_chcr(data);
        break;
    default:
        log(LogLevel::WARN, "Bad DMA Channel register index %d! Ignoring write!", register_index);
        break;
    }
}

template <>
u32 DMAController::read_channel(DMAChannel& channel, u32 channel_index, u32 register_index)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Reading DMA Channel %d %s", channel_index, CHANNEL_REGISTER_NAMES[register_index]);
    }

    switch (register_index)
    {
    case DMA_CHANNEL_INDEX_MADR:
        return channel.read_madr();
    case DMA_CHANNEL_INDEX_BCR:
        return channel.read_bcr();
    case DMA_CHANNEL_INDEX_CHCR:
        return channel.read_chcr();
    default:
        log(LogLevel::WARN, "Bad DMA Channel register index %d! Ignoring write!", register_index);
        break;
    }

    return 0u;
}

u8 DMAController::read8(paddr_t address)
{
    Util::die("Attempted 8-bit read from DMA controller @ 0x%08x", address);
}

u16 DMAController::read16(paddr_t address)
{
    Util::die("Attempted 16-bit read from DMA controller @ 0x%08x", address);
}

u32 DMAController::read32(paddr_t address)
{
    if (address == DMA_DPCR_ADDRESS)
    {
        return m_dpcr.m_word;
    }
    else if (address == DMA_DICR_ADDRESS)
    {
        return m_dicr.m_word;
    }
    else if ((address >= DMA_CONTROLLER_CHANNELS_START) && (address <= DMA_CONTROLLER_CHANNELS_END))
    {
        auto channel_number = get_channel_number(address);
        auto register_index = get_channel_register_index(address);
        auto& channel = m_channels.at(channel_number);

        return read_channel<u32>(channel, channel_number, register_index);
    }
    else
    {
        log(LogLevel::WARN, "Attempted 32-bit read of from DMA Controller @ 0x%08x", address);
    }

    return 0u;
}

void DMAController::write8(paddr_t address, u8 data)
{
    Util::die("Attempted 8-bit write of 0x%02x to DMA controller @ 0x%08x", data, address);
}

void DMAController::write16(paddr_t address, u16 data)
{
    Util::die("Attempted 16-bit write of 0x%04x to DMA controller @ 0x%08x", data, address);
}

void DMAController::write32(paddr_t address, u32 data)
{
    if (address == DMA_DPCR_ADDRESS)
    {
        m_dpcr.m_word = data;
    }
    else if (address == DMA_DICR_ADDRESS)
    {
        write_dicr(data);
    }
    else if ((address >= DMA_CONTROLLER_CHANNELS_START) && (address <= DMA_CONTROLLER_CHANNELS_END))
    {
        auto channel_number = get_channel_number(address);
        auto register_index = get_channel_register_index(address);
        auto& channel = m_channels.at(channel_number);

        if (channel_number == static_cast<u32>(DMAChannels::OTC))
        {
            // For DMA Channel 6 CHCR, only bits 24, 28 and 30 are writeable
            if (register_index == DMA_CHANNEL_INDEX_CHCR)
            {
                data &= DMA_CHANNEL6_WRITE_MASK;
                data |= 2u; // Bit1 is always '1' for Channel6.CHCR
            }
        }

        write_channel<u32>(channel, channel_number, register_index, data);

        // Make sure the DMA channel is actually enabled in DPCR
        // We use __builtin_expect() here because it's most likely that the programmer
        // has been smart enough to enable the channel before trying to kick off the transfer,
        // therefore we should let the compiler know that this branch is most likely
        // always going to be true
        if (__builtin_expect((m_dpcr.m_word & (1u << (channel_number * 4u))) > 0, 1))
        {
            // Kick off DMA if the channel is active
            if (channel_active(channel))
            {
                do_dma(channel);
            }
        }
    }
    else
    {
        log(LogLevel::WARN, "Attempted 32-bit write of 0x%08x to DMA controller @ 0x%08x", data, address);
    }
}

bool DMAController::irq() const
{
    bool irq = false;   // Assume that we're not generating an IRQ signal

    if (m_dicr.bits.force_irq)
    {
        irq = true;
    }
    else if (m_dicr.bits.irq_master_en)
    {
        if (((m_dicr.m_word & 0x007F0000) > 0u) && ((m_dicr.m_word & 0x7F000000) > 0u))
        {
            irq = true;
        }
    }

    return irq;
}

void DMAController::write_dicr(u32 data)
{
    // Disallow writes to bit31 (it's read only)
    // https://psx-spx.consoledev.net/dmachannels/
    u32 dicr_value = data & DMA_DICR_WRITE_MASK;

    // Bits 24-30 are Write-1-Reset
    u8 ack  = ((data >> 24u) & 0x7F);
    dicr_value &= ~ack;

    m_dicr.m_word = dicr_value;
}

bool DMAController::channel_active(DMAChannel const& channel)
{
    if (channel.transfer_mode() == TransferMode::BlockTransfer)
    {
        return channel.start_busy_flag_set() && channel.start_trigger_flag_set();
    }

    return channel.start_busy_flag_set();
}

void DMAController::do_dma(DMAChannel& channel)
{
    switch (channel.transfer_mode())
    {
    case TransferMode::BlockTransfer:
        immediate_transfer(channel);
        break;
    case TransferMode::SyncBlockTransfer:
        sync_block_transfer(channel);
        break;
    case TransferMode::LinkedListTransfer:
        linked_list_transfer(channel);
        break;
    }
}

// DMA Transfer functions
u32 DMAController::immediate_transfer(DMAChannel& channel)
{
    // Start/Trigger is unset immediately
    channel.clear_trigger_flag();
    i32     words_left      = static_cast<i32>(channel.number_of_words_to_transfer());
    vaddr_t current_address = channel.madr().bits.start_address;
    i32     address_step    = (channel.address_step() == DMAAddressStep::Forward) ? 4 : -4;
    u32     write_value     = 0u;

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Starting Block DMA Transfer of %d words on Channel %u (start address @ 0x%08x)", words_left, channel.channel_id(), current_address);
    }

    // Immediately clear the trigger flag
    channel.clear_trigger_flag();

    // Get the "real" name of the channel
    DMAChannels channel_name = static_cast<DMAChannels>(channel.channel_id());
    while (words_left > 0)
    {
        if (channel.direction() == DMADirection::FromRAM)
        {
            Util::die("DMA from RAM currently unsupported!");
        }
        else
        {
            // FIXME: Refactor this into its own function!
            // Write the OTC table
            switch (channel_name)
            {
            case DMAChannels::OTC:
                write_value = get_otc_write_value(static_cast<u32>(words_left), current_address);
                break;
            case DMAChannels::CDDROM:
            case DMAChannels::GPU:
            case DMAChannels::MDECIn:
            case DMAChannels::MDECOut:
            case DMAChannels::PIO:
            case DMAChannels::SPU:
            default:
                Util::die("Unimplemented Block Transfer port!");
            }

            Bus::the()->write(current_address, write_value);
        }

        current_address += static_cast<u32>(address_step);
        words_left--;
    }

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "DMA Transfer done!");
    }

    channel.clear_busy_flag();

    return 0u;
}

u32 DMAController::sync_block_transfer(DMAChannel& channel)
{
    paddr_t current_address = channel.madr().bits.start_address;
    auto block_size         = channel.bcr().sync1.block_size;
    auto number_of_blocks   = channel.bcr().sync1.block_amount;
    i32 address_step        = (channel.address_step() == DMAAddressStep::Forward) ? 4 : -4;
    i32 words_left          = static_cast<i32>(block_size * number_of_blocks);

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Starting Sync DMA Transfer of %d words on Channel %u (start address @ 0x%08x)", words_left, channel.channel_id(), current_address);
    }

    // Get the "real" name of the channel
    DMAChannels channel_name = static_cast<DMAChannels>(channel.channel_id());
    u32 write_value;
    while (words_left > 0)
    {
        if (channel.direction() == DMADirection::FromRAM)
        {
            // FIXME: Refactor this into its own function!
            // Write the OTC table
            switch (channel_name)
            {
            case DMAChannels::GPU:
                write_value = Bus::the()->read<u32>(current_address);
                Bus::the()->gpu().write_gp0(write_value);
                break;
            case DMAChannels::CDDROM:
            case DMAChannels::OTC:
            case DMAChannels::MDECIn:
            case DMAChannels::MDECOut:
            case DMAChannels::PIO:
            case DMAChannels::SPU:
            default:
                Util::die("Unimplemented Block Transfer port!");
            }
        }
        else if (channel.direction() == DMADirection::ToRAM)
        {
            Util::die("Unsupported Sync DMA Request direction (ToRAM)!");
        }

        current_address += static_cast<u32>(address_step);
        channel.write_madr(current_address);    // MADR is updated
        words_left--;
    }

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "DMA Transfer done!");
    }

    channel.clear_busy_flag();

    return 0u;
}

u32 DMAController::linked_list_transfer(DMAChannel& channel)
{
    paddr_t entry_address   = channel.madr().bits.start_address;
    u32 list_entry          = 0u;

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Starting Linked List DMA Transfer, starting @ 0x%08x", entry_address);
    }

    do
    {
        list_entry = Bus::the()->read<u32>(entry_address);
        u8 packet_size  = static_cast<u8>((list_entry >> 24u) & 0xFFu);

        // Read all commands in the list
        paddr_t address = entry_address;
        for (auto i = 0u; i < packet_size; i++)
        {
            address += sizeof(address);
            u32 word = Bus::the()->read<u32>(address);

            if constexpr (DEBUG_ENABLE)
            {
                log(LogLevel::INFO, "GPU Command: 0x%08x", word);
            }

            Bus::the()->gpu().write_gp0(word);
        }

        entry_address = (list_entry & DMA_LIST_ENTRY_ADDRESS_MASK);

        // MADR is updated every cycle
        channel.write_madr(entry_address);
    } while ((list_entry & DMA_LIST_ENTRY_ADDRESS_MASK) != DMA_LIST_ENTRY_END_MARKER);

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "DMA Transfer done!");
    }

    channel.clear_busy_flag();

    return 0u;
}

/////////////////////////////////////////////////////////////////////////

u32 DMAController::get_otc_write_value(u32 words_left, paddr_t current_address)
{
    if (words_left == 1u)
    {
        return 0x00FFFFFFu;    // Terminate the lsit
    }
    else
    {
        return (current_address - 4u) & 0x1FFFFFu;
    }
}

}
