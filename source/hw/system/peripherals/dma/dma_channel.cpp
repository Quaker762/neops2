/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "dma_channel.h"
#include "util/log.h"

namespace PSX

{

TransferMode DMAChannel::transfer_mode() const
{
    switch (m_chcr.bits.sync_mode)
    {
    case 0u:
        return TransferMode::BlockTransfer;
    case 1u:
        return TransferMode::SyncBlockTransfer;
    case 2u:
        return TransferMode::LinkedListTransfer;
    }

    Util::die("DMA Channel SyncMode was set to an invalid value! CHCR = 0x%08x", m_chcr.m_word);
}

DMADirection DMAChannel::direction() const
{
    switch (m_chcr.bits.transfer_direction)
    {
    case 0u:
        return DMADirection::ToRAM;
    case 1u:
        return DMADirection::FromRAM;
    }

    Util::die("Bad DMA Direction value!");
}

DMAAddressStep DMAChannel::address_step() const
{
    switch (m_chcr.bits.address_step)
    {
    case 0:
        return DMAAddressStep::Forward;
    case 1:
        return DMAAddressStep::Backward;
    }

    Util::die("Bad DMA Address step value!");
}

u32 DMAChannel::number_of_words_to_transfer() const
{
    u32 number_of_words;

    switch (transfer_mode())
    {
    case TransferMode::BlockTransfer:
        number_of_words = m_bcr.sync0.block_count;
        break;
    case TransferMode::SyncBlockTransfer:
        number_of_words = m_bcr.sync1.block_amount * m_bcr.sync1.block_size;
        break;
    case TransferMode::LinkedListTransfer:
    default:
        number_of_words = 0u;
        break;
    }

    return number_of_words;
}


}
