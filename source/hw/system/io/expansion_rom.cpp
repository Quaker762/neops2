/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/io/expansion_rom.h"
#include "util/log.h"

namespace PSX
{

u8 ExpansionRom::read8(paddr_t)
{
    return 0xFFu;
}

u16 ExpansionRom::read16(paddr_t)
{
    return 0xFFFFu;
}

u32 ExpansionRom::read32(paddr_t)
{
    return 0xFFFFFFFFu;
}

void ExpansionRom::write8(paddr_t address, u8)
{
    log(LogLevel::INFO, "Attempted 8-bit write to Expansion ROM @ 0x%08x", address);
}

void ExpansionRom::write16(paddr_t address, u16)
{
    log(LogLevel::INFO, "Attempted 16-bit write to Expansion ROM @ 0x%08x", address);
}

void ExpansionRom::write32(paddr_t address, u32)
{
    log(LogLevel::INFO, "Attempted 32-bit write to Expansion ROM @ 0x%08x", address);
}

}
