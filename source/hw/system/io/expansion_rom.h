/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"

namespace PSX
{

class ExpansionRom : public BusDevice
{
    static constexpr paddr_t EXPANSION1_START_ADDRESS   = 0x1F000000u;
    static constexpr paddr_t EXPANSION1_END_ADDRESS     = 0x1F000100u;

public:
    ExpansionRom() : BusDevice(EXPANSION1_START_ADDRESS, EXPANSION1_END_ADDRESS){}

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;
};

}
