/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/bus.h"
#include "hw/system/io.h"
#include "util/global_config.h"
#include "util/log.h"

namespace PSX
{

static Bus* s_bus = nullptr;

Bus* Bus::the()
{
    if (s_bus == nullptr)
    {
        s_bus = new Bus();

        auto bootrom_filename = g_config.get_config_value<std::string>("[system]", "bios_rom");
        if (s_bus->m_bios.load(bootrom_filename) == false)
        {
            Util::die("Couldn't load BIOS rom, %s!", bootrom_filename.c_str());
        }
    }

    return s_bus;
}

bool Bus::is_in_io_register_region(paddr_t address) const
{
    static constexpr paddr_t IO_REGION_BASE = 0x1F801000u;

    return ((((address >= IO_REGION_BASE)) && ((address < (IO_REGION_BASE + 8 * KB)))) || (address == IO_MEMORY_CONTROL3_CACHE_CTL));
}

template <>
void Bus::io_write(paddr_t address, u8 const data)
{
    if (address == EXPANSION2_POST_POST1)
    {
        m_post_info.post1 = data;
    }
    else if ((address >= m_cdrom.address_lo()) && (address <= m_cdrom.address_hi()))
    {
        m_cdrom.write8(address, data);
    }
    else
    {
        Util::die("Unknown byte IO write of 0x%08x to 0x%08x", data, address);
    }
}

template <>
void Bus::io_write(paddr_t address, u16 const data)
{
    if ((address >= m_spu.address_lo()) && (address < m_spu.address_hi()))
    {
        m_spu.write16(address, data);
    }
    else if ((address >= m_timers.address_lo()) && (address <= m_timers.address_hi()))
    {
        m_timers.write16(address, data);
    }
    else if ((address >= m_joystick.address_lo()) && (address <= m_joystick.address_hi()))
    {
        m_joystick.write16(address, data);
    }
    else
    {
        Util::die("Unknown half-word IO write of 0x%08x to 0x%08x", data, address);
    }
}

template <>
void Bus::io_write(paddr_t address, u32 const data)
{
    if ((address >= m_memory_control_1.address_lo()) && (address <= m_memory_control_1.address_hi()))
    {
        m_memory_control_1.write32(address, data);
    }
    else if ((address >= m_dma.address_lo()) && (address <= m_dma.address_hi()))
    {
        m_dma.write32(address, data);
    }
    else if ((address >= m_gpu.address_lo()) && (address <= m_gpu.address_hi()))
    {
        m_gpu.write32(address, data);
    }
    else if ((address >= m_spu.address_lo()) && (address < m_spu.address_hi()))
    {
        m_spu.write32(address, data);
    }
    else if ((address >= m_timers.address_lo()) && (address <= m_timers.address_hi()))
    {
        // The BIOS performs a 32-bit write here... Perhaps someone at SONY accidentally decided to
        // emite a 32-bit store via a pointer deref? Either way, let's just truncate the upper 16-bits
        // like the hardware would do.
        m_timers.write16(address, static_cast<u16>(data));
    }
    else if (address == IO_MEMORY_CONTROL2_RAM_SIZE)
    {
        m_ram_size = data;
    }
    else if (address == INTERRUPT_CONTROL_I_STAT)
    {
        m_cpu.write_irq_stat(data);
    }
    else if (address == INTERRUPT_CONTROL_I_MASK)
    {
        m_cpu.write_irq_mask(data);
    }
    else if (address == IO_MEMORY_CONTROL3_CACHE_CTL)
    {
        log(LogLevel::INFO, "Attempting to write 0x%08x to CACHE_CTL", data);
    }
    else
    {
        Util::die("Unknown word IO write of 0x%08x to 0x%08x", data, address);
    }
}

template <>
u8 Bus::io_read(paddr_t address)
{
    if ((address >= m_cdrom.address_lo()) && (address <= m_cdrom.address_hi()))
    {
        return m_cdrom.read8(address);
    }
    else
    {
        Util::die("Unknown 8-bit IO word read from 0x%08x", address);
    }

    return 0xFFu;
}

template <>
u16 Bus::io_read(paddr_t address)
{
    if (address == INTERRUPT_CONTROL_I_MASK)
    {
        return (m_cpu.read_irq_mask() & 0xFFFFu);
    }
    else if ((address >= m_spu.address_lo()) && (address < m_spu.address_hi()))
    {
        m_spu.read16(address);
    }
    else if (address == INTERRUPT_CONTROL_I_STAT)
    {
        return static_cast<u16>(m_cpu.read_irq_stat());
    }
    else
    {
        Util::die("Unknown 16-bit IO word read from 0x%08x", address);
    }

    return 0xFFFFu;
}

template <>
u32 Bus::io_read(paddr_t address)
{
    if (address == INTERRUPT_CONTROL_I_MASK)
    {
        return m_cpu.read_irq_mask();
    }
    else if ((address >= m_dma.address_lo()) && (address <= m_dma.address_hi()))
    {
        return m_dma.read32(address);
    }
    else if ((address >= m_gpu.address_lo()) && (address <= m_gpu.address_hi()))
    {
        return m_gpu.read32(address);
    }
    else if ((address >= m_timers.address_lo()) && (address <= m_timers.address_hi()))
    {
        return m_timers.read32(address);
    }
    else if (address == INTERRUPT_CONTROL_I_STAT)
    {
        return m_cpu.read_irq_stat();
    }
    else
    {
        Util::die("Unknown 32-bit IO word read from 0x%08x", address);
    }

    return 0xFFFFFFFFu;
}

template <>
u8 Bus::read(paddr_t address)
{
    if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        return m_ram.read8(address);
    }
    else if (is_in_io_register_region(address))
    {
        return io_read<u8>(address);
    }
    else if ((address >= m_bios.address_lo()) && (address < m_bios.address_hi()))
    {
        return m_bios.read8(address);
    }
    else if ((address >= m_expansion_rom.address_lo()) && (address < m_expansion_rom.address_hi()))
    {
        return m_expansion_rom.read8(address);
    }
    else
    {
        Util::die("Unknown 8-bit read from address 0x%08x!", address);
    }

    return 0xFF;
}

template <>
u16 Bus::read(paddr_t address)
{
    if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        return m_ram.read16(address);
    }
    else if (is_in_io_register_region(address))
    {
        return io_read<u16>(address);
    }
    else if ((address >= m_bios.address_lo()) && (address < m_bios.address_hi()))
    {
        return m_bios.read16(address);
    }
    else
    {
        m_cpu.dump_registers_to_log();
        Util::die("Unknown 16-bit read from address 0x%08x!", address);
    }

    return 0xFF;
}

template <>
u32 Bus::read(paddr_t address)
{
    if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        return m_ram.read32(address);
    }
    else if ((address >= m_bios.address_lo()) && (address < m_bios.address_hi()))
    {
        return m_bios.read32(address);
    }
    else if (is_in_io_register_region(address))
    {
        return io_read<u32>(address);
    }
    else if (address == INTERRUPT_CONTROL_I_MASK)
    {
        return m_cpu.read_irq_mask();
    }
    else
    {
        m_cpu.dump_registers_to_log();
        Util::die("Unknown 32-bit read from address 0x%08x!", address);
    }

    return 0xFF;
}

template <>
void Bus::write(paddr_t address, u8 data)
{
    if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        m_ram.write8(address, data);
    }
    else if (is_in_io_register_region(address))
    {
        io_write<u8>(address, data);
    }
    else
    {
        m_cpu.dump_registers_to_log();
        Util::die("Unknown byte store to 0x%08x!", address);
    }
}

template <>
void Bus::write(paddr_t address, u16 data)
{
    if (address == INTERRUPT_CONTROL_I_MASK)
    {
        log(LogLevel::WARN, "Ignoring 16-bit write of 0x%04x to I_STAT", data);
    }
    else if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        m_ram.write16(address, data);
    }
    else if (is_in_io_register_region(address))
    {
        io_write<u16>(address, data);
    }
    else
    {
        m_cpu.dump_registers_to_log();
        Util::die("Unknown half-word store to 0x%08x!", address);
    }
}

template <>
void Bus::write(paddr_t address, u32 data)
{
    if ((address >= m_ram.address_lo()) && (address < m_ram.address_hi()))
    {
        m_ram.write32(address, data);
    }
    else if (is_in_io_register_region(address))
    {
        io_write<u32>(address, data);
    }
    else
    {
        m_cpu.dump_registers_to_log();
        Util::die("Unknown word store to 0x%08x!", address);
    }
}

}
