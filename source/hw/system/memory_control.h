/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"

namespace PSX
{

class MemoryControl : public BusDevice
{
    static constexpr paddr_t MEMORY_CONTROL_ADDRESS_START   = 0x1F801000u;
    static constexpr paddr_t MEMORY_CONTROL_ADDRESS_END     = 0x1F801020u;
    static constexpr size_t  NUMBER_OF_REGISTERS            = 9u;

    static constexpr const char* const REGISTER_NAMES[NUMBER_OF_REGISTERS] =
    {
        "Expansion 1 Base",
        "Expansion 2 Base",
        "Expansion 1 Delay/Size",
        "Expansion 3 Delay/Size",
        "BIOS ROM Size",
        "SPU_DELAY",
        "CDROM_DELAY",
        "Expansion 2 Delay/Size",
        "COM_DELAY",
    };

public:
    MemoryControl() : BusDevice(MEMORY_CONTROL_ADDRESS_START, MEMORY_CONTROL_ADDRESS_END){}

    virtual ~MemoryControl(){}

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

private:
    u32 m_registers[NUMBER_OF_REGISTERS];
};

}
