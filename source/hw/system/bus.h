/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <types.h>
#include <memory>
#include "hw/bus_device.h"
#include "hw/cdrom/controller.h"
#include "hw/controller/io.h"
#include "hw/r3000a/r3000.h"
#include "hw/gpu/gpu.h"
#include "hw/spu/spu.h"
#include "hw/system/bios.h"
#include "hw/system/memory_control.h"
#include "hw/system/ram.h"
#include "hw/system/io/expansion_rom.h"
#include "hw/system/peripherals/dma/dma_controller.h"
#include "hw/system/peripherals/timer/timer.h"

namespace PSX
{

class Bus
{
    struct MemoryRegion
    {
        paddr_t base_address;
        size_t  length;
        std::shared_ptr<BusDevice> device;
    };

    static constexpr paddr_t PIF_ADDRESS_SPACE_START  = 0x1FC00000;
    static constexpr paddr_t PIF_ADDRESS_SPACE_END    = 0x1FC00800;

public:
    static Bus* the();

    Bus()   = default;
    ~Bus()  = default;

    bool is_in_io_register_region(paddr_t address) const;

    /**
     * @brief Dump the entire system memory to a file
     */
    void dump_system_ram() const { m_ram.dump_to_disk(); }

    // Note: All addresses at this point are considered physical!
    template <typename T>
    T read(paddr_t address);

    template <typename T>
    void write(paddr_t address, T const data);

    template <typename T>
    void io_write(paddr_t address, T const data);

    template <typename T>
    T io_read(paddr_t address);

    /**
     * @brief   Get the CPU instance
     *
     * @return  Reference to the @ref N64::VR4300 connected to the system bus
     */
    R3000& cpu() { return m_cpu; }

    /**
     * @brief   Get the GPU instance
     *
     * @return  Reference to the @ref PSX::GPU connected to the system bus
     */
    GPU& gpu() { return m_gpu; }

    /**
     * @brief   Get the CDROM Controller instance
     *
     * @return  Reference to the @ref PSX::CDROMController connected to the system bus
     */
    CDROMController& cdrom() { return m_cdrom; }

    /**
     * @brief   Get the value displayed on the POST1 seven segment display
     *
     * @return  Seven segment value
     */
    u8 post1() const { return m_post_info.post1; }

    /**
     * @brief   Get the value displayed on the POST2 seven segment display
     *
     * @return  Seven segment value
     */
    u8 post2() const { return m_post_info.post2; }

    /**
     * @brief   Raise an interrupt request ot the processor
     *
     * @param   IRQ Number to raise
     */
    void raise_irq(u8 irqn) { m_cpu.raise_irq(irqn); }

private:
    u32             m_ram_size;

private:
    R3000           m_cpu;
    BIOS            m_bios;
    SystemMemory    m_ram;
    DMAController   m_dma;
    MemoryControl   m_memory_control_1;
    POSTInfo        m_post_info;
    ExpansionRom    m_expansion_rom;
    CDROMController m_cdrom;
    ControllerIO    m_joystick;

    // Hardware Devices
    SPU             m_spu;
    GPU             m_gpu;

    // Peripherals
    Timers          m_timers;
};

}
