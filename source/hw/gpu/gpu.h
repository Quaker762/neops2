/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/gpu/gp0_fifo.h"
#include "hw/bus_device.h"
#include "hw/gpu/gpu_types.h"
#include "hw/gpu/rasterizer/rasterizer.h"
#include "hw/gpu/gpu_constants.h"

#include <array>

namespace PSX
{

// Implementation of the CXD8561CQ Graphics Processor found in the original
// Playstation. This chip was designed for Sony by TOSHIBA. It is connected
// to the main system bus and therefore, unlike the GTE, it is accessed via
// memory mapped registers and I/O ports
class GPU : public BusDevice
{
    using GP0CommandHandler = void (GPU::*)();

    static constexpr paddr_t GPU_IO_ADDRESS_START   = 0x1F801810u;
    static constexpr paddr_t GPU_IO_ADDRESS_END     = 0x1F801814u;

    static constexpr paddr_t GP0_ADDRESS            = 0x1F801810u;
    static constexpr paddr_t GP1_ADDRESS            = 0x1F801814u;
    static constexpr paddr_t GPUSTAT_ADDRESS        = 0x1F801814u;

    static constexpr size_t NUMBER_OF_GP0_COMMANDS  = 256u;

    static constexpr size_t POINTS_PER_TRIANGLE     = 3u;
    static constexpr size_t POINTS_PER_QUAD         = 4u;

public:
    GPU() : BusDevice(GPU_IO_ADDRESS_START, GPU_IO_ADDRESS_END)
    {
        create_gp0_command_table();
        //m_rasterizer.init();
    }

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

    void write_gpustat(u32 value) { m_gpustat.m_word = value; }
    GPUSTAT const gpustat() const { return m_gpustat; }

    unsigned gp0_command_number_of_arguments(u8 command) const;

    void write_gp0(u32 data);

private:
    void write_gp0_command_fifo(u32 data) { m_fifo.enqueue(data); }
    void execute_gp0_command();

    void create_gp0_command_table();

    // GP0 commands
    void gp0_nop();
    void gp0_clear_texture_cache();
    void gp0_fill_vram_rectangle();
    void gp0_copy_rectangle_to_vram();
    void gp0_copy_vram_to_cpu();
    void gp0_configure_draw_mode_setings();
    void gp0_configure_texture_window();
    void gp0_set_drawing_area_top_left();
    void gp0_set_drawing_area_bottom_right();
    void gp0_set_drawing_offset();
    void gp0_configure_mask_bit();

    void gp0_render_monochrome_quad_opaque();
    void gp0_render_gourad_triangle_opaque();
    void gp0_render_gourad_quad_opaque();
    void gp0_render_textured_quad_opaque_blending();

private:
    void execute_gp1_command(GPUGP1Command command);

    // GP1 commands
    void gp1_reset_gpu();
    void gp1_flush_fifo();
    void gp1_ack_irq();
    void gp1_enable_display(GPUGP1Command const& command);
    void gp1_set_display_mode(GPUGP1Command const& command);
    void gp1_set_display_area_start(GPUGP1Command const& command);
    void gp1_set_dma_direction(GPUGP1Command const& command);
    void gp1_set_horizontal_display_range(GPUGP1Command const& command);
    void gp1_set_vertical_display_range(GPUGP1Command const& command);

private:
    void clip_to_vram_bounds(u16& x, u16& y)
    {
        x %= VRAM_WIDTH_IN_HALFWORDS;
        y %= VRAM_HEIGHT_IN_HALFWORDS;
    }

private:
    GPUSTAT m_gpustat;
    GPURasterizer m_rasterizer;
    GPUVRAMStartRegister m_vram_start_register;
    GPUClippingRegister m_draw_area_top_left;
    GPUClippingRegister m_draw_area_bottom_right;
    GP0CommandFIFO<u32> m_fifo;
    GPUGP0Command m_current_command;

    i16 m_draw_offset_x { 0 };
    i16 m_draw_offset_y { 0 };
    u16 m_display_range_x1 { 0u };
    u16 m_display_range_x2 { 0u };
    u16 m_display_range_y1 { 0u };
    u16 m_display_range_y2 { 0u };

    uint16_t m_vram_copy_start_x { 0 };
    uint16_t m_vram_copy_start_y { 0 };
    uint16_t m_vram_copy_end_x { 0 };
    uint16_t m_vram_copy_end_y { 0 };
    uint16_t m_vram_copy_curr_x { 0 };
    uint16_t m_vram_copy_curr_y { 0 };
    uint16_t m_vram_copy_width { 0 };
    uint16_t m_vram_copy_height { 0 };
    bool m_in_vram_copy { false };

    u8 m_number_of_params_written;
    unsigned m_vram_copy_words_left;
    unsigned m_parameters_for_current_command { 0u };
    bool m_command_already_received { false };

    static std::array<GP0CommandHandler, NUMBER_OF_GP0_COMMANDS> m_gp0_command_table;
};

}
