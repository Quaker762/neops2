/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/gpu/gpu.h"
#include "hw/gpu/gpu_gp0.h"
#include "util/log.h"

namespace PSX
{

static constexpr bool DEBUG_ENABLE = false;

void GPU::create_gp0_command_table()
{
    for (auto i = 0u; i < m_gp0_command_table.size(); i++)
    {
        m_gp0_command_table.at(i) = &GPU::gp0_nop;
    }

    // Setup command jump table
    m_gp0_command_table[GP0_COMMAND_CLEAR_TEX_CACHE]                = &GPU::gp0_clear_texture_cache;
    m_gp0_command_table[GP0_COMMAND_FILL_VRAM_RECTANGLE]            = &GPU::gp0_fill_vram_rectangle;
    m_gp0_command_table[GP0_RENDER_MONOCHROME_QUAD_OPAQUE_COMMAND]  = &GPU::gp0_render_monochrome_quad_opaque;
    m_gp0_command_table[GP0_RENDER_SHADED_TRIANGLE_OPAQUE]          = &GPU::gp0_render_gourad_triangle_opaque;
    m_gp0_command_table[GP0_RENDER_GOURAD_QUAD_OPAQUE_COMMAND]      = &GPU::gp0_render_gourad_quad_opaque;
    m_gp0_command_table[GP0_RENDER_TEXTURED_QUAD_OPAQUE_W_BLENDING] = &GPU::gp0_render_textured_quad_opaque_blending;
    m_gp0_command_table[GP0_COMMAND_COPY_RECTANGLE]                 = &GPU::gp0_copy_rectangle_to_vram;
    m_gp0_command_table[GP0_COMMAND_VRAM_TO_CPU_COPY]               = &GPU::gp0_copy_vram_to_cpu;
    m_gp0_command_table[GP0_COMMAND_CONFIGURE_DRAW_MODE]            = &GPU::gp0_configure_draw_mode_setings;
    m_gp0_command_table[GP0_COMMAND_CONFIGURE_TEXTURE_WINDOW]       = &GPU::gp0_configure_texture_window;
    m_gp0_command_table[GP0_COMMAND_SET_DRAWING_AREA_TOP_LEFT]      = &GPU::gp0_set_drawing_area_top_left;
    m_gp0_command_table[GP0_COMMAND_SET_DRAWING_AREA_BOTTOM_RIGHT]  = &GPU::gp0_set_drawing_area_bottom_right;
    m_gp0_command_table[GP0_COMMAND_SET_DRAWING_OFFSET]             = &GPU::gp0_set_drawing_offset;
    m_gp0_command_table[GP0_COMMAND_CONFIGURE_MASK_BIT]             = &GPU::gp0_configure_mask_bit;
}

u8 GPU::read8(paddr_t address)
{
    Util::die("Unsupported 8-bit read from GPU @ 0x%08x", address);
}

u16 GPU::read16(paddr_t address)
{
    Util::die("Unsupported 16-bit read from GPU @ 0x%08x", address);
}

u32 GPU::read32(paddr_t address)
{
    if (address == GPUSTAT_ADDRESS)
    {
        GPUSTAT stat;

        stat.m_word = m_gpustat.m_word;

        // FIXME: Don't fuse bit 28!
        stat.bits.dma_block_receive_ready   = 1u;
        if (!m_fifo.is_full())
        {
            stat.bits.ready_for_cmd_word = 1u;
        }
        else
        {
           stat.bits.ready_for_cmd_word = 0u;
        }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
        // FIXME: Implement real GPU odd/even line calculations via some `cycle` function
        m_gpustat.bits.odd_even_line = ~m_gpustat.bits.odd_even_line;
#pragma GCC diagnostic pop

        return stat.m_word;
    }
    else
    {
        log(LogLevel::WARN, "Unsupported 32-bit read from GPU @ 0x%08x", address);
        return 0u;
    }
}

void GPU::write8(paddr_t address, u8 data)
{
    Util::die("Unsupported 8-bit write of 0x%02x to GPU @ 0x%08x", data, address);
}

void GPU::write16(paddr_t address, u16 data)
{
    Util::die("Unsupported 16-bit write of 0x%04x to GPU @ 0x%08x", data, address);
}

void GPU::write32(paddr_t address, u32 data)
{
    if (address == GP0_ADDRESS)
    {
        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "GPU GP0 Command: 0x%08x", data);
        }

        write_gp0(data);
    }
    else if (address == GP1_ADDRESS)
    {
        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "GPU GP1 Command: 0x%08x", data);
        }

        execute_gp1_command(GPUGP1Command { .m_word = data });
    }
    else
    {
        log(LogLevel::WARN, "Unsupported 32-bit write of 0x%08x to GPU @ 0x%08x", data, address);
    }
}

}
