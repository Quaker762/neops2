/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/gpu/gpu.h"
#include "hw/gpu/rasterizer/triangle.h"
#include "hw/gpu/rasterizer/quad.h"

namespace PSX {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wunused-variable"

void GPU::gp0_render_monochrome_quad_opaque()
{
    Quad quad;
    auto color = m_fifo.dequeue();

    for (auto i = 0u; i < POINTS_PER_QUAD; i++)
    {
        quad.vertices[i].position.point = m_fifo.dequeue();
    }

    quad.vertices[0].color.color = color;
    quad.vertices[1].color.color = color;
    quad.vertices[2].color.color = color;
    quad.vertices[3].color.color = color;

    Triangle triangle1;
    Triangle triangle2;

    triangle1.vertices[0] = quad.vertices[0];
    triangle1.vertices[1] = quad.vertices[1];
    triangle1.vertices[2] = quad.vertices[2];

    triangle2.vertices[0] = quad.vertices[1];
    triangle2.vertices[1] = quad.vertices[2];
    triangle2.vertices[2] = quad.vertices[3];

    // Scan out both triangles
    bool should_dither = (m_gpustat.bits.dither_24_to_15_bit == 1u);
    m_rasterizer.draw_triangle_shaded(triangle1, should_dither);
    m_rasterizer.draw_triangle_shaded(triangle2, should_dither);
}

void GPU::gp0_render_gourad_triangle_opaque()
{
    Triangle triangle;
    for (auto i = 0u; i < POINTS_PER_TRIANGLE; i++)
    {
        triangle.vertices[i].color.color    = m_fifo.dequeue();
        triangle.vertices[i].position.point = m_fifo.dequeue();
    }

    bool should_dither = (m_gpustat.bits.dither_24_to_15_bit == 1u);
    m_rasterizer.draw_triangle_shaded(triangle, should_dither);
}

void GPU::gp0_render_gourad_quad_opaque()
{
    Quad quad;

    for (auto i = 0u; i < POINTS_PER_QUAD; i++)
    {
        quad.vertices[i].color.color    = m_fifo.dequeue();
        quad.vertices[i].position.point = m_fifo.dequeue();
    }

    Triangle triangle1;
    Triangle triangle2;

    triangle1.vertices[0] = quad.vertices[0];
    triangle1.vertices[1] = quad.vertices[1];
    triangle1.vertices[2] = quad.vertices[2];

    triangle2.vertices[0] = quad.vertices[1];
    triangle2.vertices[1] = quad.vertices[2];
    triangle2.vertices[2] = quad.vertices[3];

    // Scan out both triangles
    bool should_dither = (m_gpustat.bits.dither_24_to_15_bit == 1u);
    m_rasterizer.draw_triangle_shaded(triangle1, should_dither);
    m_rasterizer.draw_triangle_shaded(triangle2, should_dither);
}

void GPU::gp0_render_textured_quad_opaque_blending()
{
    Quad quad;
    u32 tex_coords_raw[4];
    GPUTexCoord tex_coords[4];
    TexCLUTPoint clut;
    TexPage texpage;

    (void)m_fifo.dequeue(); // Command/Color is unused for a textured quad

    for (auto i = 0u; i < POINTS_PER_QUAD; i++)
    {
        quad.vertices[i].position.point = m_fifo.dequeue();
        tex_coords_raw[i]               = m_fifo.dequeue();
    }

    // Extract the extra information from the commands
    clut.m_word             = static_cast<u16>(tex_coords_raw[0] >> 16u);
    texpage.m_word         = static_cast<u16>(tex_coords_raw[1] >> 16u);
    tex_coords[0].m_word    = static_cast<u16>(tex_coords_raw[0] & 0xFFFFu);
    tex_coords[1].m_word    = static_cast<u16>(tex_coords_raw[1] & 0xFFFFu);
    tex_coords[2].m_word    = static_cast<u16>(tex_coords_raw[2] & 0xFFFFu);
    tex_coords[3].m_word    = static_cast<u16>(tex_coords_raw[3] & 0xFFFFu);

    Triangle triangle1;

    triangle1.vertices[0]           = quad.vertices[0];
    triangle1.vertices[0].texcoord  = tex_coords[0];
    triangle1.vertices[1]           = quad.vertices[1];
    triangle1.vertices[1].texcoord  = tex_coords[1];
    triangle1.vertices[2]           = quad.vertices[2];
    triangle1.vertices[2].texcoord  = tex_coords[2];
    triangle1.texpage               = texpage;
    triangle1.clut                  = clut;

    Triangle triangle2;

    triangle2.vertices[0]           = quad.vertices[1];
    triangle2.vertices[0].texcoord  = tex_coords[1];
    triangle2.vertices[1]           = quad.vertices[2];
    triangle2.vertices[1].texcoord  = tex_coords[2];
    triangle2.vertices[2]           = quad.vertices[3];
    triangle2.vertices[2].texcoord  = tex_coords[3];
    triangle2.texpage               = texpage;
    triangle2.clut                  = clut;

    // Get the texture lookup type for this render phase
    GPURasterizer::TexelLookupType lookup_type;
    switch (m_gpustat.bits.texture_page_colors)
    {
    case 0u:
        lookup_type = GPURasterizer::TexelLookupType::Tex4BitCLUT;
        break;
    case 1u:
        lookup_type = GPURasterizer::TexelLookupType::Tex8BitCLUT;
        break;
    case 2u:
        lookup_type = GPURasterizer::TexelLookupType::Tex15BitDirect;
        break;
    default:
        Util::die("Unknown texel format!");
    }

    m_rasterizer.draw_triangle_textured(triangle1, lookup_type);
    m_rasterizer.draw_triangle_textured(triangle2, lookup_type);
}

#pragma GCC diagnostic pop

}


