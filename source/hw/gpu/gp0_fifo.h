/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <queue>
#include <cstddef>
#include "util/log.h"

namespace PSX
{

template <typename T>
class GP0CommandFIFO
{
    static constexpr size_t NUMBER_OF_WORD_ENTRIES = 16u;

public:
    void enqueue(T const& value)
    {
        if (is_full())
        {
            log(LogLevel::ERROR, "GP0 Command FIFO full! Fifo size: %llu", m_fifo.size());
            for (auto i = 0u; i < m_fifo.size(); i++)
            {
                log(LogLevel::ERROR, "\t0x%08x", dequeue());
            }

            Util::die("Killing program!");
            return;
        }

        m_fifo.push(value);
    }

    T dequeue()
    {
        T value = m_fifo.front();
        m_fifo.pop();

        return value;
    }

    bool is_full() const { return m_fifo.size() == NUMBER_OF_WORD_ENTRIES; }
    void flush() { while(!m_fifo.empty()) { m_fifo.pop(); } }

private:
    std::queue<T> m_fifo;
};

}
