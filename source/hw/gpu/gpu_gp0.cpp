/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/gpu/gpu.h"
#include "hw/gpu/gpu_gp0.h"
#include "emu/display.h"

namespace PSX {

static constexpr bool DEBUG_ENABLE = false;

std::array<GPU::GP0CommandHandler, GPU::NUMBER_OF_GP0_COMMANDS> GPU::m_gp0_command_table;

void GPU::gp0_nop()
{
    log(LogLevel::WARN, "GP0 NOP (unimplemented command?)");
}

void GPU::gp0_clear_texture_cache()
{
    log(LogLevel::WARN, "TODO: Implement texture cache!");
}

void GPU::gp0_fill_vram_rectangle()
{
    GPUColor    color;
    VRAMColor   vram_color;

    color.color             = m_fifo.dequeue();
    u8 r, g, b;

    r = color.components.r;
    g = color.components.g;
    b = color.components.b;
    r >>= 3u;
    g >>= 3u;
    b >>= 3u;
    vram_color.colors.r = r & 0x1Fu;
    vram_color.colors.g = g & 0x1Fu;
    vram_color.colors.b = b & 0x1Fu;
    vram_color.colors.a = 0u;


    u32 top_left_corner     = m_fifo.dequeue();
    u32 width_and_height    = m_fifo.dequeue();

    u16 top_left_x  = top_left_corner & 0xFFFFu;
    u16 top_left_y  = static_cast<u16>((top_left_corner >> 16u) & 0xFFFFu);
    u16 width       = width_and_height & 0xFFFFu;
    u16 height      = static_cast<u16>((width_and_height >> 16u) & 0xFFFFu);

    // FIXME: Research if there's a way to do this via SIMD!
    for (auto y = top_left_y; y < height; y++)
    {
        for (auto x = top_left_x; x < width; x++)
        {
            m_rasterizer.vram().write(x, y, vram_color.m_color);
        }
    }
}

void GPU::gp0_copy_rectangle_to_vram()
{
    (void)m_fifo.dequeue();

    auto destination [[maybe_unused]]   = m_fifo.dequeue();
    auto width_and_height               = m_fifo.dequeue();

    u16 x_start = destination & 0xFFFFu;
    u16 y_start = static_cast<u16>(destination >> 16u);
    u16 width   = width_and_height & 0xFFFFu;
    u16 height  = static_cast<u16>(width_and_height >> 16u);

    u32 size_in_half_words = width * height;

    size_in_half_words = ((size_in_half_words + 1u) & ~1u);

    m_vram_copy_words_left  = (size_in_half_words / 2u);
    m_vram_copy_width       = ((width - 1u) & 0x3FFu) + 1u;
    m_vram_copy_height      = ((height - 1u) & 0x1FFu) + 1u;
    m_vram_copy_start_x = m_vram_copy_curr_x = x_start & 0x3ff;
    m_vram_copy_start_y = m_vram_copy_curr_y = y_start & 0x1ff;
    m_vram_copy_end_x = m_vram_copy_start_x + m_vram_copy_width;
    m_vram_copy_end_y = m_vram_copy_start_y + m_vram_copy_height;

    m_in_vram_copy = true;
}

void GPU::gp0_copy_vram_to_cpu()
{
    (void)m_fifo.dequeue();

    auto source_coord       = m_fifo.dequeue();
    auto width_and_height   = m_fifo.dequeue();

    log(LogLevel::INFO, "VRAM->CPU Copy: source_coord = 0x%08x, width_and_height = 0x%08x", source_coord, width_and_height);
    m_gpustat.bits.vram_to_cpu_send_ready = 1u;
}

void GPU::gp0_configure_draw_mode_setings()
{
    m_gpustat.bits.texture_page_x_base  = m_current_command.configure_draw_mode_command.tex_page_x_base;
    m_gpustat.bits.texture_page_y_base  = m_current_command.configure_draw_mode_command.tex_page_y_base;
    m_gpustat.bits.semi_transparency    = m_current_command.configure_draw_mode_command.semi_transparency;
    m_gpustat.bits.texture_page_colors  = m_current_command.configure_draw_mode_command.texture_page_colors;
    m_gpustat.bits.dither_24_to_15_bit  = m_current_command.configure_draw_mode_command.dither_24_to_15_bit;
    m_gpustat.bits.display_enable       = m_current_command.configure_draw_mode_command.can_draw_to_display_area;
    m_gpustat.bits.texture_disable      = m_current_command.configure_draw_mode_command.texture_disable;

    // TODO: Work out what x and y flip does!
}

void GPU::gp0_configure_texture_window()
{
    log(LogLevel::WARN, "TODO: Implement gp0_configure_texture_window()!");
}

void GPU::gp0_set_drawing_area_top_left()
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "GPU: Setting top left draw area to %u,%u", m_current_command.drawing_area_command.x, m_current_command.drawing_area_command.y);
    }

    m_draw_area_top_left.x = m_current_command.drawing_area_command.x;
    m_draw_area_top_left.y = m_current_command.drawing_area_command.y;
}

void GPU::gp0_set_drawing_area_bottom_right()
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "GPU: Setting bottom right draw area to %u,%u", m_current_command.drawing_area_command.x, m_current_command.drawing_area_command.y);
    }

    m_draw_area_bottom_right.x = m_current_command.drawing_area_command.x;
    m_draw_area_bottom_right.y = m_current_command.drawing_area_command.y;
}

void GPU::gp0_set_drawing_offset()
{
    u16 x_offset = m_current_command.drawing_offset_command.x_offset;
    u16 y_offset = m_current_command.drawing_offset_command.y_offset;

    m_draw_offset_x = (static_cast<i16>((x_offset << 5u)) >> 5u);
    m_draw_offset_y = (static_cast<i16>((y_offset << 5u)) >> 5u);

    Display::the().redraw(m_rasterizer.vram());
}

void GPU::gp0_configure_mask_bit()
{
    m_gpustat.bits.set_mask_when_drawing    = m_current_command.configure_mask_command.set_mask_while_drawing;
    m_gpustat.bits.draw_pixels              = m_current_command.configure_mask_command.check_mask_before_draw;
}

void GPU::execute_gp0_command()
{
    auto command_byte = m_current_command.command.byte;
    (this->*m_gp0_command_table[command_byte])();
}

void GPU::write_gp0(u32 data)
{
    if (m_in_vram_copy)
    {
        u16 texel_a = static_cast<u16>(data & 0xFFFFu);
        clip_to_vram_bounds(m_vram_copy_curr_x, m_vram_copy_curr_y);
        m_rasterizer.m_vram.write(m_vram_copy_curr_x++, m_vram_copy_curr_y, texel_a);
        if (m_vram_copy_curr_x >= m_vram_copy_start_x + m_vram_copy_width)
        {
            m_vram_copy_curr_x = m_vram_copy_start_x;
            m_vram_copy_curr_y++;
        }

        u16 texel_b = static_cast<u16>((data >> 16u) & 0xFFFFu);
        clip_to_vram_bounds(m_vram_copy_curr_x, m_vram_copy_curr_y);
        m_rasterizer.m_vram.write(m_vram_copy_curr_x++, m_vram_copy_curr_y, texel_b);
        if (m_vram_copy_curr_x >= m_vram_copy_start_x + m_vram_copy_width)
        {
            m_vram_copy_curr_x = m_vram_copy_start_x;
            m_vram_copy_curr_y++;
        }

        m_vram_copy_words_left--;
        if (m_vram_copy_words_left == 0u)
        {
            m_in_vram_copy = false;
        }

        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "Got tex word, 0x%08x, words left = %u", data, m_vram_copy_words_left);
        }

        return;
    }

    if (!m_command_already_received)
    {
        unsigned number_of_parameters = gp0_command_number_of_arguments(static_cast<u8>((data >> 24u)));
        m_current_command = data;

        if (number_of_parameters == 0u)
        {
            if constexpr (DEBUG_ENABLE)
            {
                log(LogLevel::INFO, "Executing command GP0(0x%02x)", m_current_command.command.byte);
            }

            execute_gp0_command();
            m_current_command = 0u;
        }
        else
        {
            m_command_already_received          = true;
            m_parameters_for_current_command    = number_of_parameters;

            m_fifo.enqueue(data);   // Write the data into the FIFO
        }
    }
    else
    {
        m_number_of_params_written++;   // Increment the number of parameters we've received
        m_fifo.enqueue(data);           // Write the data into the FIFO
        if (m_number_of_params_written == m_parameters_for_current_command)
        {
            if constexpr (DEBUG_ENABLE)
            {
                log(LogLevel::INFO, "Executing multi-param command GP0(0x%02x)", m_current_command.command.byte);
            }

            execute_gp0_command();
            m_current_command                   = 0u;
            m_command_already_received          = false;
            m_parameters_for_current_command    = 0u;
            m_number_of_params_written          = 0u;
        }
    }
}

unsigned GPU::gp0_command_number_of_arguments(u8 command) const
{
    unsigned number_of_arguments = 0u;

    switch (command)
    {
    case GP0_COMMAND_NOP:
    case GP0_COMMAND_CLEAR_TEX_CACHE:
    // These commands are executed immediately (not clocked into the FIFO)
    case GP0_COMMAND_SET_DRAWING_AREA_TOP_LEFT:
    case GP0_COMMAND_SET_DRAWING_AREA_BOTTOM_RIGHT:
    case GP0_COMMAND_SET_DRAWING_OFFSET:

    // Apparently these commands DO consume FIFO space, however it has no parameters? Perhaps because
    // they write to GPUSTAT? Investigate this..
    case GP0_COMMAND_CONFIGURE_DRAW_MODE:
    case GP0_COMMAND_CONFIGURE_TEXTURE_WINDOW:
    case GP0_COMMAND_CONFIGURE_MASK_BIT:
        break;
    case GP0_COMMAND_COPY_RECTANGLE:
    case GP0_COMMAND_VRAM_TO_CPU_COPY:
    case GP0_COMMAND_FILL_VRAM_RECTANGLE:
        number_of_arguments = 2u;
        break;
    case GP0_RENDER_MONOCHROME_QUAD_OPAQUE_COMMAND:
        number_of_arguments = 4u;
        break;
    case GP0_RENDER_SHADED_TRIANGLE_OPAQUE:
        number_of_arguments = 5u;
        break;
    case GP0_RENDER_GOURAD_QUAD_OPAQUE_COMMAND:
        number_of_arguments = 7u;
        break;
    case GP0_RENDER_TEXTURED_QUAD_OPAQUE_W_BLENDING:
        number_of_arguments = 8u;
        break;
    default:
        Util::die("Could not get the number of commands for 0x%02x!", command);
    }

    return number_of_arguments;
}

}
