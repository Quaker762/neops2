/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

union GPUSTAT
{
    struct
    {
        u32 texture_page_x_base     : 4;
        u32 texture_page_y_base     : 1;
        u32 semi_transparency       : 2;
        u32 texture_page_colors     : 2;
        u32 dither_24_to_15_bit     : 1;
        u32 draw_area_allowed       : 1;
        u32 set_mask_when_drawing   : 1;
        u32 draw_pixels             : 1;
        u32 interlace_field         : 1;
        u32 reverse_flag            : 1;
        u32 texture_disable         : 1;
        u32 horizontal_resolution2  : 1;
        u32 horizontal_resolution1  : 2;
        u32 vertical_resolution     : 1;
        u32 video_mode              : 1;
        u32 display_color_depth     : 1;
        u32 vertical_interlace      : 1;
        u32 display_enable          : 1;
        u32 irq                     : 1;
        u32 dma_flag                : 1;    // Check meanings on no$psx doc!
        u32 ready_for_cmd_word      : 1;
        u32 vram_to_cpu_send_ready  : 1;
        u32 dma_block_receive_ready : 1;
        u32 dma_direction           : 2;
        u32 odd_even_line           : 1;
    } bits;

    u32 m_word;
};

struct GPUClippingRegister
{
    u32 x       : 10u;
    u32 y       : 9u;
    u32 unused  : 23u;
};

struct GPUVRAMStartRegister
{
    u32 x       : 10u;  // Halfword address in VRAM
    u32 y       : 9u;   // Scanline nuber in VRAM
    u32 unused  : 23u;
};

union GPUGP0Command
{
    struct
    {
        u32 unused  : 24u;
        u32 byte    : 8u;
    } command;

    struct
    {
        u32 tex_page_x_base             : 4u;
        u32 tex_page_y_base             : 1u;
        u32 semi_transparency           : 2u;
        u32 texture_page_colors         : 2u;
        u32 dither_24_to_15_bit         : 1u;
        u32 can_draw_to_display_area    : 1u;
        u32 texture_disable             : 1u;
        u32 texture_rect_x_flip         : 1u;
        u32 texture_rect_y_flip         : 1u;
    } configure_draw_mode_command;

    struct
    {
        u32 x : 10u;
        u32 y : 9u;
    } drawing_area_command;

    struct
    {
        u32 x_offset : 11u;
        u32 y_offset : 11u;
    } drawing_offset_command;

    struct
    {
        u32 set_mask_while_drawing  : 1u;
        u32 check_mask_before_draw  : 1u;
    } configure_mask_command;

    u32 m_word;

    GPUGP0Command() = default;
    GPUGP0Command(u32 data) : m_word(data){}
    void operator=(u32 const data) { m_word = data; }
};


union GPUGP1Command
{
    struct
    {
        u32 unused  : 24u;
        u32 byte    : 8u;
    } command;

    struct
    {
        u32 display_flag    : 1u;
        u32 unused          : 31u;
    } display_on_off_command;

    struct
    {
        u32 horizontal_resolution1  : 2u;
        u32 vertical_resolution     : 1u;
        u32 video_mode              : 1u;
        u32 display_color_depth     : 1u;
        u32 vertical_interlace      : 1u;
        u32 horizontal_resolution2  : 1u;
        u32 reverse_flag            : 1u;
    } display_mode_command;

    struct
    {
        u32 dma_direction : 2u;
    } dma_direction_command;

    struct
    {
        u32 x       : 10u;
        u32 y       : 9u;
    } display_area_start_command;

    struct
    {
        u16 x1 : 12u;
        u16 x2 : 12u;
    } horizontal_display_command;

    struct
    {
        u16 y1 : 10u;
        u16 y2 : 10u;
    } vertical_display_command;

    u32 m_word;
};

union GPUPoint
{
    struct
    {
        u16 x;
        u16 y;
    } coords;

    u32 point;
};

union GPUColor
{
    struct
    {
        u8 r;
        u8 g;
        u8 b;
    } components;

    u32 color;
};

union GPUTexCoord
{
    struct
    {
        u16 u : 8u;
        u16 v : 8u;
    } coords;

    u16 m_word;
};

union TexPage
{
    struct
    {
        u16 x_base              : 4u;
        u16 y_base              : 1u;
        u16 semi_transparency   : 2u;
        u16 texpage_colors      : 2u;
        u16 unused              : 2u;
        u16 texture_disable     : 1u;
        u16 unused2             : 4u;
    } bits;

    u16 m_word;
};

union TexCLUTPoint
{
    struct
    {
        u16 x_in_16_halword_steps   : 6u;
        u16 y                       : 9u;
    } coords;

    u16 m_word;
};


static_assert(sizeof(GPUGP1Command) == sizeof(u32));

}
