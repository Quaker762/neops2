/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <utility>

#include "hw/gpu/rasterizer/rasterizer.h"
#include "hw/gpu/rasterizer/edge.h"
#include "util/log.h"

namespace PSX
{

void GPURasterizer::rasterize_triangle_gouraud(Triangle const& triangle, TriangleBoundingBox const& bounding_box, unsigned triangle_area, bool should_dither)
{
    auto const& v0 = triangle.vertices[0];
    auto const& v1 = triangle.vertices[1];
    auto const& v2 = triangle.vertices[2];

    Edge edge0(v1.position, v2.position);
    Edge edge1(v2.position, v0.position);
    Edge edge2(v0.position, v1.position);

    GPUPoint scan_point;
    scan_point.coords.x = bounding_box.min_x;
    scan_point.coords.y = bounding_box.min_y;

    auto weight0_row_start = edge0.evaluate_for_point(scan_point);
    auto weight1_row_start = edge1.evaluate_for_point(scan_point);
    auto weight2_row_start = edge2.evaluate_for_point(scan_point);

    bool was_rendering_row = false;
    for (auto y = bounding_box.min_y; y < bounding_box.max_y; y++)
    {
        auto weight0 = weight0_row_start;
        auto weight1 = weight1_row_start;
        auto weight2 = weight2_row_start;

        for (auto x = bounding_box.min_x; x < bounding_box.max_x; x++)
        {
            if ((weight0 >= 0) && (weight1 >= 0) && (weight2 >= 0))
            {
                was_rendering_row = true;
                auto r = ((v0.color.components.r * static_cast<unsigned>(weight0)) +
                          (v1.color.components.r * static_cast<unsigned>(weight1)) +
                          (v2.color.components.r * static_cast<unsigned>(weight2))) / triangle_area;

                auto g = ((v0.color.components.g * static_cast<unsigned>(weight0)) +
                          (v1.color.components.g * static_cast<unsigned>(weight1)) +
                          (v2.color.components.g * static_cast<unsigned>(weight2))) / triangle_area;

                auto b = ((v0.color.components.b * static_cast<unsigned>(weight0)) +
                          (v1.color.components.b * static_cast<unsigned>(weight1)) +
                          (v2.color.components.b * static_cast<unsigned>(weight2))) / triangle_area;

                blit_pixel(x, y, static_cast<u8>(r), static_cast<u8>(g), static_cast<u8>(b), should_dither);
            }
            else
            {
                // Were we rendering a row?
                if (was_rendering_row)
                {
                    // Okay, we were, and now we're not. Let's abort the rest of this row
                    // as we've run out of triangle to render.
                    break;
                }
            }

            weight0 += edge0.a();
            weight1 += edge1.a();
            weight2 += edge2.a();
        }

        was_rendering_row = false;

        weight0_row_start += edge0.b();
        weight1_row_start += edge1.b();
        weight2_row_start += edge2.b();
    }
}

void GPURasterizer::rasterize_triangle_textured(Triangle const& triangle, TriangleBoundingBox const& bounding_box, unsigned triangle_area, TexelLookupType lookup_type)
{
    auto const& v0 = triangle.vertices[0];
    auto const& v1 = triangle.vertices[1];
    auto const& v2 = triangle.vertices[2];

    Edge edge0(v1.position, v2.position);
    Edge edge1(v2.position, v0.position);
    Edge edge2(v0.position, v1.position);

    GPUPoint scan_point;
    scan_point.coords.x = bounding_box.min_x;
    scan_point.coords.y = bounding_box.min_y;

    auto weight0_row_start = edge0.evaluate_for_point(scan_point);
    auto weight1_row_start = edge1.evaluate_for_point(scan_point);
    auto weight2_row_start = edge2.evaluate_for_point(scan_point);

    bool was_rendering_row = false;
    for (auto y = bounding_box.min_y; y < bounding_box.max_y; y++)
    {
        auto weight0 = weight0_row_start;
        auto weight1 = weight1_row_start;
        auto weight2 = weight2_row_start;

        for (auto x = bounding_box.min_x; x < bounding_box.max_x; x++)
        {
            if ((weight0 >= 0) && (weight1 >= 0) && (weight2 >= 0))
            {
                was_rendering_row = true;
                auto u = ((v0.texcoord.coords.u * static_cast<unsigned>(weight0)) +
                          (v1.texcoord.coords.u * static_cast<unsigned>(weight1)) +
                          (v2.texcoord.coords.u * static_cast<unsigned>(weight2))) / triangle_area;

                auto v = ((v0.texcoord.coords.v * static_cast<unsigned>(weight0)) +
                          (v1.texcoord.coords.v * static_cast<unsigned>(weight1)) +
                          (v2.texcoord.coords.v * static_cast<unsigned>(weight2))) / triangle_area;

                u8 index;
                VRAMColor color;
                switch (lookup_type)
                {
                case TexelLookupType::Tex4BitCLUT:
                    index = lookup_texel_4bit(static_cast<u8>(u), static_cast<u8>(v), triangle.texpage);
                    color = clut_lookup(index, triangle.clut);
                    break;
                case TexelLookupType::Tex8BitCLUT:
                case TexelLookupType::Tex15BitDirect:
                default:
                    Util::die("Unsupported texel lookup type!");
                }

                // According to No$cash, a texel of 0x0000 is transparent.
                // As this is the case, any fragment we encounter that is pure black we
                // discard from rendition.
                if (color.m_color != 0u)
                {
                    // Do a direct VRAM write
                    m_vram.write(x, y, color.m_color);
                }
            }
            else
            {
                // Were we rendering a row?
                if (was_rendering_row)
                {
                    // Okay, we were, and now we're not. Let's abort the rest of this row
                    // as we've run out of triangle to render.
                    break;
                }
            }

            weight0 += edge0.a();
            weight1 += edge1.a();
            weight2 += edge2.a();
        }

        was_rendering_row = false;

        weight0_row_start += edge0.b();
        weight1_row_start += edge1.b();
        weight2_row_start += edge2.b();
    }
}


void GPURasterizer::draw_triangle_shaded(Triangle& triangle, bool should_dither)
{
    auto area = triangle.area();
    auto const bounding_box = get_triangle_bounding_box(triangle);

    if (area > 0)
    {
        std::swap(triangle.vertices[1], triangle.vertices[2]);
    }

    rasterize_triangle_gouraud(triangle, bounding_box, static_cast<unsigned>(std::abs(area)), should_dither);
}

void GPURasterizer::draw_triangle_textured(Triangle& triangle, TexelLookupType lookup_type)
{
    auto area = triangle.area();
    auto const bounding_box = get_triangle_bounding_box(triangle);

    if (area > 0)
    {
        std::swap(triangle.vertices[1], triangle.vertices[2]);
    }

    rasterize_triangle_textured(triangle, bounding_box, static_cast<unsigned>(std::abs(area)), lookup_type);
}

}
