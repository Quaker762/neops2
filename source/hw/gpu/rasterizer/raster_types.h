/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

static constexpr size_t VERTICES_PER_TRIANGLE   = 3u;
static constexpr size_t VERTICES_PER_QUAD       = 4u;
static constexpr size_t TRIANGLES_PER_QUAD      = 2u;

struct RasterPoint
{
    u16 x;
    u16 y;
};


}
