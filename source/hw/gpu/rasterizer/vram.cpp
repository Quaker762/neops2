/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include "hw/gpu/rasterizer/vram.h"

namespace PSX
{

void VRAM::write(unsigned x, unsigned y, u16 const data)
{
    m_vram.data()[x + (y * VRAM_WIDTH_IN_HALFWORDS)] = data;
}

u16 VRAM::read(unsigned x, unsigned y) const
{
    return m_vram.data()[x + (y * VRAM_WIDTH_IN_HALFWORDS)];
}

void VRAM::dump_to_file() const
{
    char const* const data = reinterpret_cast<char const*>(m_vram.data());
    static int index = 0;
    std::fstream file;

    std::string filename = "vram.bin";
    filename.append(std::to_string(index++));
    file.open(filename, std::ios::out | std::ios::binary);
    file.write(reinterpret_cast<char const*>(data), VRAM_SIZE_IN_HALF_WORDS * sizeof(u16));
    file.close();
}

}
