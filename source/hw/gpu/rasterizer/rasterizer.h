/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/gpu/rasterizer/triangle.h"
#include "hw/gpu/rasterizer/vram.h"

namespace PSX
{

class GPURasterizer
{
    friend class GPU;   // The GPU has full access over the scope of rasterizer, as it's the Top Level Entity

    struct TriangleBoundingBox
    {
        u16 min_x;
        u16 min_y;
        u16 max_x;
        u16 max_y;
    };

public:
    enum class TexelLookupType
    {
        Tex4BitCLUT,
        Tex8BitCLUT,
        Tex15BitDirect
    };

public:
    GPURasterizer() = default;
    ~GPURasterizer() = default;

    void draw_triangle_shaded(Triangle& triangle, bool should_dither);
    void draw_triangle_textured(Triangle& triangle, TexelLookupType lookup_type);

private:
    void rasterize_triangle_gouraud(Triangle const& triangle, TriangleBoundingBox const& bounding_box, unsigned triangle_area, bool should_dither);
    void rasterize_triangle_textured(Triangle const& triangle, TriangleBoundingBox const& bounding_box, unsigned triangle_area, TexelLookupType lookup_type);

    bool is_point_inside_triangle(Triangle const& triangle);
    i32 edge_equation(GPUPoint const& a, GPUPoint const& b, GPUPoint const& c) const;
    TriangleBoundingBox get_triangle_bounding_box(Triangle const& triangle) const;

    void blit_pixel(u16 x, u16 y, u8 r, u8 g, u8 b, bool should_dither);

    u8 lookup_texel_4bit(u8 u, u8 v, TexPage const& texpage);
    VRAMColor clut_lookup(u8 index, TexCLUTPoint const& clut);

    VRAM& vram() { return m_vram; }
    VRAM const& vram() const { return m_vram; }

private:
    VRAM m_vram;
};

}
