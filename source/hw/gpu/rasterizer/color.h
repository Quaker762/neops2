/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

// Thanks Duckstation! https://github.com/stenzek/duckstation/blob/b530b08bc4f6dd61294c3bc9650b04db1cc0f8e7/src/core/gpu_types.h

namespace PSX
{

__attribute__((always_inline)) static constexpr u32 RGBA5551ToRGBA8888 (u16 color)
{
    // Helper/format conversion functions - constants from https://stackoverflow.com/a/9069480
    #define E5TO8(color) ((((color) * 527u) + 23u) >> 6)

    const u32 r = E5TO8(color & 31u);
    const u32 g = E5TO8((color >> 5) & 31u);
    const u32 b = E5TO8((color >> 10) & 31u);
    const u32 a = ((color >> 15) != 0) ? 255 : 0;
    return r | (g << 8) | (b << 16) | (a << 24);

    #undef E5TO8
}

__attribute__((always_inline)) static constexpr u16 RGBA8888ToRGB5551 [[maybe_unused]] (u32 color)
{
    u16 const r = (color & 0xFFu);
    u16 const g = ((color >> 8u) & 0xFFu);
    u16 const b = ((color >> 16u) & 0xFFu);
    u16 const a = ((color >> 24u) & 0x1u);

    return static_cast<u16>((r | (g << 5u) | (b << 10u) | (a << 15u)));
}

union VRAMColor
{
    struct
    {
        u16 r : 5u;
        u16 g : 5u;
        u16 b : 5u;
        u16 a : 1u;
    } colors;

    u16 m_color;
};

}
