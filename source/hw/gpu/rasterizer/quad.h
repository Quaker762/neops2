/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/gpu/rasterizer/vertex.h"
#include "hw/gpu/rasterizer/raster_types.h"

namespace PSX
{

struct Quad
{
    GPUVertex vertices[VERTICES_PER_QUAD];

    signed area() const
    {
        auto const& vertex_a = vertices[0].position;
        auto const& vertex_b = vertices[1].position;
        auto const& vertex_c = vertices[2].position;

        return (((vertex_c.coords.x - vertex_a.coords.x) * (vertex_b.coords.y - vertex_a.coords.y)) -
                ((vertex_c.coords.y - vertex_a.coords.y) * (vertex_b.coords.x - vertex_a.coords.x)));
    }
};

}
