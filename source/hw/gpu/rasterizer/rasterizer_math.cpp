/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/gpu/rasterizer/rasterizer.h"

namespace PSX
{

GPURasterizer::TriangleBoundingBox GPURasterizer::get_triangle_bounding_box(Triangle const& triangle) const
{
    TriangleBoundingBox bounding_box;

    bounding_box.min_x = std::min(std::min(triangle.vertices[0].position.coords.x, triangle.vertices[1].position.coords.x), triangle.vertices[2].position.coords.x);
    bounding_box.min_y = std::min(std::min(triangle.vertices[0].position.coords.y, triangle.vertices[1].position.coords.y), triangle.vertices[2].position.coords.y);
    bounding_box.max_x = std::max(std::max(triangle.vertices[0].position.coords.x, triangle.vertices[1].position.coords.x), triangle.vertices[2].position.coords.x);
    bounding_box.max_y = std::max(std::max(triangle.vertices[0].position.coords.y, triangle.vertices[1].position.coords.y), triangle.vertices[2].position.coords.y);

    return bounding_box;
}

i32 GPURasterizer::edge_equation(GPUPoint const& a, GPUPoint const& b, GPUPoint const& c) const
{
    return (((c.coords.x - a.coords.x) * (b.coords.y - a.coords.y)) - ((c.coords.y - a.coords.y) * (b.coords.x - a.coords.x)));
}

}

