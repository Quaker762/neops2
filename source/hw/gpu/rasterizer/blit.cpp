/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>

#include "hw/gpu/rasterizer/rasterizer.h"

namespace PSX
{

void GPURasterizer::blit_pixel(u16 x, u16 y, u8 r, u8 g, u8 b, bool should_dither)
{
    // psx-spx.consoledev.net/graphicsprocessingunitgpu
    // "For dithering, VRAM is broken to 4x4 pixel blocks, depending on the location in that 4x4 pixel region, the corresponding
    // dither offset is added to the 8bit R/G/B values, the result is saturated to +00h..+FFh, and then divided by 8,
    // resulting in the final 5bit R/G/B values."
    static int dither_table[4u][4u] =
    {
        {-4, 0, -3, 1},
        {2, -2, 3, -1},
        {-3, 1, -4, 0},
        {3, -1, 2, -2}
    };

    VRAMColor write_color;

    if (should_dither)
    {
        auto r_as_int = static_cast<int>(r);
        auto g_as_int = static_cast<int>(g);
        auto b_as_int = static_cast<int>(b);
        auto dither_offset = dither_table[y & 3u][x & 3u];
        auto r_dithered = std::clamp(r_as_int + dither_offset, 0, 0xFF);
        auto g_dithered = std::clamp(g_as_int + dither_offset, 0, 0xFF);
        auto b_dithered = std::clamp(b_as_int + dither_offset, 0, 0xFF);

        r_dithered >>= 3u;
        g_dithered >>= 3u;
        b_dithered >>= 3u;
        write_color.colors.r = r_dithered & 0x1F;
        write_color.colors.g = g_dithered & 0x1F;
        write_color.colors.b = b_dithered & 0x1F;
    }
    else
    {
        // Convert to RGBA5551
        r >>= 3u;
        g >>= 3u;
        b >>= 3u;
        write_color.colors.r = r & 0x1Fu;;
        write_color.colors.g = g & 0x1Fu;;
        write_color.colors.b = b & 0x1Fu;;
    }

    write_color.colors.a = 1u;
    m_vram.write(x, y, write_color.m_color);
}

}
