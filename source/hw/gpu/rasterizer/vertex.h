/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utility>

#include "hw/gpu/gpu_types.h"
#include "hw/gpu/rasterizer/color.h"
#include "types.h"

namespace PSX
{

struct GPUVertex
{
    GPUPoint    position;
    GPUColor    color;
    GPUTexCoord texcoord;

    void operator=(GPUVertex const& rhs)
    {
        position.coords.x   = rhs.position.coords.x;
        position.coords.y   = rhs.position.coords.y;
        color.color         = rhs.color.color;
        texcoord.m_word     = rhs.texcoord.m_word;
    }
};

}
