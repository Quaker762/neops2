/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>

#include "hw/gpu/rasterizer/rasterizer.h"


namespace PSX
{

u8 GPURasterizer::lookup_texel_4bit(u8 u, u8 v, TexPage const& texpage)
{
    // First, get the texel location in VRAM
    auto const shift_amount   = (u % 4u) * 4u;
    auto const texel_lookup_x = (texpage.bits.x_base * 64u) + (u >> 2u);
    auto const texel_lookup_y = (v + (texpage.bits.y_base * 256u));

    // Do the texel lookup
    u16 texel = m_vram.read(texel_lookup_x, texel_lookup_y);

    texel = (texel >> shift_amount) & 0xFu; // Shift and mask texel to get 4-bit index to CLUT

    return static_cast<u8>(texel);
}

VRAMColor GPURasterizer::clut_lookup(u8 index, TexCLUTPoint const& clut)
{
    auto const clut_color_x = (clut.coords.x_in_16_halword_steps * 16u) + index;
    auto const clut_color_y = clut.coords.y;
    VRAMColor color;

    color.m_color = m_vram.read(clut_color_x, clut_color_y);
    return color;
}

}
