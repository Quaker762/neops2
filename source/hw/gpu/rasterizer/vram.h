/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <array>
#include <memory>
#include <sdl_header.h>
#include "hw/gpu/gpu_constants.h"
#include "types.h"

namespace PSX
{

class VRAM
{
public:
    VRAM() = default;
    ~VRAM() = default;

    void write(unsigned x, unsigned y, u16 const data);
    u16 read(unsigned x, unsigned y) const;

    void dump_to_file() const;

private:
    std::array<u16, VRAM_SIZE_IN_HALF_WORDS> m_vram;
};

}
