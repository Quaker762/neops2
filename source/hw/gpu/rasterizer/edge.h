/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/gpu/gpu_types.h"

namespace PSX
{

// fgiesen.wordpress.com/2013/02/06/the-barycentric-conspirac/
class Edge
{
public:
    Edge() = delete;
    Edge(GPUPoint const& v0, GPUPoint const& v1)
    {
        m_a = v0.coords.y - v1.coords.y;
        m_b = v1.coords.x - v0.coords.x;
        m_c = (v0.coords.x * v1.coords.y) - (v0.coords.y * v1.coords.x);

        m_v0 = v0;
        m_v1 = v1;
    }

    inline i32 evaluate(i32 x, i32 y)
    {
        return (m_a * x) + (m_b * y) + m_c;
    }

    i32 a() const { return m_a; }
    i32 b() const { return m_b; }
    i32 c() const { return m_c; }

    inline i32 evaluate_for_point(GPUPoint const& point)
    {
        return ((m_v1.coords.x - m_v0.coords.x) * (point.coords.y - m_v0.coords.y)) - ((m_v1.coords.y - m_v0.coords.y) * (point.coords.x - m_v0.coords.x));
    }

private:
    GPUPoint m_v0;
    GPUPoint m_v1;
    i32 m_a;
    i32 m_b;
    i32 m_c;
};

}
