/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

// Polygon render commands
static constexpr u8 GP0_COMMAND_NOP                             = 0x00u;
static constexpr u8 GP0_COMMAND_FILL_VRAM_RECTANGLE             = 0x02u;
static constexpr u8 GP0_COMMAND_CLEAR_TEX_CACHE                 = 0x01u;
static constexpr u8 GP0_RENDER_MONOCHROME_QUAD_OPAQUE_COMMAND   = 0x28u;
static constexpr u8 GP0_RENDER_TEXTURED_QUAD_OPAQUE_W_BLENDING  = 0x2Cu;
static constexpr u8 GP0_RENDER_SHADED_TRIANGLE_OPAQUE           = 0x30u;
static constexpr u8 GP0_RENDER_GOURAD_QUAD_OPAQUE_COMMAND       = 0x38u;
static constexpr u8 GP0_COMMAND_COPY_RECTANGLE                  = 0xa0u;
static constexpr u8 GP0_COMMAND_VRAM_TO_CPU_COPY                = 0xc0u;
static constexpr u8 GP0_COMMAND_CONFIGURE_DRAW_MODE             = 0xe1u;
static constexpr u8 GP0_COMMAND_CONFIGURE_TEXTURE_WINDOW        = 0xe2u;
static constexpr u8 GP0_COMMAND_SET_DRAWING_AREA_TOP_LEFT       = 0xe3u;
static constexpr u8 GP0_COMMAND_SET_DRAWING_AREA_BOTTOM_RIGHT   = 0xe4u;
static constexpr u8 GP0_COMMAND_SET_DRAWING_OFFSET              = 0xe5u;
static constexpr u8 GP0_COMMAND_CONFIGURE_MASK_BIT              = 0xe6u;
