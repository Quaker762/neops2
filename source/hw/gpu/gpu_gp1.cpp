/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/gpu/gpu.h"

namespace PSX {

static constexpr u8 GP1_OP_RESET_GPU                        = 0x00u;
static constexpr u8 GP1_FLUSH_FIFO                          = 0x01u;
static constexpr u8 GP1_ACK_IRQ                             = 0x02u;
static constexpr u8 GP1_ENABLE_DISPLAY                      = 0x03u;
static constexpr u8 GP1_OP_DMA_DIRECTION                    = 0x04u;
static constexpr u8 GP1_OP_DISPLAY_AREA_START               = 0x05u;
static constexpr u8 GP1_OP_SET_HORIZONTAL_DISPLAY_RANGE     = 0x06u;
static constexpr u8 GP1_OP_SET_VERTICAL_DISPLAY_RANGE       = 0x07u;
static constexpr u8 GP1_OP_DISPLAY_MODE                     = 0x08u;

void GPU::gp1_reset_gpu()
{
    log(LogLevel::INFO, "Resetting GPU via GP1(00h)");


    m_fifo.flush();                 // Flush the command FIFO
    m_gpustat.m_word = 0x14802000;  // Set GPUSTAT to 0x14802000. See: problemkaputt.de/psx-spx.htm#gpudisplaycontrolcommandsgp1
}

void GPU::gp1_flush_fifo()
{
    // NOTE: This should also abort the current render command, however this isn't
    // posible here because everything is sequentially executed for now
    m_fifo.flush();
}

void GPU::gp1_ack_irq()
{
    m_gpustat.bits.irq = 0u;    // Reset the IRQ flag
}

void GPU::gp1_enable_display(GPUGP1Command const& command)
{
    m_gpustat.bits.display_enable = command.display_on_off_command.display_flag;
}

void GPU::gp1_set_dma_direction(GPUGP1Command const& command)
{
    m_gpustat.bits.dma_direction = command.dma_direction_command.dma_direction;
}

void GPU::gp1_set_display_area_start(GPUGP1Command const& command)
{
    m_vram_start_register.x = command.display_area_start_command.x;
    m_vram_start_register.y = command.display_area_start_command.y;
}

void GPU::gp1_set_display_mode(GPUGP1Command const& command)
{
    m_gpustat.bits.horizontal_resolution1   = command.display_mode_command.horizontal_resolution1;
    m_gpustat.bits.vertical_resolution      = command.display_mode_command.vertical_resolution;
    m_gpustat.bits.video_mode               = command.display_mode_command.video_mode;
    m_gpustat.bits.display_color_depth      = command.display_mode_command.display_color_depth;
    m_gpustat.bits.vertical_interlace       = command.display_mode_command.vertical_interlace;
    m_gpustat.bits.horizontal_resolution2   = command.display_mode_command.horizontal_resolution2;
    m_gpustat.bits.reverse_flag             = command.display_mode_command.reverse_flag;
}

void GPU::gp1_set_horizontal_display_range(GPUGP1Command const& command)
{
    m_display_range_x1 = command.horizontal_display_command.x1;
    m_display_range_x2 = command.horizontal_display_command.x2;
}

void GPU::gp1_set_vertical_display_range(GPUGP1Command const& command)
{
    m_display_range_y1 = command.vertical_display_command.y1;
    m_display_range_y2 = command.vertical_display_command.y2;
}

void GPU::execute_gp1_command(GPUGP1Command command)
{
    switch (command.command.byte)
    {
    case GP1_OP_RESET_GPU:
        gp1_reset_gpu();
        break;
    case GP1_FLUSH_FIFO:
        gp1_flush_fifo();
        break;
    case GP1_ACK_IRQ:
        gp1_ack_irq();
        break;
    case GP1_ENABLE_DISPLAY:
        gp1_enable_display(command);
        break;
    case GP1_OP_DMA_DIRECTION:
        gp1_set_display_mode(command);
        break;
    case GP1_OP_DISPLAY_AREA_START:
        gp1_set_display_area_start(command);
        break;
    case GP1_OP_DISPLAY_MODE:
        gp1_set_display_mode(command);
        break;
    case GP1_OP_SET_HORIZONTAL_DISPLAY_RANGE:
        gp1_set_horizontal_display_range(command);
        break;
    case GP1_OP_SET_VERTICAL_DISPLAY_RANGE:
        gp1_set_vertical_display_range(command);
        break;
    default:
        Util::die("Unknown GP1 command byte, 0x%02x (0x%08x)!", command.command.byte, command.m_word);
    }
}

}
