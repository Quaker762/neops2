/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdlib>
#include "types.h"

namespace PSX
{

static constexpr size_t VRAM_WIDTH_IN_HALFWORDS     = 1024u;
static constexpr size_t VRAM_HEIGHT_IN_HALFWORDS    = 512u;
static constexpr size_t VRAM_BIT_DEPTH              = 16u;
static constexpr size_t VRAM_SIZE_IN_HALF_WORDS     = VRAM_WIDTH_IN_HALFWORDS * VRAM_HEIGHT_IN_HALFWORDS;
static constexpr u32 VRAM_SIZE                      = 1 * MB;

}
