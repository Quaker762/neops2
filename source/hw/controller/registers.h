/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

union ModeRegister
{
    struct
    {
        u16 baud_reload_factor  : 2;
        u16 char_length         : 2;
        u16 parity_enable       : 1;
        u16 parity_type         : 1;
        u16 always_zero1        : 2;
        u16 clk_out_polarity    : 1;
        u16 always_zero2        : 7;
    } bits;

    u16 m_word;
};

union ControlRegister
{
    struct
    {
        u16 tx_enable           : 1;
        u16 joyn_output         : 1;
        u16 rx_enable           : 1;
        u16 unknown1            : 1;
        u16 acknowledge         : 1;
        u16 unknown2            : 1;
        u16 reset               : 1;
        u16 unused1             : 1;
        u16 rx_irq_mode         : 2;
        u16 tx_int_enable       : 1;
        u16 rx_int_enable       : 1;
        u16 ack_int_enable      : 1;
        u16 desired_slot_num    : 1;
        u16 unused2             : 2;
    } bits;

    u16 m_word;
};

}
