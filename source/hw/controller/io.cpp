/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/controller/io.h"
#include "util/log.h"

namespace PSX
{

static constexpr bool DEBUG_ENABLE = true;

ControllerIO::ControllerIO() : BusDevice(CONTROLLER_ADDRESS_RANGE_START, CONTROLLER_ADDRESS_RANGE_END)
{

}

u8 ControllerIO::read8(paddr_t address)
{
    Util::die("Unhandled 8-bit read from Controller IO port @ 0x%08x", address);
    return 0u;
}

u16 ControllerIO::read16(paddr_t address)
{
    Util::die("Unhandled 16-bit read from Controller IO port @ 0x%08x", address);
    return 0u;
}

u32 ControllerIO::read32(paddr_t address)
{
    Util::die("Unhandled 32-bit read from Controller IO port @ 0x%08x", address);
    return 0u;
}

void ControllerIO::write8(paddr_t address, u8 data)
{
    Util::die("Unhandled 8-bit write of 0x%02x to Controller IO port @ 0x%08x", data, address);
}

void ControllerIO::write16(paddr_t address, u16 data)
{
    if (address == JOY_CTRL_ADDRESS)
    {
        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "Writing 0x%04x to JOY_CTRL", data);
        }

        m_ctrl.m_word = (data & CONTROLLER_CTRL_WRITE_MASK);
    }
    else if (address == JOY_MODE_ADDRESS)
    {
        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "Writing 0x%04x to JOY_MODE", data);
        }

        m_ctrl.m_word = data;
    }
    else if (address == JOY_BAUD_ADDRESS)
    {
        if constexpr (DEBUG_ENABLE)
        {
            log(LogLevel::INFO, "Writing 0x%04x to JOY_BAUD", data);
        }

        m_baud_rate = data;
    }
    else
    {
        Util::die("Unhandled 16-bit write of 0x%02x to Controller IO port @ 0x%08x", data, address);
    }
}

void ControllerIO::write32(paddr_t address, u32 data)
{
    Util::die("Unhandled 32-bit write of 0x%02x to Controller IO port @ 0x%08x", data, address);
}

}
