/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"
#include "registers.h"

namespace PSX
{

class ControllerIO : public BusDevice
{
    static constexpr paddr_t CONTROLLER_ADDRESS_RANGE_START = 0x1F801040u;
    static constexpr paddr_t CONTROLLER_ADDRESS_RANGE_END   = 0x1F80104Eu;
    static constexpr paddr_t JOY_MODE_ADDRESS               = 0x1F801048u;
    static constexpr paddr_t JOY_CTRL_ADDRESS               = 0x1F80104Au;
    static constexpr paddr_t JOY_BAUD_ADDRESS               = 0x1F80104Eu;
    static constexpr u16     CONTROLLER_CTRL_WRITE_MASK     = 0x3F7Fu;

public:
    ControllerIO();
    virtual ~ControllerIO(){}

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

private:
    ControlRegister m_ctrl;
    ModeRegister    m_mode;
    u16 m_baud_rate;

};

}
