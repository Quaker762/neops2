/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/r3000a/r3000.h"

namespace PSX
{

u32 R3000::JTAG::read_gpr(u8 index) const
{
    return m_cpu.read_register(index);
}

u32 R3000::JTAG::read_pc() const
{
    return m_cpu.m_pc;
}

u32 R3000::JTAG::read_next_pc() const
{
    return m_cpu.m_next_pc;
}

void R3000::JTAG::write_gpr(u8 index, u32 value) const
{
    m_cpu.m_gprs.m_gprs[index] = value;
    m_cpu.write_register(index, value);
}

void R3000::JTAG::force_pc(u32 pc)
{
    m_cpu.m_pc = pc;
    m_cpu.m_next_pc = pc + 4u;
}
void R3000::JTAG::clock()
{
    m_cpu.cycle();
}

void R3000::JTAG::write_hi(u32 value)
{
    m_cpu.m_hi = value;
}

void R3000::JTAG::write_lo(u32 value)
{
    m_cpu.m_lo = value;
}

u32 R3000::JTAG::read_hi() const
{
    return m_cpu.m_hi;
}

u32 R3000::JTAG::read_lo() const
{
    return m_cpu.m_lo;
}

void R3000::JTAG::write_cop0_register(u8 index, u32 data) const
{
    m_cpu.m_cop0.write_register(index, data);
}

u32 R3000::JTAG::read_cop0_register(u8 index) const
{
    return m_cpu.m_cop0.read_register(index);
}

}
