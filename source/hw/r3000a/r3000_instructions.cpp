/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/r3000a/r3000.h"
#include "hw/r3000a/mips/mips_opcodes.h"
#include "util/log.h"

namespace PSX
{

void R3000::special()
{
    switch (m_current_instruction.r_type.funct)
    {
    case MIPS::SPECIAL_ADD:
        special_add();
        break;
    case MIPS::SPECIAL_ADDU:
        special_addu();
        break;
    case MIPS::SPECIAL_AND:
        special_and();
        break;
    case MIPS::SPECIAL_BREAK:
        special_break();
        break;
    case MIPS::SPECIAL_DIV:
        special_div();
        break;
    case MIPS::SPECIAL_DIVU:
        special_divu();
        break;
    case MIPS::SPECIAL_JALR:
        special_jalr();
        break;
    case MIPS::SPECIAL_JR:
        special_jr();
        break;
    case MIPS::SPECIAL_MFHI:
        special_mfhi();
        break;
    case MIPS::SPECIAL_MFLO:
        special_mflo();
        break;
    case MIPS::SPECIAL_MTHI:
        special_mthi();
        break;
    case MIPS::SPECIAL_MTLO:
        special_mtlo();
        break;
    case MIPS::SPECIAL_MULT:
        special_mult();
        break;
    case MIPS::SPECIAL_MULTU:
        special_multu();
        break;
    case MIPS::SPECIAL_OR:
        special_or();
        break;
    case MIPS::SPECIAL_XOR:
        special_xor();
        break;
    case MIPS::SPECIAL_NOR:
        special_nor();
        break;
    case MIPS::SPECIAL_SLL:
        special_sll();
        break;
    case MIPS::SPECIAL_SLLV:
        special_sllv();
        break;
    case MIPS::SPECIAL_SRL:
        special_srl();
        break;
    case MIPS::SPECIAL_SRLV:
        special_srlv();
        break;
    case MIPS::SPECIAL_SLT:
        special_slt();
        break;
    case MIPS::SPECIAL_SLTU:
        special_sltu();
        break;
    case MIPS::SPECIAL_SRA:
        special_sra();
        break;
    case MIPS::SPECIAL_SRAV:
        special_srav();
        break;
    case MIPS::SPECIAL_SUB:
        special_sub();
        break;
    case MIPS::SPECIAL_SUBU:
        special_subu();
        break;
    case MIPS::SPECIAL_SYSCALL:
        special_syscall();
        break;
    default:
        invalid_instruction(m_current_instruction);
        break;
    }
}

void R3000::branch(u16 offset)
{
    i32 sign_extended_offset = static_cast<i16>((offset << 2u));  // Sign extended offset
    i32 pc = static_cast<i32>(m_pc) + sign_extended_offset;

    m_branch_delay  = true;
    m_next_pc       = static_cast<u32>(pc);
}

void R3000::addi()
{
    auto rt         = m_current_instruction.i_type.rt;
    auto rs         = m_current_instruction.i_type.rs;
    i32 immediate   = static_cast<i16>(m_current_instruction.i_type.immediate);
    i32 result;

    auto will_overflow = __builtin_sadd_overflow(static_cast<i32>(m_gprs.m_gprs[rs]), immediate, &result);
    if (will_overflow)
    {
        raise_exception(ExceptionCauses::Ov);
    }

    write_register(rt, static_cast<u32>(result));
}

void R3000::addiu()
{
    i32 immediate   = static_cast<i16>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;
    u32 result      = static_cast<u32>((static_cast<i32>(m_gprs.m_gprs[rs]) + immediate));

    write_register(rt, result);
}

void R3000::andi()
{
    u32 immediate   = static_cast<u32>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;
    u32 result      = (m_gprs.m_gprs[rs] & immediate);

    write_register(rt, result);
}

void R3000::bcondz()
{
    static constexpr u8 CONDITION_LESS_THAN_ZERO                = 0u;
    static constexpr u8 CONDITION_GREATER_OR_EQUAL_THAN_ZERO    = 1u;

    auto rs             = m_current_instruction.i_type.rs;
    auto condition      = (m_current_instruction.m_word >> 16) & 3u;
    auto should_link    = (((m_current_instruction.m_word >> 19) & 3u) == 1u);
    bool should_branch  = false;

    switch (condition)
    {
    case CONDITION_LESS_THAN_ZERO:
        should_branch   = (static_cast<i32>(m_gprs.m_gprs[rs]) < 0);
        m_branch_delay  = true;
        break;
    case CONDITION_GREATER_OR_EQUAL_THAN_ZERO:
        should_branch   = (static_cast<i32>(m_gprs.m_gprs[rs]) >= 0);
        m_branch_delay= true;
        break;
    default:
        break;
    }

    // Put the address of the instruction following the delay slot into R31
    if (should_link)
    {
        write_register(REGISTER_RA, m_executing_pc + 8u);
    }

    if (should_branch)
    {
        branch(m_current_instruction.i_type.immediate);
    }
}

void R3000::beq()
{
    auto rs     = m_current_instruction.i_type.rs;
    auto rt     = m_current_instruction.i_type.rt;
    u16 offset  = m_current_instruction.i_type.immediate;

    if (m_gprs.m_gprs[rs] == m_gprs.m_gprs[rt])
    {
        branch(offset);
    }
}

void R3000::bgtz()
{
    auto rs     = m_current_instruction.i_type.rs;
    u16 offset  = m_current_instruction.i_type.immediate;

    if (static_cast<i32>(m_gprs.m_gprs[rs]) > 0)
    {
        branch(offset);
    }
}

void R3000::blez()
{
    auto rs     = m_current_instruction.i_type.rs;
    u16 offset  = m_current_instruction.i_type.immediate;

    if (static_cast<i32>(m_gprs.m_gprs[rs]) <= 0)
    {
        branch(offset);
    }
}

void R3000::bne()
{
    auto rs     = m_current_instruction.i_type.rs;
    auto rt     = m_current_instruction.i_type.rt;
    u16 offset  = m_current_instruction.i_type.immediate;

    if (m_gprs.m_gprs[rs] != m_gprs.m_gprs[rt])
    {
        branch(offset);
    }
}

void R3000::copn(u8 coprocessor)
{
    auto cop_instruction = m_current_instruction.r_type.rs;
    u8 rt = m_current_instruction.r_type.rt;
    u8 rd = m_current_instruction.r_type.rd;
    u32 cop_register_value = 0u;
    u8 destination_register = 0u;

    if (coprocessor == 0u)
    {
        switch (cop_instruction)
        {
        case MIPS::COP_MTC:
            m_cop0.write_register(rd, m_gprs.m_gprs[rt]);
            break;
        case MIPS::COP_MFC:
            cop_register_value      = m_cop0.read_register(rd);
            destination_register    = rt;
            break;
        case MIPS::COP_RFE:
            rfe();
            break;
        default:
            Util::die("Unknown cop0 instruction, 0x%02x", cop_instruction);
        }
    }
    else
    {
        Util::die("Unknown coprocessor, %u", coprocessor);
    }

    // Always perform the store (we basically get it for free anyway, and a store is cheap)
    schedule_load_delay(destination_register, cop_register_value);
}

void R3000::j()
{
    // Unconditionally take the branch
    auto target = static_cast<u32>(m_current_instruction.j_type.target << 2u);

    m_next_pc       = ((m_pc & 0xF0000000u) | target);
    m_branch_delay  = true;
}

void R3000::jal()
{
    // Unconditionally take the branch
    auto target = static_cast<u32>(m_current_instruction.j_type.target << 2u);

    write_register(REGISTER_RA, m_executing_pc + 8u);   // This is the instruction AFTER the branch delay instruction
    m_next_pc           = ((m_pc & 0xF0000000u) | target);
    m_branch_delay      = true;
}

void R3000::lui()
{
    auto immediate = static_cast<u32>(m_current_instruction.i_type.immediate) << 16u;
    auto rt = m_current_instruction.i_type.rt;

    write_register(rt, immediate);
}

void R3000::lb()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lb read from 0x%08x due to cache isolation", address);
        return;
    }

    i32 sign_extended_value = static_cast<i8>(bus_read<u8>(address));
    schedule_load_delay(m_current_instruction.i_type.rt, static_cast<u32>(sign_extended_value));
}

void R3000::lbu()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lbu read from 0x%08x due to cache isolation", address);
        return;
    }

    schedule_load_delay(m_current_instruction.i_type.rt, bus_read<u8>(address));
}

void R3000::lh()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lh read from 0x%08x due to cache isolation", address);
        return;
    }
    else if (is_unaligned_access<u16>(address))
    {
        raise_exception(ExceptionCauses::AdEL);
        return;
    }

    i32 sign_extended_value = static_cast<i16>(bus_read<u16>(address));
    schedule_load_delay(m_current_instruction.i_type.rt, static_cast<u32>(sign_extended_value));
}


void R3000::lhu()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lhu read from 0x%08x due to cache isolation", address);
        return;
    }
    else if (is_unaligned_access<u16>(address))
    {
        raise_exception(ExceptionCauses::AdEL);
        return;
    }

    schedule_load_delay(m_current_instruction.i_type.rt, bus_read<u16>(address));
}

void R3000::lw()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lw read from 0x%08x due to cache isolation", address);
        return;
    }
    else if (is_unaligned_access<u32>(address))
    {
        raise_exception(ExceptionCauses::AdEL);
        return;
    }

    schedule_load_delay(m_current_instruction.i_type.rt, bus_read<u32>(address));
}

// https://stackoverflow.com/questions/57522055/what-do-the-mips-load-word-left-lwl-and-load-word-right-lwr-instructions-do
void R3000::lwl()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    vaddr_t aligned_address = (address & ~3u);

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lw read from 0x%08x due to cache isolation", address);
        return;
    }

    // Do the load
    u32 rt_value = m_gprs.m_gprs[m_current_instruction.r_type.rt];
    u32 aligned_value = bus_read<u32>(aligned_address);

    u32 result = 0u;
    switch ((address & 3u))
    {
    case 0u:
        result = (rt_value & 0x00FFFFFFu) | ((aligned_value & 0x000000FFu) << 24u);
        break;
    case 1u:
        result = (rt_value & 0x0000FFFFu) | ((aligned_value & 0x0000FFFFu) << 16u);
        break;
    case 2u:
        result = (rt_value & 0x00000FFu) | ((aligned_value & 0x00FFFFFFu) << 8u);
        break;
    case 3u:
        result = aligned_value;
        break;
    }

    schedule_load_delay(m_current_instruction.i_type.rt, result);
}

// https://stackoverflow.com/questions/57522055/what-do-the-mips-load-word-left-lwl-and-load-word-right-lwr-instructions-do
void R3000::lwr()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    vaddr_t aligned_address = (address & ~3u);

    if (m_cop0.sr().bits.ISc == 1)
    {
        log(LogLevel::WARN, "No executing lw read from 0x%08x due to cache isolation", address);
        return;
    }

    // Do the load
    u32 rt_value = m_gprs.m_gprs[m_current_instruction.r_type.rt];
    u32 aligned_value = bus_read<u32>(aligned_address);

    u32 result = 0u;
    switch ((address & 3u))
    {
    case 0u:
        result = aligned_value;
        break;
    case 1u:
        result = (rt_value & 0xFF000000u) | ((aligned_value & 0xFFFFFF00u) >> 8u);
        break;
    case 2u:
        result = (rt_value & 0xFFFF0000u) | ((aligned_value & 0xFFFF0000u) >> 16u);
        break;
    case 3u:
        result = (rt_value & 0xFFFFFF00u) | ((aligned_value & 0xFF000000u) >> 24u);
        break;
    }

    schedule_load_delay(m_current_instruction.i_type.rt, result);
}

void R3000::ori()
{
    auto immediate  = static_cast<u32>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;
    auto result     = m_gprs.m_gprs[rs] | immediate;

    write_register(rt, result);
}

void R3000::sb()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto rt         = m_current_instruction.i_type.rt;
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    paddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    u8 value       = (m_gprs.m_gprs[rt] & 0xFFu);

    bus_write<u8>(address, value);
}

void R3000::sh()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto rt         = m_current_instruction.i_type.rt;
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    paddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    u16 value       = (m_gprs.m_gprs[rt] & 0xFFFFu);

    if (is_unaligned_access<u16>(address))
    {
        raise_exception(ExceptionCauses::AdES);
        return;
    }

    bus_write<u16>(address, value);
}

void R3000::slti()
{
    i32 immediate   = static_cast<i16>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;

    u32 result = (static_cast<i32>(m_gprs.m_gprs[rs]) < immediate) ? 1u : 0u;

    write_register(rt, result);
}

void R3000::sltiu()
{
    i32 immediate   = static_cast<i16>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;

    u32 result = (m_gprs.m_gprs[rs] < static_cast<u32>(immediate)) ? 1u : 0u;

    write_register(rt, result);
}

void R3000::sw()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto rt         = m_current_instruction.i_type.rt;
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    paddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));

    if (is_unaligned_access<u32>(address))
    {
        raise_exception(ExceptionCauses::AdES);
        return;
    }

    bus_write<u32>(address, m_gprs.m_gprs[rt]);
}

void R3000::swl()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    vaddr_t aligned_address = (address & ~3u);

    // Do the load
    u32 rt_value = m_gprs.m_gprs[m_current_instruction.r_type.rt];
    u32 aligned_value = bus_read<u32>(aligned_address);

    u32 result = 0u;
    switch ((address & 0x3))
    {
    case 0:
        result = ((rt_value & 0xff000000) >> 24u) | (aligned_value & 0xffffff00);
        break;
    case 1:
        result = ((rt_value & 0xffff0000) >> 16u) | (aligned_value & 0xffff0000);
        break;
    case 2:
        result = ((rt_value & 0xffffff00) >> 8u) | (aligned_value & 0xff000000);
        break;
    case 3:
        result = rt_value;
        break;
    }

    bus_write<u32>(aligned_address, result);
}

void R3000::swr()
{
    i32 offset      = static_cast<i16>(m_current_instruction.i_type.immediate);  // Sign extended offset
    auto base       = m_gprs.m_gprs[m_current_instruction.i_type.rs];
    vaddr_t address = static_cast<paddr_t>((static_cast<i32>(base) + offset));
    vaddr_t aligned_address = (address & ~3u);

    // Do the load
    u32 rt_value = m_gprs.m_gprs[m_current_instruction.r_type.rt];
    u32 aligned_value = bus_read<u32>(aligned_address);

    u32 result = 0u;
    switch ((address & 0x3))
    {
    case 0:
        result = rt_value;
        break;
    case 1:
        result = ((rt_value & 0x00ffffff) << 8u) | (aligned_value & 0x000000ff);
        break;
    case 2:
        result = ((rt_value & 0x0000ffff) << 16u) | (aligned_value & 0x0000ffff);
        break;
    case 3:
        result = ((rt_value & 0x000000ff) << 24u) | (aligned_value & 0x00ffffff);
        break;
    }

    bus_write<u32>(aligned_address, result);
}

void R3000::xori()
{
    auto immediate  = static_cast<u32>(m_current_instruction.i_type.immediate);
    auto rs         = m_current_instruction.i_type.rs;
    auto rt         = m_current_instruction.i_type.rt;
    auto result     = m_gprs.m_gprs[rs] ^ immediate;

    write_register(rt, result);
}

// Special instructions
void R3000::special_add()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    i32 result;

    bool will_overflow = __builtin_sadd_overflow(static_cast<i32>(m_gprs.m_gprs[rs]), static_cast<i32>(m_gprs.m_gprs[rt]), &result);
    if (will_overflow)
    {
        raise_exception(ExceptionCauses::Ov);
    }

    write_register(rd, static_cast<u32>(result));
}

void R3000::special_addu()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    u32 result  = m_gprs.m_gprs[rs] + m_gprs.m_gprs[rt];

    write_register(rd, result);
}

void R3000::special_and()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    u32 result  = m_gprs.m_gprs[rs] & m_gprs.m_gprs[rt];

    write_register(rd, result);
}

void R3000::special_break()
{
    raise_exception(ExceptionCauses::Bp);
}

void R3000::special_div()
{
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;

    i32 numerator = static_cast<i32>(m_gprs.m_gprs[rs]);
    i32 denominator = static_cast<i32>(m_gprs.m_gprs[rt]);

    // FIXME: Handle divide by zero
    if (denominator == 0)
    {
        m_hi = static_cast<u32>(numerator);

        if (numerator >= 0)
        {
            m_lo = UINT32_MAX;
        }
        else
        {
            m_lo = 1u;
        }
    }
    else if ((static_cast<u32>(numerator) & 0x80000000) && (denominator == -1))
    {
        m_hi = 0u;
        m_lo = static_cast<u32>(INT32_MIN);
    }
    else
    {
        m_lo = static_cast<u32>((numerator / denominator));
        m_hi = static_cast<u32>((numerator % denominator));
    }
}

void R3000::special_divu()
{
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;

    u32 numerator = m_gprs.m_gprs[rs];
    u32 denominator = m_gprs.m_gprs[rt];

    if (denominator == 0u)
    {
        // Result in unpredictable
        m_hi = numerator;
        m_lo = UINT32_MAX;
    }
    else
    {
        m_hi = (numerator % denominator);
        m_lo = (numerator / denominator);
    }
}

void R3000::special_jalr()
{
    auto rs         = m_current_instruction.r_type.rs;
    auto rd         = m_current_instruction.r_type.rd;

    m_next_pc       = m_gprs.m_gprs[rs];
    m_branch_delay  = true;

    write_register(rd, m_executing_pc + 8u);
}

void R3000::special_jr()
{
    m_next_pc       = m_gprs.m_gprs[m_current_instruction.r_type.rs];
    m_branch_delay  = true;
}

void R3000::special_mfhi()
{
    write_register(m_current_instruction.r_type.rd, m_hi);
}

void R3000::special_mflo()
{
    write_register(m_current_instruction.r_type.rd, m_lo);
}

void R3000::special_mthi()
{
    m_hi = m_gprs.m_gprs[m_current_instruction.r_type.rd];
}

void R3000::special_mtlo()
{
    m_lo = m_gprs.m_gprs[m_current_instruction.r_type.rd];
}

void R3000::special_mult()
{
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;

    // Do the multiplication in twos complement
    i64 operand1 = static_cast<i64>(static_cast<i32>(m_gprs.m_gprs[rs]));
    i64 operand2 = static_cast<i64>(static_cast<i32>(m_gprs.m_gprs[rt]));
    u64 result = static_cast<u64>(operand1 * operand2);

    m_hi = static_cast<u32>((result & 0xFFFFFFFF00000000ull) >> 32ull);
    m_lo = (result & 0xFFFFFFFFu);
}

void R3000::special_multu()
{
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;
    u64 result = m_gprs.m_gprs[rs] * m_gprs.m_gprs[rt];

    m_hi = static_cast<u32>(((result & 0xFFFFFFFF00000000ull) >> 32ull));
    m_lo = (result & 0xFFFFFFFFu);
}

void R3000::special_nor()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    u32 result  = ~(m_gprs.m_gprs[rs] | m_gprs.m_gprs[rt]);

    write_register(rd, result);
}

void R3000::special_or()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    u32 result  = m_gprs.m_gprs[rs] | m_gprs.m_gprs[rt];

    write_register(rd, result);
}

void R3000::special_sll()
{
    auto shamt = m_current_instruction.r_type.shamt;
    auto rd    = m_current_instruction.r_type.rd;
    auto rt    = m_current_instruction.r_type.rt;

    auto new_register_value = (m_gprs.m_gprs[rt] << shamt);
    write_register(rd, new_register_value);
}

void R3000::special_sllv()
{
    auto rs = m_current_instruction.r_type.rs;
    auto rd = m_current_instruction.r_type.rd;
    auto rt = m_current_instruction.r_type.rt;

    auto shamt = (m_gprs.m_gprs[rs] & 0x1Fu);
    auto new_register_value = (m_gprs.m_gprs[rt] << shamt);
    write_register(rd, new_register_value);
}

void R3000::special_srl()
{
    auto shamt = m_current_instruction.r_type.shamt;
    auto rd    = m_current_instruction.r_type.rd;
    auto rt    = m_current_instruction.r_type.rt;

    auto new_register_value = (m_gprs.m_gprs[rt] >> shamt);
    write_register(rd, new_register_value);
}

void R3000::special_srlv()
{
    auto rd = m_current_instruction.r_type.rd;
    auto rt = m_current_instruction.r_type.rt;
    auto rs = m_current_instruction.r_type.rs;

    auto shamt = (m_gprs.m_gprs[rs] & 0x1Fu);
    auto new_register_value = (m_gprs.m_gprs[rt] >> shamt);
    write_register(rd, new_register_value);
}

void R3000::special_slt()
{
    auto rd = m_current_instruction.r_type.rd;
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;
    u32 result;

    result = (static_cast<i32>(m_gprs.m_gprs[rs]) < static_cast<i32>(m_gprs.m_gprs[rt])) ? 1u : 0u;

    write_register(rd, result);
}

void R3000::special_sltu()
{
    auto rd = m_current_instruction.r_type.rd;
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;
    u32 result;

    result = (m_gprs.m_gprs[rs] < m_gprs.m_gprs[rt]) ? 1u : 0u;

    write_register(rd, result);
}

void R3000::special_sra()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rt     = m_current_instruction.r_type.rt;
    auto shamt  = m_current_instruction.r_type.shamt;

    // Doing a right shift on a _signed_ variable acts as an arithmetic shift in C++
    i32 register_as_i32 = static_cast<i32>(m_gprs.m_gprs[rt]);
    register_as_i32 >>= shamt;

    write_register(rd, static_cast<u32>(register_as_i32));
}

void R3000::special_srav()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rt     = m_current_instruction.r_type.rt;
    auto rs     = m_current_instruction.r_type.rs;

    auto shamt  = (m_gprs.m_gprs[rs] & 0x1Fu);

    // Doing a right shift on a _signed_ variable acts as an arithmetic shift in C++
    i32 register_as_i32 = static_cast<i32>(m_gprs.m_gprs[rt]);
    register_as_i32 >>= shamt;

    write_register(rd, static_cast<u32>(register_as_i32));
}

void R3000::special_sub()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    i32 result;

    bool will_overflow = __builtin_ssub_overflow(static_cast<i32>(m_gprs.m_gprs[rs]), static_cast<i32>(m_gprs.m_gprs[rt]), &result);
    if (will_overflow)
    {
        raise_exception(ExceptionCauses::Ov);
    }

    write_register(rd, static_cast<u32>(result));
}

void R3000::special_subu()
{
    auto rd = m_current_instruction.r_type.rd;
    auto rs = m_current_instruction.r_type.rs;
    auto rt = m_current_instruction.r_type.rt;
    u32 result;

    result = m_gprs.m_gprs[rs] - m_gprs.m_gprs[rt];
    write_register(rd, result);
}

void R3000::special_syscall()
{
    syscall();
}

void R3000::special_xor()
{
    auto rd     = m_current_instruction.r_type.rd;
    auto rs     = m_current_instruction.r_type.rs;
    auto rt     = m_current_instruction.r_type.rt;
    u32 result  = m_gprs.m_gprs[rs] ^ m_gprs.m_gprs[rt];

    write_register(rd, result);
}

}
