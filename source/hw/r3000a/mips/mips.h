/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

static constexpr paddr_t KUSEG_START_ADDRESS    = 0x00000000u;
static constexpr paddr_t KSEG0_START_ADDRESS    = 0x80000000u;
static constexpr paddr_t KSEG1_START_ADDRESS    = 0xA0000000u;
static constexpr paddr_t KSSEG_START_ADDRESS    = 0xC0000000u;
static constexpr paddr_t KSEG3_START_ADDRESS    = 0xE0000000u;

static constexpr size_t KUSEG_SIZE              = 0x80000000u;
static constexpr size_t KSEG0_SIZE              = 0x1FFFFFFFu;
static constexpr size_t KSEG1_SIZE              = 0x1FFFFFFFu;
static constexpr size_t KSSEG_SIZE              = 0x1FFFFFFFu;
static constexpr size_t KSEG3_SIZE              = 0x1FFFFFFFu;

// Thanks Simias!
static constexpr u32 SEGMENT_ADDRESS_MASKS[8u] =
{
    0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
    0x7FFFFFFFu,
    0x1FFFFFFFu,
    0xFFFFFFFFu, 0xFFFFFFFFu
};

static constexpr u32        MIPS_INITIAL_PC_VALUE               = 0xBFC00000u;
static constexpr vaddr_t    MIPS_EXCEPTION_ADDRESS_GENERAL_BEV  = (MIPS_INITIAL_PC_VALUE + 0x180);
static constexpr vaddr_t    MIPS_EXCEPTION_ADDRESS_GENERAL      = 0x80000080u;
static constexpr size_t     MIPS_NUMBER_OF_GPR                  = 32u;  // This never changes!
static_assert(MIPS_NUMBER_OF_GPR == 32u, "Invalid number of General Purpose Registers (must be 32)!");

static constexpr const char* g_register_names[MIPS_NUMBER_OF_GPR] =
{
    "$zero",
    "$at",
    "$v0",
    "$v1",
    "$a0",
    "$a1",
    "$a2",
    "$a3",
    "$t0",
    "$t1",
    "$t2",
    "$t3",
    "$t4",
    "$t5",
    "$t6",
    "$t7",
    "$s0",
    "$s1",
    "$s2",
    "$s3",
    "$s4",
    "$s5",
    "$s6",
    "$s7",
    "$t8",
    "$t9",
    "$k0",
    "$k1",
    "$gp",
    "$sp",
    "$fp",
    "$ra"
};

// Register aliases
static constexpr u32 REGISTER_RA    = 31u;

//
// Instruction union type
// Defines a MIPs instruction and all of the different decoding types
// for instructions in the MIPs architecture
//
union Instruction
{
    void operator=(u32 instruction) { m_word = instruction; }

    struct [[gnu::packed]] IType
    {
        u32 immediate : 16;
        u32 rt : 5;
        u32 rs : 5;
        u32 op : 6;
    } i_type;

    struct [[gnu::packed]] JType
    {
        u32 target : 26;
        u32 op : 6;
    } j_type;

    struct [[gnu::packed]] RType
    {
        u32 funct : 6;
        u32 shamt : 5;
        u32 rd : 5;
        u32 rt : 5;
        u32 rs : 5;
        u32 op : 6;
    } r_type;

    u32 m_word;
};
