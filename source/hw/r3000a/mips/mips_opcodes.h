/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX::MIPS
{

static constexpr u8 OPCODE_SPECIAL  = 0x00u;
static constexpr u8 OPCODE_BCOND    = 0x01u;
static constexpr u8 OPCODE_J        = 0x02u;
static constexpr u8 OPCODE_JAL      = 0x03u;
static constexpr u8 OPCODE_BEQ      = 0x04u;
static constexpr u8 OPCODE_BNE      = 0x05u;
static constexpr u8 OPCODE_BLEZ     = 0x06u;
static constexpr u8 OPCODE_BGTZ     = 0x07u;
static constexpr u8 OPCODE_ADDI     = 0x08u;
static constexpr u8 OPCODE_ADDIU    = 0x09u;
static constexpr u8 OPCODE_SLTI     = 0x0Au;
static constexpr u8 OPCODE_SLTIU    = 0x0Bu;
static constexpr u8 OPCODE_ANDI     = 0x0Cu;
static constexpr u8 OPCODE_ORI      = 0x0Du;
static constexpr u8 OPCODE_XORI     = 0x0Eu;
static constexpr u8 OPCODE_LUI      = 0x0Fu;
static constexpr u8 OPCODE_COP0     = 0x10u;
static constexpr u8 OPCODE_COP1     = 0x11u;
static constexpr u8 OPCODE_COP2     = 0x12u;
static constexpr u8 OPCODE_COP3     = 0x13u;
static constexpr u8 OPCODE_LB       = 0x20u;
static constexpr u8 OPCODE_LH       = 0x21u;
static constexpr u8 OPCODE_LWL      = 0x22u;
static constexpr u8 OPCODE_LW       = 0x23u;
static constexpr u8 OPCODE_LBU      = 0x24u;
static constexpr u8 OPCODE_LHU      = 0x25u;
static constexpr u8 OPCODE_LWR      = 0x26u;
static constexpr u8 OPCODE_SB       = 0x28u;
static constexpr u8 OPCODE_SH       = 0x29u;
static constexpr u8 OPCODE_SWL      = 0x2Au;
static constexpr u8 OPCODE_SW       = 0x2Bu;
static constexpr u8 OPCODE_SWR      = 0x2Eu;
static constexpr u8 OPCODE_LWC0     = 0x30u;
static constexpr u8 OPCODE_LWC1     = 0x31u;
static constexpr u8 OPCODE_LWC2     = 0x32u;
static constexpr u8 OPCODE_LWC3     = 0x33u;

// Special opcodes
static constexpr u8 SPECIAL_SLL     = 0x00u;
static constexpr u8 SPECIAL_SRL     = 0x02u;
static constexpr u8 SPECIAL_SRA     = 0x03u;
static constexpr u8 SPECIAL_SLLV    = 0x04u;
static constexpr u8 SPECIAL_SRLV    = 0x06u;
static constexpr u8 SPECIAL_SRAV    = 0x07u;
static constexpr u8 SPECIAL_JR      = 0x08u;
static constexpr u8 SPECIAL_JALR    = 0x09U;
static constexpr u8 SPECIAL_SYSCALL = 0x0CU;
static constexpr u8 SPECIAL_BREAK   = 0x0DU;
static constexpr u8 SPECIAL_MFHI    = 0x10U;
static constexpr u8 SPECIAL_MTHI    = 0x11U;
static constexpr u8 SPECIAL_MFLO    = 0x12U;
static constexpr u8 SPECIAL_MTLO    = 0x13U;
static constexpr u8 SPECIAL_MULT    = 0x18U;
static constexpr u8 SPECIAL_MULTU   = 0x19U;
static constexpr u8 SPECIAL_DIV     = 0x1Au;
static constexpr u8 SPECIAL_DIVU    = 0x1Bu;
static constexpr u8 SPECIAL_ADD     = 0x20u;
static constexpr u8 SPECIAL_ADDU    = 0x21u;
static constexpr u8 SPECIAL_SUB     = 0x22u;
static constexpr u8 SPECIAL_SUBU    = 0x23u;
static constexpr u8 SPECIAL_AND     = 0x24u;
static constexpr u8 SPECIAL_OR      = 0x25u;
static constexpr u8 SPECIAL_XOR     = 0x26u;
static constexpr u8 SPECIAL_NOR     = 0x27u;
static constexpr u8 SPECIAL_SLT     = 0x2Au;
static constexpr u8 SPECIAL_SLTU    = 0x2Bu;

}
