/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/r3000a/mips/mips_cop0.h"
#include "util/log.h"

namespace PSX::MIPS
{

static constexpr bool DEBUG_ENABLE = false;

Coprocessor0::~Coprocessor0()
{

}

void Coprocessor0::write_register(u8 index, u32 value)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%08x to cop0 register %s", value, register_names[index]);
    }

    m_registers[index] = value;
}

u32 Coprocessor0::read_register(u8 index) const
{
    auto value = m_registers[index];

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Read 0x%08x from cop0 register %s", value, register_names[index]);
    }

    return value;
}

bool Coprocessor0::irq_enabled() const
{
    return ((m_registers[STATUS_REGISTER_INDEX] & 0x1u) != 0u);
}

bool Coprocessor0::is_interrupt_masked() const
{
    // Check if SR bits 8-10 are set (0x700)
    return (((m_registers[STATUS_REGISTER_INDEX] & 0x700u) != 0u));
}

void Coprocessor0::set_cause_interrupt_bit()
{
    m_registers[CAUSE_REGISTER_INDEX] |= (1u << 10u);
}

void Coprocessor0::unset_cause_interrupt_bit()
{
    m_registers[CAUSE_REGISTER_INDEX] &= ~(1u << 10u);
}

}
