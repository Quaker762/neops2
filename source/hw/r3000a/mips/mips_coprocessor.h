/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{
class R3000;
}

namespace PSX::MIPS
{

static constexpr u8 COP_MFC = 0b0000;
static constexpr u8 COP_MTC = 0b0100;
static constexpr u8 COP_RFE = 0b10000;

class Coprocessor
{
public:
    friend class PSX::R3000;

    Coprocessor() = default;
    virtual ~Coprocessor();

private:
    virtual void write_register(u8 index, u32 value) = 0;
    virtual u32 read_register(u8 index) const = 0;
};

}
