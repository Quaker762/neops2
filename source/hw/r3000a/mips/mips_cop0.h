/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/r3000a/mips/mips_coprocessor.h"
#include "types.h"

namespace PSX::MIPS
{

class Coprocessor0 : Coprocessor
{
private:
    static constexpr size_t NUMBER_OF_REGISTERS = 64u;

    static constexpr const char* const register_names[NUMBER_OF_REGISTERS] =
    {
        "",
        "",
        "",
        "BPC",
        "",
        "BDA",
        "JUMPDEST",
        "DCIC",
        "BadVaddr",
        "BDAM",
        "",
        "BPCM",
        "SR",
        "CAUSE",
        "EPC",
        "PRID",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
    };

    union StatusRegister
    {
        // Register bits
        struct
        {
            u32 IEc         : 1;
            u32 KUc         : 1;
            u32 IEp         : 1;
            u32 KUp         : 1;
            u32 IEo         : 1;
            u32 KUo         : 1;
            u32 unused1     : 2;
            u32 Im          : 8;
            u32 ISc         : 1;
            u32 SWc         : 1;
            u32 PZ          : 1;
            u32 CM          : 1;
            u32 PE          : 1;
            u32 TS          : 1;
            u32 BEV         : 1;
            u32 unused2     : 2;
            u32 RE          : 1;
            u32 unused3     : 1;
            u32 CU0_Enable  : 1;
            u32 CU1_Enable  : 1;
            u32 CU2_Enable  : 1;
            u32 CU3_Enable  : 1;
        } bits;

        u32 m_word;
    };

    enum class ExceptionCauses : u32
    {
        INT     = 0u,
        AdEL    = 4u,
        AdES    = 5u,
        IBE     = 6u,
        DBE     = 7u,
        Sys     = 8u,
        Bp      = 9u,
        RI      = 10u,
        CpU     = 11u,
        Ov      = 12u,
        Tr      = 13u,
        FPE     = 14u,
    };

    union CauseRegister
    {
        // Register bits
        struct
        {
            u32 always0_1   : 2;
            u32 ExcCode     : 5;
            u32 always0_2   : 1;
            u32 IP          : 8;
            u32 always0_3   : 12;
            u32 CE          : 2;
            u32 always0_4   : 1;
            u32 BD          : 1;
        } bits;

        u32 m_word;
    };

public:
    static constexpr size_t STATUS_REGISTER_INDEX   = 12;
    static constexpr size_t CAUSE_REGISTER_INDEX    = 13;
    static constexpr size_t EPC_REGISTER_INDEX      = 14;

    friend class PSX::R3000;

    virtual ~Coprocessor0();

private:
    virtual void write_register(u8 index, u32 value) override;
    virtual u32 read_register(u8 index) const override;

    StatusRegister sr() const { return StatusRegister { .m_word = m_registers[STATUS_REGISTER_INDEX] }; }
    CauseRegister cause() const { return CauseRegister { .m_word = m_registers[CAUSE_REGISTER_INDEX] }; }

    bool irq_enabled() const;
    bool is_interrupt_masked() const;

    void write_status(u32 value) { m_registers[STATUS_REGISTER_INDEX] = value; }
    void write_cause(u32 value) { m_registers[CAUSE_REGISTER_INDEX] = value; }
    void write_epc(vaddr_t address)  { m_registers[EPC_REGISTER_INDEX] = address; }

    void set_cause_interrupt_bit();
    void unset_cause_interrupt_bit();

    u8 status_int_mask() const { return sr().bits.Im; }
    u8 cause_int_pend() const { return cause().bits.IP; }

private:
    u32 m_registers[NUMBER_OF_REGISTERS];
};

}
