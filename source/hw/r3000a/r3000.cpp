/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/r3000a/r3000.h"
#include "hw/system/bus.h"
#include "util/log.h"
#include <cstring>

using namespace PSX::MIPS;

namespace PSX
{

static constexpr bool R3000_DEBUG = false;

void R3000::dump_registers_to_log()
{
    for (auto i = 0u; i < MIPS_NUMBER_OF_GPR; i++)
    {
        log(LogLevel::NONE, "%s: 0x%08x", g_register_names[i], m_gprs.m_gprs[i]);
    }

    log(LogLevel::NONE, "$pc: 0x%08x", m_executing_pc);
}

u32 R3000::read_register(size_t offset) const
{
    return m_gprs.m_gprs[offset];
}

void R3000::write_register(size_t offset, u32 value)
{
    m_gprs.m_delayed_gprs[offset]   = value;
    m_gprs.m_gprs[0]                = 0u;
    m_gprs.m_delayed_gprs[0]        = 0u;
}

template <>
u8 R3000::bus_read(vaddr_t address)
{
    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    return PSX::Bus::the()->read<u8>(physical_address);
}

template <>
u16 R3000::bus_read(vaddr_t address)
{
    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    return PSX::Bus::the()->read<u16>(physical_address);
}

template <>
u32 R3000::bus_read(vaddr_t address)
{
    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    return PSX::Bus::the()->read<u32>(physical_address);
}

template <>
void R3000::bus_write(vaddr_t address, u8 data)
{
    if (m_cop0.sr().bits.ISc == 1)
    {
        if constexpr (R3000_DEBUG)
        {
            log(LogLevel::WARN, "Not writing 8-bit value 0x%02x to 0x%08x due to cache isolation", data, address);
        }
        return;
    }

    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    PSX::Bus::the()->write<u8>(physical_address, data);
}

template <>
void R3000::bus_write(vaddr_t address, u16 data)
{
    if (m_cop0.sr().bits.ISc == 1)
    {
        if constexpr (R3000_DEBUG)
        {
            log(LogLevel::WARN, "Not writing 16-bit value 0x%04x to 0x%08x due to cache isolation", data, address);
        }
        return;
    }

    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    PSX::Bus::the()->write<u16>(physical_address, data);
}

template <>
void R3000::bus_write(vaddr_t address, u32 data)
{
    if (m_cop0.sr().bits.ISc == 1u)
    {
        if constexpr (R3000_DEBUG)
        {
            log(LogLevel::WARN, "Not writing 32-bit value 0x%08x to 0x%08x due to cache isolation", data, address);
        }

        return;
    }

    auto mask                   = SEGMENT_ADDRESS_MASKS[address >> 29u];
    paddr_t physical_address    = (address & mask);

    PSX::Bus::the()->write<u32>(physical_address, data);
}


void R3000::schedule_load_delay(u8 index, u32 value)
{
    m_load_delay_register_index = index;
    m_load_delay_register_value = value;
}

Instruction R3000::fetch_instruction() const
{
    // FIXME: Check PC alignment and raise exception if ((pc & 3) != 0)
    paddr_t masked_pc = (m_executing_pc & PC_ADDRESS_MASK);    // Mask of the upper bits of the segment (irrelevant for now)
    u32 instruction = Bus::the()->read<u32>(masked_pc);

    return { .m_word = instruction };
}

// See IDTR3051.PDF, page 71, "Returning From Exceptions"
void R3000::rfe()
{
    // It's up to the programmer to restore _all_ of the state. We just
    // need to restore the I** and K** bits in COP0.SR as per
    // page 68 of the manual
    auto status = m_cop0.sr();

    status.bits.KUc = status.bits.KUp;
    status.bits.IEc = status.bits.IEp;
    status.bits.KUp = status.bits.KUo;
    status.bits.IEp = status.bits.IEo;

    m_cop0.write_status(status.m_word);
}

// See IDTR3051.PDF, page 84, "System Call Exception"
void R3000::syscall()
{
    raise_exception(ExceptionCauses::Sys);
}

void R3000::raise_exception(ExceptionCauses cause)
{
    vaddr_t epc             = m_executing_pc;
    auto cause_register     = m_cop0.cause();
    auto status_register    = m_cop0.sr();

    // Adjust for branch delay
    if (in_branch_delay())
    {
        epc -= 4u;
        cause_register.bits.BD = 1u;
    }

    // Set the ExcCode in the status register
    cause_register.bits.ExcCode = (static_cast<u32>(cause) & 0x1Fu);

    // "The R3051/ 52 saves the KUp, IEp, KUc, and IEc bits of the status register
    // in the KUo, IEo, KUp, and IEp bits, respectively, and clears the KUc and IEc
    // bits."
    status_register.bits.KUo = status_register.bits.KUp;
    status_register.bits.IEo = status_register.bits.IEp;
    status_register.bits.KUp = status_register.bits.KUc;
    status_register.bits.IEp = status_register.bits.IEc;
    status_register.bits.KUc = 0u;
    status_register.bits.IEc = 0u;

    // Write back to the coprocessor
    m_cop0.write_epc(epc);
    m_cop0.write_cause(cause_register.m_word);
    m_cop0.write_status(status_register.m_word);

    // Immediately jump to the exception routine
    if (status_register.bits.BEV)
    {
        m_pc        = MIPS_EXCEPTION_ADDRESS_GENERAL_BEV;
        m_next_pc   = MIPS_EXCEPTION_ADDRESS_GENERAL_BEV + 4u;
    }
    else
    {
        m_pc        = MIPS_EXCEPTION_ADDRESS_GENERAL;
        m_next_pc   = MIPS_EXCEPTION_ADDRESS_GENERAL + 4u;
    }
}

void R3000::write_irq_stat(u32 data)
{
    m_irq_stat &= data;
    handle_active_interrupts();
}

void R3000::execute_instruction()
{
    auto opcode = m_current_instruction.i_type.op;

    switch (opcode)
    {
    case OPCODE_ADDI:
        addi();
        break;
    case OPCODE_ADDIU:
        addiu();
        break;
    case OPCODE_ANDI:
        andi();
        break;
    case OPCODE_BCOND:
        bcondz();
        break;
    case OPCODE_BEQ:
        beq();
        break;
    case OPCODE_BGTZ:
        bgtz();
        break;
    case OPCODE_BLEZ:
        blez();
        break;
    case OPCODE_BNE:
        bne();
        break;
    case OPCODE_COP0:
    case OPCODE_COP1:
    case OPCODE_COP2:
    case OPCODE_COP3:
        copn((m_current_instruction.r_type.op & 0b11));
        break;
    case OPCODE_J:
        j();
        break;
    case OPCODE_JAL:
        jal();
        break;
    case OPCODE_LUI:
        lui();
        break;
    case OPCODE_LB:
        lb();
        break;
    case OPCODE_LH:
        lh();
        break;
    case OPCODE_LBU:
        lbu();
        break;
    case OPCODE_LHU:
        lhu();
        break;
    case OPCODE_LWL:
        lwl();
        break;
    case OPCODE_LWR:
        lwr();
        break;
    case OPCODE_LW:
        lw();
        break;
    case OPCODE_LWC0:
        raise_exception(ExceptionCauses::CpU);
        break;
    case OPCODE_LWC1:
        raise_exception(ExceptionCauses::CpU);
        break;
    case OPCODE_LWC2:
        Util::die("Unhandled LWC from GTE Coprocessor!");
        break;
    case OPCODE_LWC3:
        raise_exception(ExceptionCauses::CpU);
        break;
    case OPCODE_ORI:
        ori();
        break;
    case OPCODE_SPECIAL:
        special();
        break;
    case OPCODE_SB:
        sb();
        break;
    case OPCODE_SH:
        sh();
        break;
    case OPCODE_SLTI:
        slti();
        break;
    case OPCODE_SLTIU:
        sltiu();
        break;
    case OPCODE_SW:
        sw();
        break;
    case OPCODE_SWL:
        swl();
        break;
    case OPCODE_SWR:
        swr();
        break;
    case OPCODE_XORI:
        xori();
        break;
    default:
        invalid_instruction(m_current_instruction);
    }
}

bool R3000::is_in_bios_call() const
{
    u32 masked_pc = (m_executing_pc & 0x1FFFFFFF);
    if ((masked_pc == 0xA0) || (masked_pc == 0xB0))
    {
        return true;
    }

    return false;
}

void R3000::invalid_instruction(Instruction const& instruction)
{
    Util::die("Unimplemented or invalid instruction 0x%08x @ 0x%08x!", instruction.m_word, m_executing_pc);
}

static void do_bios_print(char const character)
{
    static char buff[512u];
    static size_t i = 0u;

    if (character == '\n')
    {
        buff[i++] = '\0';
        log(LogLevel::BIOS, "%s", &buff[0]);

        i = 0u;
        return;
    }

    buff[i++] = character;
}

void R3000::cycle()
{
    m_in_delay_slot = m_branch_delay;
    m_branch_delay  = false;

    m_executing_pc  = m_pc;
    m_pc            = m_next_pc;
    m_next_pc       += sizeof(Instruction::m_word);

    check_and_execute_irq();
    m_current_instruction = fetch_instruction();

    if (is_in_bios_call())
    {
        if (m_gprs.m_gprs[9] == 0x3D)
        {
            do_bios_print(static_cast<char>(m_gprs.m_gprs[4]));
        }
    }

    // When a load instruction occurs, the value loaded is NOT available to the
    // processor on the next instruction clock. It takes another full cycle before it is available
    // to the processor, ergo, if we were to perform the following:
    //
    // lw $t0, 0xabcd
    // xor $t0
    // xor $t0
    //
    // The operation proceeding `lw` would be the OLD value of $t0. Generally, the compiler is smart enough
    // to insert a `nop` here to account for branch delay.
    // As this is the case, all load instructions write into the branch delay slot instead of
    // directly writing to either register file. On the next cycle, we this value into the shadow
    // register file (the "delay" reg file). At the end of _that_ instruction, the value is then
    // copied into the regular register file, and the data is available to instructions.
    write_register(m_load_delay_register_index, m_load_delay_register_value);
    m_load_delay_register_index = 0u;
    m_load_delay_register_value = 0u;

    // Execute instruction
    execute_instruction();

    // Copy the delayed register file into the "real" register file
    (void)memcpy(&m_gprs.m_gprs[0], &m_gprs.m_delayed_gprs[0], sizeof(m_gprs.m_delayed_gprs));
}


}
