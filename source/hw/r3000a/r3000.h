/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"
#include "hw/r3000a/mips/mips.h"
#include "hw/r3000a/mips/mips_cop0.h"
#include "hw/system/io.h"

namespace PSX
{

//
// Implementation of the R3000a Processor found in the original SONY PlayStation
//
class R3000
{
    friend class Coprocessor0;

    static constexpr u32 PC_ADDRESS_MASK = 0x1FFFFFFFu;

    struct GeneralRegisters
    {
        u32 m_gprs[MIPS_NUMBER_OF_GPR];
        u32 m_delayed_gprs[MIPS_NUMBER_OF_GPR];
    };

    // Yeah, I know what you're thinking; "boundary scan is impossible, why is this called JTAG!!?". Fair enough.
    // Unlike ARM, however, there doesn't seem to be any standard name for debug access (like 'DAP'), so "JTAG"
    // is the closest name that will probably make sense to the most people besides "Debugger", which this clearly
    // is not.
    struct JTAG
    {
        JTAG(R3000& cpu) : m_cpu(cpu) {}
        u32 read_gpr(u8 index) const;
        u32 read_pc() const;
        u32 read_next_pc() const;
        u32 read_hi() const;
        u32 read_lo() const;
        u32 read_cop0_register(u8 index) const;

        template <typename T>
        T bus_read(vaddr_t address) { return m_cpu.bus_read<T>(address); }

        void write_hi(u32 value);
        void write_lo(u32 value);
        void write_gpr(u8 index, u32 value) const;
        void write_cop0_register(u8 index, u32 data) const;
        void force_pc(u32 pc);
        void clock();

    private:
        R3000& m_cpu;
    };

    using ExceptionCauses = MIPS::Coprocessor0::ExceptionCauses;

public:
    R3000() = default;

    void reset();
    void cycle();

    JTAG& jtag() { return m_jtag; }

    void dump_registers_to_log();

    void write_irq_stat(u32 data);
    void write_irq_mask(u32 data) { m_irq_mask = data; }
    u32 read_irq_stat() const { return m_irq_stat; }
    u32 read_irq_mask() const { return m_irq_mask; }

    void raise_irq(u8 irqn);
    void check_and_execute_irq();

private:
    // Opcode implementations
    void addi();
    void addiu();
    void andi();
    void bcondz();
    void beq();
    void bgtz();
    void blez();
    void bne();
    void copn(u8 coprocessor);
    void j();
    void jal();
    void lui();
    void lb();
    void lbu();
    void lh();
    void lhu();
    void lw();
    void lwl();
    void lwr();
    void ori();
    void sb();
    void sh();
    void slti();
    void sltiu();
    void sw();
    void swl();
    void swr();
    void special();
    void xori();

    // Special opcode functions
    void special_add();
    void special_addu();
    void special_and();
    void special_break();
    void special_div();
    void special_divu();
    void special_jalr();
    void special_jr();
    void special_mfhi();
    void special_mthi();
    void special_mflo();
    void special_mtlo();
    void special_mult();
    void special_multu();
    void special_nor();
    void special_or();
    void special_sll();
    void special_sllv();
    void special_slt();
    void special_sltu();
    void special_sra();
    void special_srav();
    void special_srl();
    void special_srlv();
    void special_sub();
    void special_subu();
    void special_syscall();
    void special_xor();

    // Helper functions
    void branch(u16 offset);
    void rfe();
    void syscall();
    void raise_exception(ExceptionCauses cause);

private:
    u32 read_register(size_t offset) const;
    void write_register(size_t offset, u32 value);
    void schedule_load_delay(u8 index, u32 value);

    template <typename T>
    T bus_read(vaddr_t address);

    template <typename T>
    void bus_write(vaddr_t address, T const data);

    template <typename T>
    bool is_unaligned_access(paddr_t address) { return (address & (sizeof(T)-1)) != 0; }

    bool in_branch_delay() const { return m_in_delay_slot; }

    Instruction fetch_instruction() const;
    void execute_instruction();
    void invalid_instruction(Instruction const& instruction);

    bool is_in_bios_call() const;

private:
    u8 interrupt_mask() const;
    bool interrupt_pending() const;
    bool interrupts_enabled() const;

    void handle_active_interrupts();

private:
    u32 m_executing_pc { MIPS_INITIAL_PC_VALUE };
    u32 m_pc { MIPS_INITIAL_PC_VALUE };
    u32 m_next_pc { MIPS_INITIAL_PC_VALUE + 4 };
    bool m_branch_delay { false };
    bool m_in_delay_slot { false };
    u8  m_load_delay_register_index { 0u };
    u32 m_load_delay_register_value { 0u };
    u32 m_hi;
    u32 m_lo;

    // Interrupt related data
    u32 m_irq_stat;
    u32 m_irq_mask;

    GeneralRegisters    m_gprs;
    JTAG                m_jtag { *this };
    Instruction         m_current_instruction;
    MIPS::Coprocessor0  m_cop0;
};

}
