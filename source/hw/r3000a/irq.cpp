/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/r3000a/r3000.h"
#include "util/log.h"

namespace PSX
{

static constexpr u8 MAX_IRQN        = 10u;
static constexpr u8 DEBUG_ENABLE    = true;

void R3000::raise_irq(u8 irqn)
{
    // Write the corresponding bit to the I_STAT register
    if (irqn > MAX_IRQN)
    {
        log(LogLevel::WARN, "Invalid IRQN %u", irqn);
        return;
    }

    m_irq_stat |= (1u << irqn);
    handle_active_interrupts();
}

bool R3000::interrupt_pending() const
{
    return (m_irq_mask & m_irq_stat) > 0u;
}

bool R3000::interrupts_enabled() const
{
    return (m_cop0.sr().bits.IEc == 1u);
}

u8 R3000::interrupt_mask() const
{
    return m_cop0.sr().bits.Im;
}

void R3000::handle_active_interrupts()
{
    if (interrupt_pending())
    {
        m_cop0.set_cause_interrupt_bit();
    }
    else
    {
        m_cop0.unset_cause_interrupt_bit();
    }
}

void R3000::check_and_execute_irq()
{
    auto cause_register     = m_cop0.cause();
    auto status_register    = m_cop0.sr();

    if (m_cop0.irq_enabled())
    {
        if ((cause_register.bits.IP & status_register.bits.Im) != 0u)
        {
            if constexpr (DEBUG_ENABLE)
            {
                log(LogLevel::INFO, "Raising IRQ! cause.IP = 0x%x SR.Im = 0x%x", cause_register.bits.IP, status_register.bits.Im);
            }

            raise_exception(ExceptionCauses::INT);
        }
    }
}

}
