/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/spu/spu.h"
#include "util/log.h"

namespace PSX
{

static constexpr bool SPU_DEBUG = false;

u8 SPU::read8(paddr_t address)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 8-bit read from SPU @ 0x%08x", address);
    }

    return 0;
}

u16 SPU::read16(paddr_t address)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 16-bit read from SPU @ 0x%08x", address);
    }

    return 0;
}

u32 SPU::read32(paddr_t address)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 32-bit read from SPU @ 0x%08x", address);
    }

    return 0;
}

void SPU::write8(paddr_t address, u8)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 8-bit write to SPU @ 0x%08x", address);
    }
}

void SPU::write16(paddr_t address, u16)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 16-bit write to SPU @ 0x%08x", address);
    }
}

void SPU::write32(paddr_t address, u32)
{
    if constexpr (SPU_DEBUG)
    {
        log(LogLevel::INFO, "Attempted 32-bit write to SPU @ 0x%08x", address);
    }
}

}
