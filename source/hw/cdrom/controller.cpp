/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/bus.h"
#include "hw/cdrom/controller.h"
#include "util/log.h"

namespace PSX
{

static constexpr bool DEBUG_ENABLE  = true;

static constexpr u8 PORT1_INDEX0_COMMAND_REGISTER           = 0x00u;
static constexpr u8 PORT1_INDEX1_RESPONSE_FIFO_R            = 0x01u;
static constexpr u8 PORT2_INDEX0_PARAM_FIFO                 = 0x00u;
static constexpr u8 PORT2_INDEX1_IEN_REGISTER               = 0x01u;
static constexpr u8 PORT3_INDEX1_INTERRUPT_FLAG_REGISTER_RW = 0x01u;

CDROMController::CDROMController() : BusDevice(CDROM_ADDRESS_RANGE_START, CDROM_ADDRESS_RANGE_END)
{
    m_index_status.bits.prmempt = 1u;   // Value of '1' signifies that the Parmeter FIFO is empty!
    m_stat_code.bits.shell_open = 1u;   // Start off with the CD-Shell open by default (so we boot to the BIOS menu)

    // Set up the command table
    for (auto i = 0u; i < sizeof(m_command_table) / sizeof(ControllerCommand); i++)
    {
        m_command_table[i] = nullptr;
    }

    m_command_table[0x01] = &PSX::CDROMController::command_get_stat;
    m_command_table[0x19] = &PSX::CDROMController::command_test;
}

void CDROMController::cycle()
{
    if ((m_irq_enable & m_irq_flag_register.bits.int1_int7) != 0u)
    {
        Bus::the()->cpu().raise_irq(2u);
    }
}

void CDROMController::write_parameter_fifo(u8 const value)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "cdrom: Writing 0x%02x to parameter FIFO", value);
    }

    auto did_write = m_parameter_fifo.enqueue(value);
    if (did_write == false)
    {
        log(LogLevel::ERROR, "cdrom: Parameter FIFO full!");
    }
}

// We need to ignore -Wconversion here
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
void CDROMController::generate_irq(CDROMIRQ irq)
{
    u8 irq_as_u8 = static_cast<u8>(irq);

    m_irq_flag_register.bits.int1_int7 = irq_as_u8;
}
#pragma GCC diagnostic pop

void CDROMController::handle_address0_write(u8 const& data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%02x to 1F801800 @ index %u", data, port_index());
    }

    m_index_status.bits.index = (data & INDEX_STATUS_WRITE_MASK);
}

void CDROMController::handle_address1_write(u8 const& data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%02x to 1F801801 @ index %u", data, port_index());
    }

    switch (port_index())
    {
    case PORT1_INDEX0_COMMAND_REGISTER:
        handle_command_write(data);
        break;
    default:
        Util::die("Attempted write to CDROM Controller of 0x%02x to Port Index %u @ 1F801801", data, port_index());
    }
}

void CDROMController::handle_address2_write(u8 const& data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%02x to 1F801802 @ index %u", data, port_index());
    }

    switch (port_index())
    {
    case PORT2_INDEX0_PARAM_FIFO:
        write_parameter_fifo(data);
        break;
    case PORT2_INDEX1_IEN_REGISTER:
        m_irq_enable = (data & 0x1Fu);
        break;
    default:
        Util::die("Attempted write to CDROM Controller of 0x%02x to Port Index %u @ 1F801802", data, port_index());
    }
}

void CDROMController::handle_address3_write(u8 const& data)
{
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Writing 0x%02x to 1F801803 @ index %u", data, port_index());
    }

    switch (port_index())
    {
    case PORT3_INDEX1_INTERRUPT_FLAG_REGISTER_RW:
        // This register is "Write 1 Clear)
        m_irq_flag_register.m_byte = (m_irq_flag_register.m_byte & ~data);
        break;
    default:
        Util::die("Attempted write to CDROM Controller of 0x%02x to Port Index %u @ 1F801803", data, port_index());
    }
}

u8 CDROMController::handle_address0_read()
{
    IndexStatusRegister index_status_register = m_index_status;

    // Set up some of the bits in the status register before returning it
    index_status_register.bits.prmempt = (m_parameter_fifo.empty()) ? 1u : 0u;
    index_status_register.bits.prmwrdy = (m_parameter_fifo.is_full()) ? 1u : 0u;
    index_status_register.bits.rslrrdy = (m_response_fifo.empty()) ? 1u : 0u;

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Reading 0x%02x from 1F801800 @ index %u", index_status_register.m_byte, port_index());
    }

    return index_status_register.m_byte;
}

u8 CDROMController::handle_address1_read()
{
    u8 data = 0u;

    switch (port_index())
    {
    case PORT1_INDEX1_RESPONSE_FIFO_R:
        data = m_response_fifo.dequeue();
        break;
    default:
        Util::die("Attempted 1F801801 read of Port Index %u from CDROM Controller!", port_index());
    }

    /*
    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Reading 0x%02x from 1F801801 @ index %u", data, port_index());
    }
    */

    return data;
}

u8 CDROMController::handle_address2_read()
{
    Util::die("Attempted 1F801802 read of Port Index %u from CDROM Controller!", port_index());
}

u8 CDROMController::handle_address3_read()
{
    u8 data = 0u;

    switch (port_index())
    {
    case PORT3_INDEX1_INTERRUPT_FLAG_REGISTER_RW:
        data = (m_irq_flag_register.m_byte | 0xE0u);
        break;
    default:
        Util::die("Attempted 1F801803 read of Port Index %u from CDROM Controller!", port_index());
    }

    if constexpr (DEBUG_ENABLE)
    {
        log(LogLevel::INFO, "Reading 0x%02x from 1F801803 @ index %u", data, port_index());
    }

    return data;
}

u8 CDROMController::read8(paddr_t address)
{
    u8 reg = (address & 3u);
    u8 data = 0u;

    switch (reg)
    {
    case 0u:
        data = handle_address0_read();
        break;
    case 1u:
        data = handle_address1_read();
        break;
    case 2u:
        data = handle_address2_read();
        break;
    case 3u:
        data = handle_address3_read();
        break;
    }

    return data;
}

u16 CDROMController::read16(paddr_t address)
{
    Util::die("Attempted 16-bit CDROM Controller read from 0x%08x", address);
}

u32 CDROMController::read32(paddr_t address)
{
    Util::die("Attempted 32-bit CDROM Controller read from 0x%08x", address);
}

void CDROMController::write8(paddr_t address, u8 data)
{
    u8 reg = (address & 3u);
    switch (reg)
    {
    case 0u:
        handle_address0_write(data);
        break;
    case 1u:
        handle_address1_write(data);
        break;
    case 2u:
        handle_address2_write(data);
        break;
    case 3u:
        handle_address3_write(data);
        break;
    }
}

void CDROMController::write16(paddr_t address, u16 data)
{
    Util::die("Attempted 16-bit write of %04x to %08x to CDROM Controller!", address, data);
}

void CDROMController::write32(paddr_t address, u32 data)
{
    Util::die("Attempted 32-bit write of %08x to %08x to CDROM Controller!", address, data);
}

}
