/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "hw/bus_device.h"
#include "hw/cdrom/fifo.h"
#include "hw/cdrom/registers.h"
#include "types.h"

namespace PSX
{

class CDROMController : public BusDevice
{
    static constexpr paddr_t CDROM_ADDRESS_RANGE_START      = 0x1F801800u;
    static constexpr paddr_t CDROM_ADDRESS_RANGE_END        = 0x1F801803u;

    static constexpr u8 INDEX_STATUS_WRITE_MASK             = 3u;
    static constexpr u8 IRQFLAG_REGISTER_WRITE_MASK         = 0x1Fu;

    static constexpr size_t NUMBER_OF_COMMANDS              = 255u;
    using ControllerCommand = void(PSX::CDROMController::*)();

    enum class CDROMIRQ : u8
    {
        NO_RESPONSE                     = 0u,
        READS_READN_SECOND_RESPONSE     = 1u,
        SECOND_RESPONSE                 = 2u,
        FIRST_RESPONSE                  = 3u,
        DATA_END                        = 4u,
        ERROR_CODE_RESPONSE             = 5u,
    };

public:
    CDROMController();
    virtual ~CDROMController(){}

    virtual u8 read8(paddr_t address) override;
    virtual u16 read16(paddr_t address) override;
    virtual u32 read32(paddr_t address) override;

    virtual void write8(paddr_t address, u8 data) override;
    virtual void write16(paddr_t address, u16 data) override;
    virtual void write32(paddr_t address, u32 data) override;

    void cycle();

    void handle_address0_write(u8 const& data);
    void handle_address1_write(u8 const& data);
    void handle_address2_write(u8 const& data);
    void handle_address3_write(u8 const& data);

    u8 handle_address0_read();
    u8 handle_address1_read();
    u8 handle_address2_read();
    u8 handle_address3_read();

    inline u8 port_index() const { return m_index_status.bits.index; }

private:
    void handle_command_write(u8 const command);
    void write_parameter_fifo(u8 const value);

    void generate_irq(CDROMIRQ irq);

private:
    // Command functions
    void command_get_stat();
    void command_test();

private:
    CDROMFIFO           m_parameter_fifo;
    CDROMFIFO           m_response_fifo;
    IndexStatusRegister m_index_status;
    IRQFlagRegister     m_irq_flag_register;
    StatusCode          m_stat_code;
    u8                  m_irq_enable;

    ControllerCommand   m_command_table[NUMBER_OF_COMMANDS];
};


}
