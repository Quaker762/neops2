/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"

namespace PSX
{

union IndexStatusRegister
{
    struct
    {
        u8 index        : 2u;   // IO Index
        u8 adpbusy      : 1u;   // XA-ADPCM FIFO Empty (0 = Empty)
        u8 prmempt      : 1u;   // Parameter FIFO empty (1 = Empty)
        u8 prmwrdy      : 1u;   // Parammeter FIFO full (0 = Full)
        u8 rslrrdy      : 1u;   // Response FIFO empty (0 = Empty)
        u8 drqsts       : 1u;   // Data FIFO empty (0 = Empty)
        u8 busysts      : 1u;   // Command/Param transmission busy (1 = Busy)
    } bits;

    u8 m_byte;
};

union IRQFlagRegister
{
    struct
    {
        //
        //  INT0   No response received (no interrupt request)
        //  INT1   Received SECOND (or further) response to ReadS/ReadN (and Play+Report)
        //  INT2   Received SECOND response (to various commands)
        //  INT3   Received FIRST response (to any command)
        //  INT4   DataEnd (when Play/Forward reaches end of disk) (maybe also for Read?)
        //  INT5   Received error-code (in FIRST or SECOND response)
        //          INT5 also occurs on SECOND GetID response, on unlicensed disks
        //          INT5 also occurs when opening the drive door (even if no command
        //              was sent, ie. even if no read-command or other command is active)
        //  INT6   N/A
        //  INT7   N/A
        //
        u8 int1_int7        : 3u;
        u8 int8_unknown     : 1u;   // Usually 0. Write 1 to acknowledge (INT8  ;XXX CLRBFEMPT)
        u8 int10_cmd_start  : 1u;   // INT10h;XXX CLRBFWRDY
        u8 always1_1        : 1u;   // ;XXX SMADPCLR
        u8 reset_param_fifo : 1u;   // ;XXX CLRPRM
        u8 always1_2        : 1u;   // ;XXX CHPRST
    } bits;

    u8 m_byte;
};

union StatusCode
{
    struct
    {
        u8 error            : 1;
        u8 spindle_motor    : 1;
        u8 seek_error       : 1;
        u8 id_error         : 1;
        u8 shell_open       : 1;
        u8 read             : 1;
        u8 seek             : 1;
        u8 play             : 1;
    } bits;

    u8 m_byte;
};

}
