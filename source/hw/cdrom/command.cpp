/*
 * This file is part of NeoPS
 * Copyright (c) 2023 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/cdrom/controller.h"
#include "util/log.h"

namespace PSX
{

static constexpr u8 TEST_COMMAND_GET_BIOS_DATE = 0x20u;

void CDROMController::command_get_stat()
{
    m_response_fifo.enqueue(m_stat_code.m_byte);
    generate_irq(CDROMIRQ::FIRST_RESPONSE);
}

void CDROMController::command_test()
{
    if (m_parameter_fifo.empty())
    {
        Util::die("cdrom: Got test command with empty command FIFO!");
    }

    auto sub_command = m_parameter_fifo.dequeue();
    switch (sub_command)
    {
    case TEST_COMMAND_GET_BIOS_DATE:
        // Write the Date and Version of the CDROM firmware into the response FIFO and raise CDROM INT3
        m_response_fifo.enqueue(0x95);
        m_response_fifo.enqueue(0x05);
        m_response_fifo.enqueue(0x16);
        m_response_fifo.enqueue(0xC1);
        generate_irq(CDROMIRQ::FIRST_RESPONSE);
        break;
    default:
        Util::die("cdrom: Unknown Test sub-command, 0x%02x", sub_command);
    }
}

void CDROMController::handle_command_write(u8 const command)
{
    ControllerCommand function = m_command_table[command];
    if (function == nullptr)
    {
        Util::die("cdrom: Unimplemented controller command, 0x%02x", command);
    }

    (*this.*function)();    // What the fuck?
}

}
