/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <queue>
#include <cstddef>
#include "types.h"

namespace PSX
{

class CDROMFIFO
{
    static constexpr size_t NUMBER_OF_BYTE_ENTRIES = 16u;

public:
    bool enqueue(u8 const& value)
    {
        if (is_full())
        {
            return false;
        }

        m_fifo.push(value);
        return true;
    }

    u8 dequeue()
    {
        u8 value = m_fifo.front();
        m_fifo.pop();

        return value;
    }

    bool is_full() const { return m_fifo.size() == NUMBER_OF_BYTE_ENTRIES; }
    bool empty() const { return m_fifo.empty(); }
    void flush() { while(!m_fifo.empty()) { m_fifo.pop(); } }

private:
    std::queue<u8> m_fifo;
};

}
