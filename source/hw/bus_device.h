/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <types.h>

namespace PSX
{

// Class describing an I/O device that sits on the bus that isn't the CPU
class BusDevice
{
public:
    virtual u8 read8(paddr_t address) = 0;
    virtual u16 read16(paddr_t address) = 0;
    virtual u32 read32(paddr_t address) = 0;

    virtual void write8(paddr_t address, u8 data) = 0;
    virtual void write16(paddr_t address, u16 data) = 0;
    virtual void write32(paddr_t address, u32 data) = 0;

    paddr_t address_lo() const { return m_address_low; }
    paddr_t address_hi() const { return m_address_high; }

protected:
    BusDevice() = delete;
    BusDevice(paddr_t address_low, paddr_t address_high) :
        m_address_low(address_low),
        m_address_high(address_high)
    {

    }

    virtual ~BusDevice() = 0;

private:
    paddr_t m_address_low;
    paddr_t m_address_high;
};

}
