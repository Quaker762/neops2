/*
 * This file is part of NeoPS
 * Copyright (c) 2016-2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <unordered_map>
#include <cstdint>
#include <fstream>

namespace Util {

class ConfigurationFile
{
public:
    static constexpr const char*  FILENAME              = "NeoPS.cfg"; /**< Configuration file name */
    static constexpr bool           BOOL_DEFAULT_VAL    = false;
    static constexpr int            INT_DEFAULT_VAL     = 0;
    static constexpr float          FLOAT_DEFAULT_VAL   = 0.0f;
    static constexpr const char*    STRING_DEFAULT_VAL  = "";

public:
    /**
     * Constructor
     */
    ConfigurationFile();

    /**
     * Load the file from the disk, and populate the section map
     */
    void load();

    /**
     * @brief   Is this configuration file loaded?
     *
     * @return  bool indicating whether or not this configuration file has been loaded
     */
    bool is_loaded() const { return m_loaded; }

    /**
     * Get a value from the configuration file. The section it resides in MUST
     * be specified.
     *
     * @tparam T        Type we want to return (interpreted from options)
     * @param section   Section this option is located in
     *
     * @return Option value
     *
     * @warning This function <i>could</i> cause a crash if an invalid type if selected. Double check you
     * have the correct type before using this function.
     */
    template <typename T>
    T get_config_value(const std::string& section, const std::string& optionName);

private:

    /**
     * Get byte offset of [section]
     *
     * @param sectionName Name of section we want to find
     *
     * @returns Byte offset of section on success, -1 on failure
     */
    int64_t get_section_offset(const std::string& sectionName) const;

private:
    std::ifstream m_cfg_file;
    std::unordered_map<std::string, std::uint64_t> m_section_map;
    bool m_loaded { false };
};

}
