/*
 * This file is part of NeoPS
 * Copyright (c) 2020-2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "types.h"
#include <cstring>

#ifndef __WIN32__
#include <endian.h>
#else
#define htobe16(x) __builtin_bswap16(x)
#define htobe32(x) __builtin_bswap32(x)
#define htobe64(x) __builtin_bswap64(x)
#endif

namespace Util
{

static inline u16 read_swap16(u8* data)
{
    u16 value;
    (void)memcpy(&value, data, sizeof(value));
    return htobe16(value);
}

static inline u32 read_swap32(u8* data)
{
    u32 value;
    (void)memcpy(&value, data, sizeof(value));
    return htobe32(value);
}

static inline u64 read_swap64(u8* data)
{
    u64 value;
    (void)memcpy(&value, data, sizeof(value));
    return htobe64(value);
}

static inline void write_swap16(u8* data, u16 value)
{
    u16 value_swap = htobe16(value);
    (void)memcpy(data, &value_swap, sizeof(value_swap));
}

static inline void write_swap32(u8* data, u32 value)
{
    u32 value_swap = htobe32(value);
    (void)memcpy(data, &value_swap, sizeof(value_swap));
}

static inline void write_swap64(u8* data, u64 value)
{
    u64 value_swap = htobe64(value);
    (void)memcpy(data, &value_swap, sizeof(value_swap));
}

}
