/*
 * This file is part of NeoPS
 * Copyright (c) 2020-2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include "util/log.h"
#include "util/global_config.h"

namespace Util
{

int log(LogLevel logType, const char* str, ...)
{
    static std::FILE* logfile = nullptr;

    std::va_list args;
    auto colored_logging = g_config.get_config_value<bool>("[logging]", "use_colored_logging");
    auto print_to_file [[maybe_unused]] = g_config.get_config_value<bool>("[logging]", "log_to_file");

    if (print_to_file == true)
    {
        if(!logfile)
        {
            static const char* filename = "log.txt";
            if(!(logfile = std::fopen(filename, "w+")))
            {
                std::fprintf(stderr, "Unable to open a handle to %s", filename);
                // fallback to stderr then
                logfile = stderr;
            }
        }
    }
    else
    {
        logfile = stderr;
    }

    const char* label = "";
    switch(logType)
    {
    case LogLevel::BIOS:
        if (colored_logging)
            label = "\033[38;5;198m[bios]\033[0m ";
        else
            label = "[bios] ";
        break;
    case LogLevel::INFO:
        if (colored_logging)
            label = "\033[38;5;45m[info]\033[0m ";
        else
            label = "[info] ";
        break;

    case LogLevel::WARN:
        if (colored_logging)
            label = "\033[38;5;226m[warning]\033[0m ";
        else
            label = "[warning] ";
        break;

    case LogLevel::ERROR:
        if (colored_logging)
            label = "\033[38;5;124m[error]\033[0m ";
        else
            label = "[error] ";
        break;

    case LogLevel::FATAL:
        if (colored_logging)
            label = "\033[31;5m[fatal]\033[0m ";
        else
            label = "[fatal] ";
        break;

    case LogLevel::NONE:
        break;
    }

    va_start(args, str);
    int num_printed = 0;
    if(std::fputs(label, logfile) < 0 || (num_printed = std::vfprintf(logfile, str, args)) < 0)
    {
        std::fprintf(stderr, "Unable to write to flush info log!");
    }
    else
    {
        std::fputc('\n', logfile);
    }

    va_end(args);

    return num_printed;
}

void die(const char* str, ...)
{
    std::va_list    args;
    static char     buff[4096];

    va_start(args, str);
    std::vsnprintf(buff, sizeof(buff), str, args);
    log(LogLevel::FATAL, "%s", buff);
    va_end(args);
    exit(-1);
}

}
