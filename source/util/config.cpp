/*
 * This file is part of NeoPS
 * Copyright (c) 2016-2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "util/config.h"
#include "util/log.h"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

static std::vector<std::string> get_tokens(std::string const& str)
{
    std::stringstream str_stream(str);
    std::istream_iterator<std::string> begin(str_stream);
    std::istream_iterator<std::string> end;

    return std::vector<std::string>(begin, end);
}

namespace Util {

ConfigurationFile::ConfigurationFile()
{
}

void ConfigurationFile::load()
{
    std::string     line;
    std::uint64_t   byte_index = 0;

    if (m_loaded)
    {
        log(LogLevel::WARN, "Configuration file already loaded!");
        return;
    }

    m_cfg_file.open(FILENAME);

    // It seems the file does not exist... Oops!
    if(!m_cfg_file.is_open())
    {
        log(LogLevel::WARN, "Unable to find configuration file! Reverting to default values...");
        return;
    }

    while(std::getline(m_cfg_file, line))
    {
        // Increment the current byte index into then file, which we then will use
        // cache the offset of each section in the file, making a look slightly faster.
        // We also take into account the newline character, which gets swallowed by `getline()`
        byte_index += (line.length() + 1);

        if(line.length() > 0)
        {
            // This line is a comment
            if(((line.at(0) == '/') && (line.at(1) == '/')) || (line.at(0) == '#'))
                continue;

            // This line is the start of a configuration section
            if(line.at(0) == '[' && line.at(line.length() - 1) == ']')
            {
                m_section_map.insert(std::make_pair(line, byte_index)); // Index the beginning of this section. This way we can automatically seek to the correct location
            }
        }
    }

    m_cfg_file.close();
    m_loaded = true;
}

int64_t ConfigurationFile::get_section_offset(std::string const& section_name) const
{
    std::int64_t            ret = -1;

    auto search = m_section_map.find(section_name);
    if(search != m_section_map.end())
        ret = static_cast<int64_t>(search->second);

    return ret;
}

template <>
bool ConfigurationFile::get_config_value(std::string const& section, std::string const& option)
{
    bool        ret = BOOL_DEFAULT_VAL;
    int64_t     offset = get_section_offset(section);
    std::string line;
    std::vector<std::string> split;

    if(offset == -1)
        return ret;

    m_cfg_file.open(FILENAME, std::ios_base::openmode::_S_in);
    m_cfg_file.seekg(offset, std::ios::beg); // Seek to the section location
    while(std::getline(m_cfg_file, line))
    {
        split = get_tokens(line);
        if(split.at(0) == option)
        {
            ret = static_cast<bool>(std::stoi(split.at(1)));
            break;
        }
    }

    m_cfg_file.close();

    return ret;
}

template <>
int ConfigurationFile::get_config_value(std::string const& section, std::string const& option)
{
    int         ret = INT_DEFAULT_VAL;
    int64_t     offset = get_section_offset(section);
    std::string line;
    std::vector<std::string> split;

    if(offset == -1)
        return ret;

    m_cfg_file.open(FILENAME, std::ios_base::openmode::_S_in);
    m_cfg_file.seekg(offset, std::ios::beg); // Seek to the section location
    while(std::getline(m_cfg_file, line))
    {
        split = get_tokens(line);
        if(split.at(0) == option)
        {
            ret = static_cast<int>(std::stoi(split.at(1)));
            break;
        }
    }

    m_cfg_file.close();

    return ret;
}

template <>
float ConfigurationFile::get_config_value(std::string const& section, std::string const& option)
{
    float       ret = FLOAT_DEFAULT_VAL;
    int64_t     offset = get_section_offset(section);
    std::string line;
    std::vector<std::string> split;

    if(offset == -1)
        return ret;

    m_cfg_file.open(FILENAME, std::ios_base::openmode::_S_in);
    m_cfg_file.seekg(offset, std::ios::beg); // Seek to the section location
    while(std::getline(m_cfg_file, line))
    {
        split = get_tokens(line);
        if(split.at(0) == option)
        {
            ret = static_cast<float>(std::stof(split.at(1)));
            break;
        }
    }

    m_cfg_file.close();

    return ret;
}

template <>
std::string ConfigurationFile::get_config_value(std::string const& section, std::string const& option)
{
    std::string ret = STRING_DEFAULT_VAL;
    int64_t     offset = get_section_offset(section);
    std::string line;
    std::vector<std::string> split;

    if(offset == -1)
        return ret;

    m_cfg_file.open(FILENAME, std::ios_base::openmode::_S_in);
    m_cfg_file.seekg(offset, std::ios::beg); // Seek to the section location
    while(std::getline(m_cfg_file, line))
    {
        split = get_tokens(line);
        if(split.at(0) == option)
        {
            ret = split.at(1);
            break;
        }
    }

    m_cfg_file.close();

    return ret;
}

}
