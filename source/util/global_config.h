/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "util/config.h"

namespace Util {

class GlobalConfiguration
{
public:
    GlobalConfiguration();

    template <typename T>
    T get_config_value(const std::string& section, const std::string& option_name) { return m_config_file.get_config_value<T>(section, option_name); }

private:
    ConfigurationFile m_config_file;
};

}

// FIXME: Non-POD static. The C++ spec says nothing about initialisation order.
// This is _bad_!
extern Util::GlobalConfiguration g_config;
