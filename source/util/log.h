/*
 * This file is part of NeoPS
 * Copyright (c) 2020-2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace Util
{

/**
 *  A level of error to tell the user how serious a message is.
 */
enum class LogLevel
{
    BIOS,   /**< BIOS message */
    INFO,   /**< General information. */
    WARN,   /**< Something suspicious happened. */
    ERROR,  /**< Something bad happened. */
    FATAL,  /**< Something really bad happened and the program will probably terminate. */
    NONE,   /**< Unspecified message. */
};

/**
 *  Write a string to the log file.
 *
 *  @param logType The @ref LogLevel to log with.
 *  @param str     Formatted string to print.
 */
[[gnu::format(printf, 2, 3)]] int log(LogLevel logType, const char* str, ...);

/**
 *  Kill the process due to a fatal error being encountered
 *  and inform the user of the problem (as well as writing to
 *  the error log, log.txt)
 *
 *  @param str Formatted string to print.
 *
 *  @note This function does not return.
 */
[[noreturn]] [[gnu::format(printf, 1, 2)]] void die(const char* str, ...);

// Thanks Serenity :^)
#define log_if(flag, component, fmt, ...)               \
    do                                                  \
    {                                                   \
        if constexpr (flag)                             \
            silent::log(component, fmt, ##__VA_ARGS__); \
    } while (0)

}

using Util::log;
using Util::LogLevel;
