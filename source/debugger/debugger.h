/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include "types.h"
#include "debugger/gdb.h"

namespace PSX
{

class Emulator;
class Debugger
{
public:
    Debugger(Emulator& emu) : m_playstation(emu){}

    /**
     * @brief Resume system execution
     */
    void resume();

    /**
     * @brief Step a single instruction
     */
    void step();

    /**
     * @brief Trigger an immediate break to the debugger
     */
    void trigger_break();

    /**
     * @brief Attach the debugger
     */
    void attach();

private:
    Emulator&               m_playstation;
    std::vector<vaddr_t>    m_breakpoints;
    GDBRemoteServer         m_gdb_server;

};

}
