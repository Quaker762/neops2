/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "debugger/gdb.h"
#include <regex>
#include <iomanip>
#include <iostream>


namespace PSX
{

static constexpr char const* const GDB_PACKET_REGEX_STRING = R"(^\$([^#]*)#([0-9a-zA-Z]{2}))";

bool GDBPacket::is_valid() const
{
    return std::regex_match(m_data.c_str(), std::regex(GDB_PACKET_REGEX_STRING));
}

std::vector<std::string> GDBPacket::split()
{
    std::vector<std::string> components;
    std::regex command_regex(GDB_PACKET_REGEX_STRING);
    std::smatch matcher;

    if (std::regex_match(m_data, matcher, command_regex))
    {
        // We skip the first item in the match list because it contains the full packet
        for (auto i = 1u; i < matcher.size(); i++)
        {
            components.push_back(matcher[i].str());
        }
    }

    return components;
}

std::string GDBPacket::get_checksum() const
{
    unsigned sum = 0;
    for (auto const& character : m_data)
    {
        sum += static_cast<unsigned>(character);
    }

    sum %= 256u;
    std::stringstream stream;
    stream << std::hex << std::setfill('0') << std::setw(2) << sum;

    return stream.str();
}

}
