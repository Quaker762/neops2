/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/system/bus.h"
#include "hw/r3000a/mips/mips.h"
#include "hw/r3000a/mips/mips_cop0.h"
#include "debugger/gdb.h"
#include "types.h"
#include "util/log.h"

#include <iomanip>
#include <iostream>

static constexpr bool LOG_GDB_COMMANDS = false; // Enable me for verbose debugging!

namespace PSX
{

bool GDBRemoteServer::await_connection()
{
    auto start_result = m_server.start(); // Start the server
    if (start_result == false)
    {
        Util::die("Failed to start TCP server!");
    }

    m_server.await_client_connection();

    return true;
}

void GDBRemoteServer::send_ack()
{
    m_server.send_data("+", 1);
}

void GDBRemoteServer::send_packet(GDBPacket const& packet)
{
    std::string message_data;

    message_data += '$';
    message_data += packet.message_data();
    message_data += '#';
    message_data += packet.get_checksum();

    m_server.send_data(message_data.c_str(), static_cast<socketdatalen_t>(message_data.length()));
}

GDBPacket GDBRemoteServer::get_packet()
{
    std::string packet_data;

    while (m_server.data_available())
    {
        packet_data += m_server.block_and_read_character();
    }

    return GDBPacket(packet_data);
}

void GDBRemoteServer::handle_command()
{
    auto packet = get_packet();

    // HACK: Fix this flakiness please! The socket sometimes sucks in the ACK
    // and the next packet at the same time which fucks the whole thing up!
    if (packet.message_data()[0] == '+' && packet.message_data().size() > 1u)
    {
        packet.message_data().erase(0);
    }

    if ((packet.is_ack()) || (packet.is_empty()))
    {
        // Do nothing
    }
    else if (packet.is_nack())
    {
        Util::die("Received NACK from GDB!");
    }
    else if (packet.is_valid())
    {
        if constexpr (LOG_GDB_COMMANDS)
        {
            log(LogLevel::INFO, "Got packet %s from GDB", packet.message_data().c_str());
        }

        // Split the message up into its components
        auto const packet_components = packet.split();

        if (packet_components.empty() == false)
        {
            auto const& data = packet_components.at(0);
            auto command_type = data.at(0);

            switch (command_type)
            {
            case '?':
                send_ack();
                handle_stopped_command();
                break;
            case 'H':
                send_ack();
                handle_set_thread_command(data);
                break;
            case 'c':
                send_ack();
                handle_continue_command(data);
                break;
            case 'g':
                send_ack();
                handle_send_registers();
                break;
            case 'm':
                send_ack();
                handle_read_memory(data.substr());
                break;
            case 'q':
                send_ack();
                handle_query_command(data);
                break;
            case 's':
                send_ack();
                handle_step_command(data);
                break;
            case 'v':
                send_ack();
                handle_v_command(data);
                break;
            default:
                send_packet({""});
                break;
            }
        }
    }
    // Handle the SIGNINT break signal
    else if (packet.message_data() == BREAK_CHARACTER)
    {
        send_packet({"T02"});   // Send the response
        m_should_run = false;
    }
    else
    {
        Util::die("Got unknown packet, %s", packet.message_data().c_str());
    }
}

// ======== Command Implementations ======
void GDBRemoteServer::handle_query_command(std::string const& data)
{
    // Handle the 'qSupported' packet
    if (data.rfind("qSupported", 0) == 0)
    {
        send_packet({"hwbreak+"});
    }
    else if (data.rfind("qTStatus", 0) == 0)
    {
        // We don't support "Trace Experiments"
        send_packet({""});
    }
    else if (data.rfind("qfThreadInfo", 0) == 0)
    {
        // We don't have threads. We're debugging an embedded target, not the software running
        // on it.
        send_packet({""});
    }
    else if (data.rfind("qL", 0) == 0)
    {
        // We _really_ don't have threads, GDB...
        send_packet({""});
    }
    else if (data.rfind("qC", 0) == 0)
    {
        // Please stop...
        send_packet({""});
    }
    else if (data.rfind("qAttached", 0) == 0)
    {
        // Let GDB know that the remote server is attached to an existing process
        send_packet({"1"});
    }
}

void GDBRemoteServer::handle_v_command(std::string const& data)
{
    if (data.rfind("vMustReplyEmpty", 0) == 0)
    {
        // Reply with an empty command
        send_packet({""});
    }
    else
    {
        send_packet({""});
    }
}

void GDBRemoteServer::handle_set_thread_command(std::string const&)
{
    // We don't support "threads" on debug of a remote target. Let's just send
    // back an empty reply
    send_packet({""});
}

void GDBRemoteServer::handle_stopped_command()
{
    // TODO: Actually halt the processor here
    send_packet({"S05"});
}

void GDBRemoteServer::handle_send_registers()
{
    std::stringstream register_stream;

    // Send all GPR values
    for (u8 reg = 0u; reg < MIPS_NUMBER_OF_GPR; reg++)
    {
        u32 value = Bus::the()->cpu().jtag().read_gpr(reg);

        register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(value);
    }

    // Append the status register
    register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(Bus::the()->cpu().jtag().read_cop0_register(MIPS::Coprocessor0::STATUS_REGISTER_INDEX));

    // Read LO and HI
    register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(Bus::the()->cpu().jtag().read_lo());
    register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(Bus::the()->cpu().jtag().read_hi());

    // Read the BadVaddr register
    register_stream << std::hex << std::setfill('0') << std::setw(8) << 0u;

    // Read the CAUSE register
    register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(Bus::the()->cpu().jtag().read_cop0_register(MIPS::Coprocessor0::CAUSE_REGISTER_INDEX));

    // Read the PC register
    register_stream << std::hex << std::setfill('0') << std::setw(8) << __builtin_bswap32(Bus::the()->cpu().jtag().read_pc());

    for (int reg = 38; reg < 73; reg++)
    {
        register_stream << std::hex << std::setfill('0') << std::setw(8) << 0u;
    }

    send_packet({register_stream.str()});
}

void GDBRemoteServer::handle_read_memory(std::string const& data)
{
    // Find the position of the `,`
    auto comma_index = data.rfind(',', std::string::npos);
    auto address = data.substr(1, comma_index-1);
    auto size = data.substr(comma_index+1, data.length());

    auto base_address = std::stoul(address, nullptr, 16);
    auto number_of_bytes = std::stoul(size);

    std::stringstream data_stream;
    for (auto i = base_address; i < base_address + number_of_bytes; i++)
    {
        auto read_value = Bus::the()->cpu().jtag().bus_read<u8>(i);
        data_stream << std::hex << std::setfill('0') << std::setw(2) << static_cast<u32>(read_value);
    }

    send_packet({ data_stream.str() });
}

void GDBRemoteServer::handle_continue_command(std::string const&)
{
    m_should_run = true;
}

void GDBRemoteServer::handle_step_command(std::string const& data [[maybe_unused]])
{
    // If there's not data, let's continue from the current address
    send_packet({"T00"});
    Bus::the()->cpu().cycle();
}

}
