/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#ifdef __WIN32__
#include <winsock2.h>
typedef SOCKET sock_t;
typedef int socketlen_t;
typedef int socketsize_t;
typedef char* socketdataptr_t;
typedef char const* const_socketdataptr_t;
typedef int socketdatalen_t;
#else
#include <sys/socket.h>
typedef int sock_t;
typedef socketlen_t socklen_t;
typedef ssize_t socketsize_t;
typedef void* socketdataptr_t;
typedef void const* const_socketdataptr_t;
typedef size_t socketdatalen_t;
#endif

namespace PSX
{

class Socket
{
public:
#ifdef __WIN32__
    static constexpr SOCKET BAD_SOCKET = INVALID_SOCKET;
#else
    static constexpr int BAD_SOCKET = -1;
#endif

public:
    Socket();
    Socket(sock_t raw_sock);
    ~Socket(){}

    void try_create();

    Socket accept(sockaddr* addr, socketlen_t* addrlen);
    int bind(sockaddr const* name, socketlen_t namelen);
    void close();
    int listen(int backLog);

    // Transmission functions
    socketsize_t recv(socketdataptr_t data, socketdatalen_t length, int flags);
    socketsize_t send(const_socketdataptr_t const data, socketdatalen_t length, int flags);

    sock_t raw_sock() const { return m_raw_socket; }
    bool is_bad_socket() const;
    bool data_available() const;

private:
    sock_t m_raw_socket;
};

}
