/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "util/log.h"
#include "debugger/net/server.h"

#ifdef __WIN32__
#include <winsock2.h>
#endif

namespace PSX
{

TCPServer::TCPServer()
{
#ifdef __WIN32__
    winsock2_init();
#endif
}

#ifdef __WIN32__
bool TCPServer::winsock2_init()
{
    static WSADATA wsa_data;
    auto wsa_retval = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (wsa_retval != 0)
    {
        Util::die("Failed to start WSA! Reason %d", wsa_retval);
    }

    return true;
}

void TCPServer::winsock2_shutdown()
{
    // Do nothing for now...
}
#endif

bool TCPServer::start()
{
    static sockaddr_in server_address;

    server_address.sin_family       = AF_INET;
    server_address.sin_addr.s_addr  = INADDR_ANY;
    server_address.sin_port         = htons(7373);

    m_server_sock.try_create();

    auto bind_result = m_server_sock.bind(reinterpret_cast<sockaddr*>(&server_address), sizeof(server_address));
    if (bind_result < 0)
        return false;

    return true;
}

void TCPServer::await_client_connection()
{
    static sockaddr_in server_address;
    static socketsize_t server_address_size = sizeof(server_address);

    server_address.sin_family       = AF_INET;
    server_address.sin_addr.s_addr  = INADDR_ANY;
    server_address.sin_port         = htons(7373);

    auto listen_result = m_server_sock.listen(1);
    if (listen_result < 0)
    {
        Util::die("Failed to listen on server sock. Reaon: %d", listen_result);
    }

    m_client_sock = m_server_sock.accept(reinterpret_cast<sockaddr*>(&server_address), &server_address_size);

    // TODO: Implement operator==()!
    if (m_client_sock.is_bad_socket())
    {
        Util::die("Failed to accept client socket!");
    }

}

socketsize_t TCPServer::block_and_recv(socketdataptr_t data, socketdatalen_t length)
{
    return m_client_sock.recv(data, length, 0);
}

char TCPServer::block_and_read_character()
{
    char read_data;

    auto read_result = m_client_sock.recv(&read_data, 1, 0);
    if (read_result < 0)
    {
        Util::die("Failed to read character from socket");
    }

    return read_data;
}

void TCPServer::send_data(const_socketdataptr_t data, socketdatalen_t length)
{
    m_client_sock.send(data, length, 0);
}

bool TCPServer::data_available() const
{
    return m_client_sock.data_available();
}

}
