/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "util/log.h"
#include "debugger/net/sock.h"

namespace PSX
{

Socket::Socket()
{

}

Socket::Socket(sock_t raw_sock)
    : m_raw_socket(raw_sock)
{
}

Socket Socket::accept(sockaddr* addr, socketlen_t* addrlen)
{
    Socket new_socket;

    new_socket.m_raw_socket = ::accept(m_raw_socket, addr, addrlen);

    if (new_socket.m_raw_socket == BAD_SOCKET)
    {
        // Can't use ::ERROR here because of winsock2.h...
        // In file included from C:/msys64/ucrt64/include/windows.h:71,
        // from C:/msys64/ucrt64/include/winsock2.h:23,
        // from E:/Users/Jesse/Documents/Programming/Emulators/neops/source/debugger/net/sock.h:21,
        // from E:\Users\Jesse\Documents\Programming\Emulators\neops\source\debugger\net\sock.cpp:19:
        // E:\Users\Jesse\Documents\Programming\Emulators\neops\source\debugger\net\sock.cpp: In member function 'void PSX::Socket::accept(const PSX::Socket&, sockaddr*, socketlen_t*)':
        // E:\Users\Jesse\Documents\Programming\Emulators\neops\source\debugger\net\sock.cpp:39:35: error: expected unqualified-id before numeric constant
        //39 |         log(LogLevel::ERROR, "Failed to accept socket!");
        //
        // Absolute fucking clownery
        log(LogLevel::WARN, "Failed to accept socket!");
    }

    return { new_socket.m_raw_socket };
}

int Socket::bind(sockaddr const* name, socketlen_t namelen)
{
    return ::bind(m_raw_socket, name, namelen);
}

void Socket::try_create()
{
    m_raw_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

void Socket::close()
{
#ifdef __WIN32__
    ::closesocket(m_raw_socket);
#else
    ::close(m_raw_socket);
#endif
}

int Socket::listen(int backLog)
{
    return ::listen(m_raw_socket, backLog);
}

// Transmission functions
socketsize_t Socket::recv(socketdataptr_t data, socketdatalen_t length, int flags)
{
    return ::recv(m_raw_socket, data, length, flags);
}

socketsize_t Socket::send(const_socketdataptr_t const data, socketdatalen_t length, int flags)
{
    return ::send(m_raw_socket, data, length, flags);
}

bool Socket::is_bad_socket() const
{
#ifdef __WIN32__
    return (m_raw_socket == INVALID_SOCKET);
#else
    return (m_raw_socket < 0);
#endif
}

bool Socket::data_available() const
{
#ifdef __WIN32__
    u_long count;
    auto result = ioctlsocket(m_raw_socket, FIONREAD, &count);
    if (result != 0)
    {
        return false;
    }
#else
    int count;
    auto result = ioctl(m_raw_socket, FIONREAD, &count);
    if (result != 0)
    {
        return false;
    }
#endif

    return count > 0;

}

}
