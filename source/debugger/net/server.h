/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstddef>
#include "debugger/net/sock.h"

namespace PSX
{

class TCPServer
{
public:
    TCPServer();
    ~TCPServer() = default;

#ifdef __WIN32__
    bool winsock2_init();
    void winsock2_shutdown();
#endif

    bool start();
    void await_client_connection();
    socketsize_t block_and_recv(socketdataptr_t data, socketdatalen_t length);
    char block_and_read_character();
    void send_data(const_socketdataptr_t data, socketdatalen_t length);
    bool data_available() const;

private:
#ifdef __WIN32__
    bool m_wsa_initialized { false };
#endif

    Socket m_server_sock;
    Socket m_client_sock;
};

}
