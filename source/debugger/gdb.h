/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <functional>
#include <map>
#include <string>
#include <vector>
#include "debugger/net/server.h"

namespace PSX
{

class GDBPacket
{
public:
    GDBPacket() = default;
    GDBPacket(std::string const& data) : m_data(data) {}

    bool is_valid() const;
    bool is_ack() { return m_data == "+"; }
    bool is_nack() { return m_data == "-"; }
    bool is_empty() { return m_data.empty(); }

    std::vector<std::string> split();
    std::string const& message_data() const { return m_data; }
    std::string& message_data() { return m_data; }
    std::string get_checksum() const;

    void append(std::string const& data) { m_data += data; }
    void append(char data) { m_data += data; }

private:
    std::string m_data;
};

class GDBRemoteServer
{
    static constexpr char const* const BREAK_CHARACTER = "\x03";

public:
    GDBRemoteServer() = default;

    bool await_connection();
    bool is_attached() const { return m_is_attached; }
    bool should_run() const { return m_should_run; }
    void handle_command();

private:
    GDBPacket get_packet();

    void send_ack();
    void send_packet(GDBPacket const& packet);

    void handle_v_command(std::string const&);
    void handle_query_command(std::string const&);
    void handle_set_thread_command(std::string const&);
    void handle_stopped_command();
    void handle_send_registers();
    void handle_read_memory(std::string const&);
    void handle_continue_command(std::string const&);
    void handle_step_command(std::string const&);

private:
    TCPServer m_server;
    bool m_is_attached { false };
    bool m_should_run { false };
};

}
