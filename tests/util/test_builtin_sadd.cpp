/*
 * This file is part of Neo64
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "types.h"

TEST_CASE("Test that __builtin_sadd_overflow gives correct values based on register values", "[builtins]")
{
    u32 value1 = static_cast<u32>(INT32_MAX);
    u32 value2 = 1u;
    i32 result;

    auto did_overflow = __builtin_sadd_overflow(static_cast<i32>(value1), static_cast<i32>(value2), &result);

    REQUIRE(did_overflow == true);
    REQUIRE(static_cast<i32>(result) == INT32_MIN); // This wraps back around to INT32_MIN (0x7FFFFFFF -> 0x80000000 in two's complement)
}
