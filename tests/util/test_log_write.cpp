/*
 * This file is part of Neo64
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include <cstdio>
#include <cstring>
#include "util/log.h"

TEST_CASE("Basic log string write", "[log]")
{
    std::printf("This is a visual test. Each message should have the correct colour!\r\n");

    static constexpr const char* TEST_MESSAGE = "HELLO, WORLD!\0";

    REQUIRE(log(LogLevel::NONE, "%s", TEST_MESSAGE) == strlen(TEST_MESSAGE));
    REQUIRE(log(LogLevel::INFO, "%s", TEST_MESSAGE) == strlen(TEST_MESSAGE));
    REQUIRE(log(LogLevel::WARN, "%s", TEST_MESSAGE) == strlen(TEST_MESSAGE));
    REQUIRE(log(LogLevel::ERROR, "%s", TEST_MESSAGE) == strlen(TEST_MESSAGE));
    REQUIRE(log(LogLevel::FATAL, "%s", TEST_MESSAGE) == strlen(TEST_MESSAGE));
}
