/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

static constexpr paddr_t    BASE_ADDRESS    = 0x00008020u;
static constexpr u8         WRITE_VALUE_LB  = 0x85u;
static constexpr u32        WRITE_VALUE_LH  = 0xAA55u;
static constexpr u32        WRITE_VALUE_LW  = 0xAA55AA55u;

TEST_CASE("Test LB instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16);
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, BASE_ADDRESS);

    // Write the byte to memory
    auto write_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    PSX::Bus::the()->write(static_cast<paddr_t>(write_address), WRITE_VALUE_LB);

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LB, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));
    PSX::Bus::the()->write(4u, create_i_instruction(0, 0, 0, 0));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == static_cast<u32>(static_cast<i8>(WRITE_VALUE_LB)));
}

TEST_CASE("Test LH instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16) & ~3;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, BASE_ADDRESS);

    // Write the byte to memory
    auto write_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    PSX::Bus::the()->write<u32>(static_cast<paddr_t>(write_address), WRITE_VALUE_LH);

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LH, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));
    PSX::Bus::the()->write(4u, create_i_instruction(0, 0, 0, 0));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == static_cast<u32>(static_cast<i16>(WRITE_VALUE_LH)));
}


TEST_CASE("Test LW instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16) & ~3;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, BASE_ADDRESS);

    // Write the byte to memory
    auto write_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    PSX::Bus::the()->write<u32>(static_cast<paddr_t>(write_address), WRITE_VALUE_LW);

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LW, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));
    PSX::Bus::the()->write(4u, create_i_instruction(0, 0, 0, 0));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == WRITE_VALUE_LW);
}

TEST_CASE("Test SB instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16);
    u8 write_value      = static_cast<u8>(random_number(0, 255));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(1u, BASE_ADDRESS);
    cpu().jtag().write_gpr(2u, write_value);

    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SB, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));

    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    auto read_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    REQUIRE(PSX::Bus::the()->read<u8>(static_cast<paddr_t>(read_address)) == write_value);
}

TEST_CASE("Test SH instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16) & ~1;
    u16 write_value      = static_cast<u8>(random_number(0, 65535));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(1u, BASE_ADDRESS);
    cpu().jtag().write_gpr(2u, write_value);

    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SH, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));

    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    auto read_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    REQUIRE(PSX::Bus::the()->read<u16>(static_cast<paddr_t>(read_address)) == write_value);
}

TEST_CASE("Test SW instruction", "[cpu_instructions]")
{
    TestInit init;

    auto address_offset = random_number(-16, 16) & ~3;
    u32 write_value     = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(1u, BASE_ADDRESS);
    cpu().jtag().write_gpr(2u, write_value);

    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SW, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));

    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    auto read_address = static_cast<i32>(BASE_ADDRESS) + static_cast<i16>(address_offset);
    REQUIRE(PSX::Bus::the()->read<u32>(static_cast<paddr_t>(read_address)) == write_value);
}

TEST_CASE("Test LUI instruction", "[cpu_instructions]")
{
    TestInit init;
    u8 rt = 2u;

    // Register setup
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LUI, 0, rt, 0xAA55u));
    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == 0xAA550000u);
}

TEST_CASE("Test LWL instruction", "[cpu_instructions]")
{
    static constexpr u32 RT_VALUE = 0xEEEEEEEEu;

    for (auto alignment_offset = 0; alignment_offset <= 3; alignment_offset++)
    {
        TestInit init;

        auto address_offset = alignment_offset;
        u8 rs = 1u;
        u8 rt = 2u;

        // Register setup
        cpu().jtag().write_gpr(rs, BASE_ADDRESS);
        cpu().jtag().write_gpr(rt, RT_VALUE);    // Write a dummy value to register rt

        // Write the byte to memory
        auto write_address = static_cast<i32>(BASE_ADDRESS);
        PSX::Bus::the()->write<u32>(static_cast<paddr_t>(write_address & ~3), WRITE_VALUE_LW);

        // Write instructions to RAM
        PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LWL, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));
        PSX::Bus::the()->write(4u, create_i_instruction(0, 0, 0, 0));

        // Execute
        cpu().jtag().force_pc(0u);
        cpu().jtag().clock();
        cpu().jtag().clock();

        u32 expected_value = 0u;
        switch (address_offset)
        {
        case 0u:
            expected_value = (RT_VALUE & 0x00FFFFFFu) | ((WRITE_VALUE_LW & 0x000000FFu) << 24u);
            break;
        case 1u:
            expected_value = (RT_VALUE & 0x0000FFFFu) | ((WRITE_VALUE_LW & 0x0000FFFFu) << 16u);
            break;
        case 2u:
            expected_value = (RT_VALUE & 0x00000FFu) | ((WRITE_VALUE_LW & 0x00FFFFFFu) << 8u);
            break;
        case 3u:
            expected_value = WRITE_VALUE_LW;
            break;
        }

        REQUIRE(cpu().jtag().read_gpr(rt) == expected_value);
    }
}

TEST_CASE("Test LWR instruction", "[cpu_instructions]")
{
    static constexpr u32 RT_VALUE = 0xEEEEEEEEu;

    for (auto alignment_offset = 0; alignment_offset <= 3; alignment_offset++)
    {
        TestInit init;

        auto address_offset = alignment_offset;
        u8 rs = 1u;
        u8 rt = 2u;

        // Register setup
        cpu().jtag().write_gpr(rs, BASE_ADDRESS);
        cpu().jtag().write_gpr(rt, RT_VALUE);    // Write a dummy value to register rt

        // Write the byte to memory
        auto write_address = static_cast<i32>(BASE_ADDRESS);
        PSX::Bus::the()->write<u32>(static_cast<paddr_t>(write_address), WRITE_VALUE_LW);

        // Write instructions to RAM
        PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_LWR, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));
        PSX::Bus::the()->write(4u, create_i_instruction(0, 0, 0, 0));

        // Execute
        cpu().jtag().force_pc(0u);
        cpu().jtag().clock();
        cpu().jtag().clock();

        u32 expected_value = 0u;
        switch (address_offset)
        {
        case 0u:
            expected_value = WRITE_VALUE_LW;
            break;
        case 1u:
            expected_value = (RT_VALUE & 0xFF000000u) | ((WRITE_VALUE_LW & 0xFFFFFF00u) >> 8u);
            break;
        case 2u:
            expected_value = (RT_VALUE & 0xFFFF0000u) | ((WRITE_VALUE_LW & 0xFFFF0000u) >> 16u);
            break;
        case 3u:
            expected_value = (RT_VALUE & 0xFFFFFF00u) | ((WRITE_VALUE_LW & 0xFF000000u) >> 24u);
            break;
        }

        REQUIRE(cpu().jtag().read_gpr(rt) == expected_value);
    }
}

TEST_CASE("Test SWL instruction", "[cpu_instructions]")
{
    static constexpr u32 RT_VALUE = 0xEEEEEEEEu;

    for (auto alignment_offset = 0; alignment_offset <= 3; alignment_offset++)
    {
        TestInit init;

        auto address_offset = alignment_offset;
        u8 rs = 1u;
        u8 rt = 2u;

        // Register setup
        cpu().jtag().write_gpr(rs, BASE_ADDRESS);
        cpu().jtag().write_gpr(rt, RT_VALUE);    // Write a dummy value to register rt

        // Write the byte to memory
        auto write_address = static_cast<i32>(BASE_ADDRESS);
        PSX::Bus::the()->write<u32>(static_cast<paddr_t>(BASE_ADDRESS), WRITE_VALUE_LW);

        // Write instructions to RAM
        PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SWL, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));

        // Execute
        cpu().jtag().force_pc(0u);
        cpu().jtag().clock();

        u32 expected_value = 0u;
        switch (((write_address + address_offset) & 0x3))
        {
        case 0:
            expected_value = ((RT_VALUE & 0xff000000) >> 24u) | (WRITE_VALUE_LW & 0xffffff00);
            break;
        case 1:
            expected_value = ((RT_VALUE & 0xffff0000) >> 16u) | (WRITE_VALUE_LW & 0xffff0000);
            break;
        case 2:
            expected_value = ((RT_VALUE & 0xffffff00) >> 8u) | (WRITE_VALUE_LW & 0xff000000);
            break;
        case 3:
            expected_value = RT_VALUE;
            break;
        }

        REQUIRE(PSX::Bus::the()->read<u32>(static_cast<paddr_t>(write_address)) == expected_value);
    }
}

TEST_CASE("Test SWR instruction", "[cpu_instructions]")
{
    static constexpr u32 RT_VALUE = 0xEEEEEEEEu;

    for (auto alignment_offset = 0; alignment_offset <= 3; alignment_offset++)
    {
        TestInit init;

        auto address_offset = alignment_offset;
        u8 rs = 1u;
        u8 rt = 2u;

        // Register setup
        cpu().jtag().write_gpr(rs, BASE_ADDRESS);
        cpu().jtag().write_gpr(rt, RT_VALUE);    // Write a dummy value to register rt

        // Write the byte to memory
        auto write_address = static_cast<i32>(BASE_ADDRESS);
        PSX::Bus::the()->write<u32>(static_cast<paddr_t>(BASE_ADDRESS), WRITE_VALUE_LW);

        // Write instructions to RAM
        PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SWR, rs, rt, static_cast<u16>(static_cast<i16>(address_offset))));

        // Execute
        cpu().jtag().force_pc(0u);
        cpu().jtag().clock();

        u32 expected_value = 0u;
        switch (((write_address + address_offset) & 0x3))
        {
        case 0:
            expected_value = RT_VALUE;
            break;
        case 1:
            expected_value = ((RT_VALUE & 0x00ffffff) << 8u) | (WRITE_VALUE_LW & 0x000000ff);
            break;
        case 2:
            expected_value = ((RT_VALUE & 0x0000ffff) << 16u) | (WRITE_VALUE_LW & 0x0000ffff);
            break;
        case 3:
            expected_value = ((RT_VALUE & 0x000000ff) << 24u) | (WRITE_VALUE_LW & 0x00ffffff);
            break;
        }

        REQUIRE(PSX::Bus::the()->read<u32>(static_cast<paddr_t>(write_address)) == expected_value);
    }
}

