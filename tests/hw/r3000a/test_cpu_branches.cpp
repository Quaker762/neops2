/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

TEST_CASE("Test BEQ instruction", "[cpu_instructions]")
{
    static constexpr u16 BRANCH_OFFSET = 4u;
    static constexpr u16 BRANCH_TARGET = (BRANCH_OFFSET << 2u);

    TestInit init;
    u8 rs = 0u;
    u8 rt = 1u;

    // Register setup
    cpu().jtag().write_gpr(rt, 0u);

    // Write the instruction to RAM
    PSX::Bus::the()->write(0, create_i_instruction(PSX::MIPS::OPCODE_BEQ, rs, rt, BRANCH_OFFSET));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    // Check to see if the delay slot instruction is executed
    REQUIRE(PSX::Bus::the()->cpu().jtag().read_pc() == 4u);

    cpu().jtag().clock();
    REQUIRE(PSX::Bus::the()->cpu().jtag().read_pc() == (BRANCH_TARGET + 4u));
}

TEST_CASE("Test BNE instruction", "[cpu_instructions]")
{
    static constexpr u16 BRANCH_OFFSET = 4u;
    static constexpr u16 BRANCH_TARGET = (BRANCH_OFFSET << 2u);

    TestInit init;
    u8 rs = 0u;
    u8 rt = 1u;

    // Register setup
    cpu().jtag().write_gpr(rt, 1u);

    // Write the instruction to RAM
    PSX::Bus::the()->write(0, create_i_instruction(PSX::MIPS::OPCODE_BNE, rs, rt, BRANCH_OFFSET));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    // Check to see if the delay slot instruction is executed
    REQUIRE(PSX::Bus::the()->cpu().jtag().read_pc() == 4u);

    cpu().jtag().clock();
    REQUIRE(PSX::Bus::the()->cpu().jtag().read_pc() == (BRANCH_TARGET + 4u));
}
