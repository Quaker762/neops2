/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/r3000.h"
#include "types.h"

u32 create_i_instruction(u32 op, u32 rs, u32 rt, u32 immediate);
u32 create_j_instruction(u32 op, u32 target);
u32 create_r_instruction(u32 op, u32 rs, u32 rt, u32 rd, u32 shamt, u32 funct);
int random_number(int a, int b);
PSX::R3000& cpu();

struct TestInit
{
    TestInit()
    {
        clear_registers();
        wipe_ram();
    }

    ~TestInit()
    {

    }

private:
    void clear_registers();
    void wipe_ram();
};
