/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

TEST_CASE("Test ANDI instruction", "[cpu_instructions]")
{
    u32 value = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_ANDI, rs, rt, 0xFFFF));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == (value & 0x0000FFFFu));
}

TEST_CASE("Test AND instruction", "[cpu_instructions]")
{
    u32 value   = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u32 value2  = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs       = 1u;
    u8 rt       = 2u;
    u8 rd       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(value2));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_AND));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value & value2));
}

TEST_CASE("Test ORI instruction", "[cpu_instructions]")
{
    u32 value = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_ORI, rs, rt, 0x1234));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == (value | 0x00001234u));
}

TEST_CASE("Test NOR instruction", "[cpu_instructions]")
{
    u32 value   = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u32 value2  = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs       = 1u;
    u8 rt       = 2u;
    u8 rd       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(value2));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_NOR));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == ~(value | value2));
}


TEST_CASE("Test OR instruction", "[cpu_instructions]")
{
    u32 value   = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u32 value2  = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs       = 1u;
    u8 rt       = 2u;
    u8 rd       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(value2));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_OR));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value | value2));
}

TEST_CASE("Test SLL instruction", "[cpu_instructions]")
{
    u32 value   = 0x05050505u;
    u32 shamt   = 1u;
    u8 rt       = 1u;
    u8 rd       = 2u;

    // Register setup
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, rt, rd, shamt, PSX::MIPS::SPECIAL_SLL));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value << shamt));
}

TEST_CASE("Test SLLV instruction", "[cpu_instructions]")
{
    u32 value   = 0x05050505u;
    u32 shamt   = 7u;
    u8 rs       = 3u;
    u8 rt       = 1u;
    u8 rd       = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, shamt);
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0u, PSX::MIPS::SPECIAL_SLLV));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value << shamt));
}

TEST_CASE("Test SRA instruction", "[cpu_instructions]")
{
    i32 value   = static_cast<i32>(0xFFFFFFFC);
    u32 shamt   = 1u;
    u8 rt       = 1u;
    u8 rd       = 2u;

    // Register setup
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, rt, rd, shamt, PSX::MIPS::SPECIAL_SRA));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == static_cast<u32>((value >> shamt)));
}

TEST_CASE("Test SRAV instruction", "[cpu_instructions]")
{
    i32 value   = static_cast<i32>(0xFFFFFFFC);
    u32 shamt   = 6u;
    u8 rt       = 1u;
    u8 rd       = 2u;
    u8 rs       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));
    cpu().jtag().write_gpr(rs, shamt);

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0u, PSX::MIPS::SPECIAL_SRAV));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == static_cast<u32>((value >> shamt)));
}

TEST_CASE("Test SRL instruction", "[cpu_instructions]")
{
    u32 value   = 0x05050505u;
    u32 shamt   = 1u;
    u8 rt       = 1u;
    u8 rd       = 2u;

    // Register setup
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, rt, rd, shamt, PSX::MIPS::SPECIAL_SRL));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value >> shamt));
}

TEST_CASE("Test SRLV instruction", "[cpu_instructions]")
{
    u32 value   = 0x05050505u;
    u32 shamt   = 1u;
    u8 rt       = 1u;
    u8 rd       = 2u;
    u8 rs       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rt, static_cast<u32>(value));
    cpu().jtag().write_gpr(rs, shamt);

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_SRLV));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value >> shamt));
}

TEST_CASE("Test XOR instruction", "[cpu_instructions]")
{
    u32 value   = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u32 value2  = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs       = 1u;
    u8 rt       = 2u;
    u8 rd       = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(value2));

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_XOR));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == (value ^ value2));
}

TEST_CASE("Test XORI instruction", "[cpu_instructions]")
{
    u32 value = static_cast<u32>(random_number(0, static_cast<int>(UINT32_MAX)));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(value));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_XORI, rs, rt, 0x1234));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == (value ^ 0x00001234u));
}
