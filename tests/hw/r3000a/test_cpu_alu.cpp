/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

TEST_CASE("Test ADDIU instruction", "[cpu_instructions]")
{
    TestInit init;

    u16 value = static_cast<u16>(random_number(INT16_MIN, INT16_MAX));
    u8 rs = 0u;
    u8 rt = 1u;

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_i_instruction(PSX::MIPS::OPCODE_ADDIU, rs, rt, value));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    auto register_value = cpu().jtag().read_gpr(rt);
    REQUIRE(static_cast<i32>(register_value) == (0 + static_cast<i16>(value)));
}

TEST_CASE("Test ADDI instruction. Doesn't test trap on overflow", "[cpu_instructions]")
{
    TestInit init;

    u16 value = static_cast<u16>(random_number(INT16_MIN, INT16_MAX));
    u8 rs = 0u;
    u8 rt = 1u;

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_i_instruction(PSX::MIPS::OPCODE_ADDIU, rs, rt, value));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    auto register_value = cpu().jtag().read_gpr(rt);
    REQUIRE(static_cast<i32>(register_value) == (0 + static_cast<i16>(value)));
}

TEST_CASE("Test ADDU instruction", "[cpu_instructions]")
{
    u32 rs_value = static_cast<u32>(random_number(INT32_MAX, INT32_MAX));
    u32 rt_value = static_cast<u32>(random_number(INT32_MAX, INT32_MAX));
    u8 rs = 1u;
    u8 rt = 2u;
    u8 rd = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0u, PSX::MIPS::SPECIAL_ADDU));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    u32 register_value = cpu().jtag().read_gpr(rd);
    REQUIRE(register_value == (rs_value + rt_value));
}

TEST_CASE("Test DIV instruction", "[cpu_instructions]")
{
    u32 rs_value = 5u;
    u32 rt_value = 2u;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_DIV));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_hi() == (5 % 2));
    REQUIRE(cpu().jtag().read_lo() == (5 / 2));
}

TEST_CASE("Test DIVU instruction", "[cpu_instructions]")
{
    u32 rs_value = 64u;
    u32 rt_value = 3u;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_DIVU));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_hi() == (64 % 3));
    REQUIRE(cpu().jtag().read_lo() == (64 / 3));
}

TEST_CASE("Test MULT instruction", "[cpu_instructions]")
{
    // Negative multiplication
    i64 rs_value = -1;
    i64 rt_value = 16;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(rs_value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(rt_value));

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_MULT));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    u64 result = static_cast<u64>(rs_value * rt_value);
    REQUIRE(cpu().jtag().read_hi() == ((static_cast<u64>(result) >> 32u) & 0xFFFFFFFFull));
    REQUIRE(cpu().jtag().read_lo() == (result & 0xFFFFFFFFu));

    // Double negative mult
    rs_value = -1;
    rt_value = -16;
    rs = 1u;
    rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(rs_value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(rt_value));

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_MULT));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    result = static_cast<u64>(rs_value * rt_value);
    REQUIRE(cpu().jtag().read_hi() == ((static_cast<u64>(result) >> 32u) & 0xFFFFFFFFull));
    REQUIRE(cpu().jtag().read_lo() == (result & 0xFFFFFFFFu));

    // Positive mult
    rs_value = 16;
    rt_value = 16;
    rs = 1u;
    rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(rs_value));
    cpu().jtag().write_gpr(rt, static_cast<u32>(rt_value));

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_MULT));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    result = static_cast<u64>(rs_value * rt_value);
    REQUIRE(cpu().jtag().read_hi() == ((static_cast<u64>(result) >> 32u) & 0xFFFFFFFFull));
    REQUIRE(cpu().jtag().read_lo() == (result & 0xFFFFFFFFu));
}

TEST_CASE("Test MULTU instruction", "[cpu_instructions]")
{
    u32 rs_value = static_cast<u32>(random_number(0, static_cast<i32>(UINT32_MAX)));
    u32 rt_value = static_cast<u32>(random_number(0, static_cast<i32>(UINT32_MAX)));
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_MULTU));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    u64 result = rs_value * rt_value;
    REQUIRE(cpu().jtag().read_hi() == ((result >> 32u) & 0xFFFFFFFFull));
    REQUIRE(cpu().jtag().read_lo() == (result & 0xFFFFFFFFu));



    // Explicitly large multiple
    rs_value = 0xA0000000u;
    rt_value = 0xB0u;
    rs = 1u;
    rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, 0u, 0u, PSX::MIPS::SPECIAL_MULTU));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    result = rs_value * rt_value;
    REQUIRE(cpu().jtag().read_hi() == ((result >> 32u) & 0xFFFFFFFFu));
    REQUIRE(cpu().jtag().read_lo() == (result & 0xFFFFFFFFu));
}

TEST_CASE("Test SUBU instruction", "[cpu_instructions]")
{
    u32 rs_value = 0xAABBCCDD;
    u32 rt_value = 0xEEAA5522;
    u8 rs = 1u;
    u8 rt = 2u;
    u8 rd = 5u;

    // Register setup
    cpu().jtag().write_gpr(rs, rs_value);
    cpu().jtag().write_gpr(rt, rt_value);

    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0u, PSX::MIPS::SPECIAL_SUBU));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    auto expected_result = rs_value - rt_value;
    REQUIRE(cpu().jtag().read_gpr(rd) == expected_result);
}
