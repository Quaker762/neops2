/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

static constexpr u32 NOP = 0x0000000u;

TEST_CASE("Test SLTU instruction", "[cpu_instructions]")
{
    TestInit init;
    Instruction instruction1 { .m_word = 0u};

    // Set-up instruction
    instruction1.r_type.op          = PSX::MIPS::OPCODE_SPECIAL;
    instruction1.r_type.rs          = 0u;
    instruction1.r_type.rt          = 1u;
    instruction1.r_type.rd          = 2u;
    instruction1.r_type.funct       = PSX::MIPS::SPECIAL_SLTU;

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, instruction1.m_word);

    // Register setup
    PSX::Bus::the()->cpu().jtag().write_gpr(1u, 0xCAFEBABEu);

    // Force the Program Counter to our written instruction and execute it
    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    REQUIRE(PSX::Bus::the()->cpu().jtag().read_gpr(2u) == 1u);

    // Set-up instruction
    instruction1.r_type.op          = PSX::MIPS::OPCODE_SPECIAL;
    instruction1.r_type.rs          = 1u;
    instruction1.r_type.rt          = 0u;
    instruction1.r_type.rd          = 2u;
    instruction1.r_type.funct       = PSX::MIPS::SPECIAL_SLTU;

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, instruction1.m_word);

    // Register setup
    PSX::Bus::the()->cpu().jtag().write_gpr(1u, 0xCAFEBABEu);

    // Force the Program Counter to our written instruction and execute it
    PSX::Bus::the()->cpu().jtag().force_pc(0u);
    PSX::Bus::the()->cpu().jtag().clock();

    REQUIRE(PSX::Bus::the()->cpu().jtag().read_gpr(2u) == 0u);
}

TEST_CASE("Test SLTIU instruction", "[cpu_instructions]")
{
    u16 immediate = 0xFFFCu;
    u8 rs = 1u;
    u8 rt = 2u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(0xFFFFFFFBu));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SLTIU, rs, rt, immediate));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == 1u);

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(0xFFFFFFFDu));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_i_instruction(PSX::MIPS::OPCODE_SLTIU, rs, rt, immediate));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rt) == 0u);
}

TEST_CASE("Test SLT instruction", "[cpu_instructions]")
{
    u8 rs = 1u;
    u8 rt = 2u;
    u8 rd = 3u;

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(-4));
    cpu().jtag().write_gpr(rt, static_cast<u32>(-1));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_SLT));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == 1u);

    // Register setup
    cpu().jtag().write_gpr(rs, static_cast<u32>(-1));
    cpu().jtag().write_gpr(rt, static_cast<u32>(-4));

    // Write the byte to memory

    // Write instructions to RAM
    PSX::Bus::the()->write(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, rs, rt, rd, 0, PSX::MIPS::SPECIAL_SLT));

    // Execute
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == 0u);
}

