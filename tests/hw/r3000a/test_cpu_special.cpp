/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips_opcodes.h"
#include "hw/system/bus.h"
#include "util.h"

TEST_CASE("Test MFHI instruction", "[cpu_instructions]")
{
    static constexpr u32 HI_VALUE   = 0xAA55AA55u;
    u8 rd = 2u;

    // Register setup
    cpu().jtag().write_hi(HI_VALUE);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, 0u, rd, 0u, PSX::MIPS::SPECIAL_MFHI));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == HI_VALUE);
}

TEST_CASE("Test MFLO instruction", "[cpu_instructions]")
{
    static constexpr u32 LO_VALUE   = 0xAA55AA55u;
    u8 rd = 2u;

    // Register setup
    cpu().jtag().write_lo(LO_VALUE);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(0u, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, 0u, rd, 0u, PSX::MIPS::SPECIAL_MFLO));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(0u);
    cpu().jtag().clock();

    REQUIRE(cpu().jtag().read_gpr(rd) == LO_VALUE);
}

TEST_CASE("Test SYSCALL jump to vector", "[cpu_instructions]")
{
    static constexpr u32        INITIAL_STATUS_REGISTER = 0x00400029u;
    static constexpr u32        SYSCALL_STATUS_REGISTER = 0x00400024u;
    static constexpr vaddr_t    SYSCALL_ADDRESS         = 8u;
    static constexpr u32        SYSCALL_CAUSE           = 8u;

    // Register setup
    cpu().jtag().write_cop0_register(PSX::MIPS::Coprocessor0::STATUS_REGISTER_INDEX, INITIAL_STATUS_REGISTER);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(SYSCALL_ADDRESS, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, 0u, 0u, 0u, PSX::MIPS::SPECIAL_SYSCALL));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(SYSCALL_ADDRESS);
    cpu().jtag().clock();

    // We should now immediately be at the vector
    REQUIRE(cpu().jtag().read_pc() == MIPS_EXCEPTION_ADDRESS_GENERAL_BEV);
    REQUIRE(cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::STATUS_REGISTER_INDEX) == SYSCALL_STATUS_REGISTER);
    REQUIRE(cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::EPC_REGISTER_INDEX) == SYSCALL_ADDRESS);

    u32 ExcCode = ((cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::CAUSE_REGISTER_INDEX) >> 2u) & 0x1Fu);
    REQUIRE(ExcCode == SYSCALL_CAUSE);
}

TEST_CASE("Test SYSCALL jump to vector, Branch Delay", "[cpu_instructions]")
{
    static constexpr u32        INITIAL_STATUS_REGISTER = 0x00400029u;
    static constexpr u32        SYSCALL_STATUS_REGISTER = 0x00400024u;
    static constexpr vaddr_t    SYSCALL_ADDRESS         = 8u;
    static constexpr u32        SYSCALL_CAUSE           = 8u;
    static constexpr u16        BRANCH_OFFSET           = 0x64u;

    // Register setup
    u8 rs = 0u;
    u8 rt = 1u;

    // Register setup
    cpu().jtag().write_gpr(rt, 0u);

    // Write the instruction to RAM
    PSX::Bus::the()->write(0, create_i_instruction(PSX::MIPS::OPCODE_BEQ, rs, rt, BRANCH_OFFSET));
    cpu().jtag().write_cop0_register(PSX::MIPS::Coprocessor0::STATUS_REGISTER_INDEX, INITIAL_STATUS_REGISTER);

    // Write the instruction to RAM
    PSX::Bus::the()->write<u32>(SYSCALL_ADDRESS - 4u, create_i_instruction(PSX::MIPS::OPCODE_BEQ, rs, rt, BRANCH_OFFSET));
    PSX::Bus::the()->write<u32>(SYSCALL_ADDRESS, create_r_instruction(PSX::MIPS::OPCODE_SPECIAL, 0u, 0u, 0u, 0u, PSX::MIPS::SPECIAL_SYSCALL));

    // Force the Program Counter to our written instruction and execute it
    cpu().jtag().force_pc(SYSCALL_ADDRESS - 4u);
    cpu().jtag().clock();
    cpu().jtag().clock();

    // We should now immediately be at the vector
    REQUIRE(cpu().jtag().read_pc() == MIPS_EXCEPTION_ADDRESS_GENERAL_BEV);
    REQUIRE(cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::STATUS_REGISTER_INDEX) == SYSCALL_STATUS_REGISTER);
    REQUIRE(cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::EPC_REGISTER_INDEX) == (SYSCALL_ADDRESS - 4u));

    u32 cause = cpu().jtag().read_cop0_register(PSX::MIPS::Coprocessor0::CAUSE_REGISTER_INDEX);
    u32 ExcCode = ((cause >> 2u) & 0x1Fu);
    REQUIRE(ExcCode == SYSCALL_CAUSE);
    REQUIRE((cause & 0x80000000u) == 0x80000000u);  // Make sure the BD bit is set
}
