/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/r3000a/mips/mips.h"
#include "hw/r3000a/r3000.h"

static constexpr u8     ZERO_REGISTER_INDEX     = 0u;
static constexpr u8     WRITE_REIGSTER_INDEX    = 1u;
static constexpr u32    TEST_WRITE_VALUE        = 0xAABBCCDD;

TEST_CASE("Test Program Counter increment is performed each cycle", "[cpu_basic]")
{
    PSX::R3000 cpu;

    REQUIRE(cpu.jtag().read_pc() == MIPS_INITIAL_PC_VALUE);
    REQUIRE(cpu.jtag().read_next_pc() == MIPS_INITIAL_PC_VALUE+4);

    cpu.cycle();

    REQUIRE(cpu.jtag().read_pc() == MIPS_INITIAL_PC_VALUE+4);
    REQUIRE(cpu.jtag().read_next_pc() == MIPS_INITIAL_PC_VALUE+8);
}

TEST_CASE("Test register reads and writes via JTAG", "[cpu_basic]")
{
    PSX::R3000 cpu;

    cpu.jtag().write_gpr(WRITE_REIGSTER_INDEX, TEST_WRITE_VALUE);
    cpu.jtag().clock();
    REQUIRE(cpu.jtag().read_gpr(WRITE_REIGSTER_INDEX) == TEST_WRITE_VALUE);
}

TEST_CASE("Make sure r0 ($zero) is fused to 0 and cannot be written to", "[cpu_basic]")
{
    PSX::R3000 cpu;

    cpu.jtag().write_gpr(ZERO_REGISTER_INDEX, TEST_WRITE_VALUE);
    cpu.jtag().clock();
    REQUIRE(cpu.jtag().read_gpr(ZERO_REGISTER_INDEX) == 0u);
}
