/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.h"
#include "hw/r3000a/mips/mips.h"
#include "hw/system/bus.h"
#include <random>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"

u32 create_i_instruction(u32 op, u32 rs, u32 rt, u32 immediate)
{
    Instruction instruction;

    instruction.i_type.op           = op;
    instruction.i_type.rs           = rs;
    instruction.i_type.rt           = rt;
    instruction.i_type.immediate    = immediate;

    return instruction.m_word;
}

u32 create_j_instruction(u32 op, u32 target)
{
    Instruction instruction;

    instruction.j_type.op       = op;
    instruction.j_type.target   = target;

    return instruction.m_word;
}

u32 create_r_instruction(u32 op, u32 rs, u32 rt, u32 rd, u32 shamt, u32 funct)
{
    Instruction instruction;

    instruction.r_type.op       = op;
    instruction.r_type.rs       = rs;
    instruction.r_type.rt       = rt;
    instruction.r_type.rd       = rd;
    instruction.r_type.shamt    = shamt;
    instruction.r_type.funct    = funct;

    return instruction.m_word;
}

int random_number(int a, int b)
{
    std::random_device rseed;
    std::mt19937 rng(rseed());
    std::uniform_int_distribution<int> dist(a, b);

    return dist(rng);
}

PSX::R3000& cpu()
{
    return PSX::Bus::the()->cpu();
}

void TestInit::clear_registers()
{
    for (u8 reg_num = 0u; reg_num < 32u; reg_num++)
    {
        cpu().jtag().write_gpr(reg_num, 0x00000000u);
    }
}

void TestInit::wipe_ram()
{
    for (auto address = PSX::RAM_BASE_ADDRESS_LO; address < PSX::RAM_BASE_ADDRESS_HI; address += 4u)
    {
        PSX::Bus::the()->write(address, 0u);
    }
}

#pragma GCC diagnostic pop
