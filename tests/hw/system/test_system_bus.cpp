/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/system/bus.h"
#include <random>

static constexpr u32        BIOS_FIRST_WORD         = 0x3C080013u;
static constexpr paddr_t    BIOS_ADDRESS_PHYSICAL   = 0x1FC00000u;
static constexpr u32        RAM_WRITE_VALUE         = 0xAABBCCDDu;
static constexpr u32        RAM_SIZE                = (2 * MB);

TEST_CASE("Test that a single byte read from the BIOS is correct through the bus", "[system_bus]")
{
    REQUIRE(PSX::Bus::the()->read<u8>(BIOS_ADDRESS_PHYSICAL) == static_cast<u8>(BIOS_FIRST_WORD));
}

TEST_CASE("Test that a 16-bit read from the BIOS is correct through the bus", "[system_bus]")
{
    REQUIRE(PSX::Bus::the()->read<u16>(BIOS_ADDRESS_PHYSICAL) == static_cast<u16>(BIOS_FIRST_WORD));
}


TEST_CASE("Test that a 32-bit read from the BIOS is correct through the bus", "[system_bus]")
{
    REQUIRE(PSX::Bus::the()->read<u32>(BIOS_ADDRESS_PHYSICAL) == BIOS_FIRST_WORD);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"

TEST_CASE("Test an 8-bit write to a random location in system memory", "[system_bus]")
{
    std::uniform_int_distribution<int> random_range(0, RAM_SIZE);
    auto seed = std::time(nullptr) ^ std::random_device()();
    std::mt19937 mt_device(seed);
    auto address = static_cast<paddr_t>(random_range(mt_device));

    PSX::Bus::the()->write<u8>(address, static_cast<u8>(RAM_WRITE_VALUE));
    REQUIRE(PSX::Bus::the()->read<u8>(address) == static_cast<u8>(RAM_WRITE_VALUE));
}

TEST_CASE("Test a 16-bit write to a random location in system memory", "[system_bus]")
{
    std::uniform_int_distribution<int> random_range(0, (RAM_SIZE - 2u));
    auto seed = std::time(nullptr) ^ std::random_device()();
    std::mt19937 mt_device(seed);
    auto address = static_cast<paddr_t>(random_range(mt_device)) & ~1u;

    PSX::Bus::the()->write<u16>(address, static_cast<u16>(RAM_WRITE_VALUE));
    REQUIRE(PSX::Bus::the()->read<u16>(address) == static_cast<u16>(RAM_WRITE_VALUE));
}

TEST_CASE("Test a 32-bit write to a random location in system memory", "[system_bus]")
{
    std::uniform_int_distribution<int> random_range(0, (RAM_SIZE - 4u));
    auto seed = std::time(nullptr) ^ std::random_device()();
    std::mt19937 mt_device(seed);
    auto address = static_cast<paddr_t>(random_range(mt_device)) & ~3u;

    PSX::Bus::the()->write<u32>(address, RAM_WRITE_VALUE);
    REQUIRE(PSX::Bus::the()->read<u32>(address) == RAM_WRITE_VALUE);
}

#pragma GCC diagnostic pop
