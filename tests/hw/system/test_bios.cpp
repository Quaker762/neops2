/*
 * This file is part of NeoPS
 * Copyright (c) 2022 Jesse Buhagiar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch2/catch_test_macros.hpp>
#include "hw/system/bios.h"
#include "util/global_config.h"

static constexpr u32 BIOS_FIRST_WORD    = 0x3C080013u;
static constexpr paddr_t BIOS_ADDRESS   = 0xBFC00000u;

TEST_CASE("Test that a single byte read from the BIOS is correct", "[bios]")
{
    PSX::BIOS bios;
    auto bios_path = g_config.get_config_value<std::string>("[system]", "bios_rom");
    REQUIRE(bios.load(bios_path) == true);

    REQUIRE(bios.read8(BIOS_ADDRESS) == static_cast<u8>(BIOS_FIRST_WORD));
}

TEST_CASE("Test that a 16-bit read from the BIOS is correct", "[bios]")
{
    PSX::BIOS bios;
    auto bios_path = g_config.get_config_value<std::string>("[system]", "bios_rom");
    REQUIRE(bios.load(bios_path) == true);

    REQUIRE(bios.read16(BIOS_ADDRESS) == static_cast<u16>(BIOS_FIRST_WORD));
}

TEST_CASE("Test that a 32-bit read from the BIOS is correct", "[bios]")
{
    PSX::BIOS bios;
    auto bios_path = g_config.get_config_value<std::string>("[system]", "bios_rom");
    REQUIRE(bios.load(bios_path) == true);

    REQUIRE(bios.read32(BIOS_ADDRESS) == BIOS_FIRST_WORD);
}
